﻿using AutoMapper;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Infrastructure
{
    class OurFilesProfiler : Profile
    {
        public OurFilesProfiler()
        {
            CreateMap<FileToAdd, FileInfoEntity>();
            CreateMap<MessageToAdd, MessageEntity>();
            CreateMap<FileInfoToAdd, FileInfoEntity>();
            CreateMap<MessageEntity, MessageToAccept>();
        }
    }
}
