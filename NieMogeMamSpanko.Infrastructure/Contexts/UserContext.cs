﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using NieMogeMamSpanko.Core.Entities;

namespace NieMogeMamSpanko.Infrastructure.Contexts
{
    public class UserContext : IdentityDbContext
    {
        public UserContext(DbContextOptions<UserContext> options) : base(options)
        {

        }
        public DbSet<UserEntity> ApplicationUsers { get; set; }
        public DbSet<FileInfoEntity> FilesInOurSystem { get; set; }
        public DbSet<MessageEntity> MessagesInOurSystem { get; set; }
        public DbSet<MessageAndUser> UserConnectedWithMessage { get; set; }
    }
}
