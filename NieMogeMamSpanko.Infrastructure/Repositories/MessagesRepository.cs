﻿using Microsoft.EntityFrameworkCore;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Infrastructure.Contexts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Infrastructure.Repositories
{
    public class MessagesRepository : IMessageRepository
    {
        private UserContext _context;
        public MessagesRepository(UserContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public void AddMessage(MessageEntity messageToAdd)
        {
            _context.MessagesInOurSystem.Add(messageToAdd);
        }

        public bool SaveChanges()
        {
            return  (_context.SaveChanges() > 0);
        }

         public async Task<IEnumerable<MessageEntity>> GetAllMessagesWhichNeedToBeAcceptAsync()
         {
            var messagesNotAccepted = await _context.MessagesInOurSystem.Where(m => m.IsAccepted == false).ToListAsync();
            return messagesNotAccepted;
         }

        public async Task<MessageEntity> GetMessageByIdAsync(string id)
        {
            var messageToReturn = await _context.MessagesInOurSystem.Where(m => m.Id == id).FirstOrDefaultAsync();
            if(messageToReturn == null)
            {
                throw new ArgumentNullException();
            }
            return messageToReturn;
        }

        public void DeleteMessage(MessageEntity messageToDelete)
        {
            _context.MessagesInOurSystem.Remove(messageToDelete);
        }
        /*
        public async Task<IEnumerable<MessageEntity>> GetAllMessagesForUserWithIdAsync(Guid id)
        {
            var messagesForUser = await _context.MessagesInOurSystem.Where(m => m.IsAccepted == true && m.userId == id).ToListAsync();
            return messagesForUser;
        }
        */

        public async Task<IEnumerable<MessageEntity>> GetMessagesByTheirIdsAsync(IEnumerable<string> messagesIds)
        {
            List<MessageEntity> messages = new List<MessageEntity>();
            foreach (var id in messagesIds)
            {
                var message = await _context.MessagesInOurSystem.Where(m => m.IsAccepted == true && m.Id == id).FirstOrDefaultAsync();
                if (message != null)
                { 
                messages.Add(message);
                }
            }
            return messages;

        }
    }
}
