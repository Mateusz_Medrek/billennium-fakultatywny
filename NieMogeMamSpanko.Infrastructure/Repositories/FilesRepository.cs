﻿using Microsoft.EntityFrameworkCore;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Infrastructure.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Infrastructure.Repositories
{
    public class FilesRepository : IFilesRepository
    {
        private UserContext _context;

        public FilesRepository(UserContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<IEnumerable<FileInfoEntity>> GetFilesAsync()
        {
            return await _context.FilesInOurSystem.ToListAsync();
        }

        public async Task<IEnumerable<FileInfoEntity>> GetFilesForMessageByMessageIdAsync(string messageId)
        {
            var files = await _context.FilesInOurSystem.Where(f => f.MessageId == messageId).ToListAsync();
            return files;
        }

        public async Task<FileInfoEntity> GetFileByIdAsync(string id)
        {
            var fileInfoEntity = await _context.FilesInOurSystem.Where(f => f.Id == id).FirstOrDefaultAsync();
            return fileInfoEntity;
        }

        public void RemoveFile(FileInfoEntity fileToRemove)
        {
            _context.FilesInOurSystem.Remove(fileToRemove);
        }

        public void AddFileInfo(FileInfoEntity fileToAdd)
        {
            if(fileToAdd == null)
            {
                throw new ArgumentNullException(nameof(fileToAdd));
            }
            _context.FilesInOurSystem.Add(fileToAdd);
        }

        public bool SaveChanges()
        {
            return ( _context.SaveChanges() > 0);
        }
    }
}
