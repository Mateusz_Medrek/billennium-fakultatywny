﻿using Microsoft.EntityFrameworkCore;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Infrastructure.Contexts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Infrastructure.Repositories
{
    public class UserConnectedWithMessageRepository : IUserConnectedWithMessageRepository
    {
        private UserContext _context;
        public UserConnectedWithMessageRepository(UserContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public void AddUserConnectedWithMessage(MessageAndUser messageAndUser)
        {
            _context.UserConnectedWithMessage.Add(messageAndUser);
        }

        public async Task<IEnumerable<MessageAndUser>> GetAllForMessageWithIdAsync(string id)
        {
            var messagesAndUsers = await _context.UserConnectedWithMessage.Where(o => o.MessageId == id).ToListAsync();
            return messagesAndUsers;
        }

        public async Task<IEnumerable<string>> GetAllMessagesIdForUserByMailAsync(string userMail)
        {
            var conncetions = await _context.UserConnectedWithMessage.Where(o => o.UserIdOrEmail == userMail).Select(o => o.MessageId).ToListAsync();
            return conncetions;
        }

        public async Task<IEnumerable<string>> GetAllMailsForMessageReceiversAsync(string messageId)
        {
            var mails = await _context.UserConnectedWithMessage.Where(o => o.MessageId == messageId).Select(o => o.UserIdOrEmail).ToListAsync();
            return mails;
        }

        public void DeleteMessageAndUser(MessageAndUser messageAndUserToDelete)
        {
            _context.Remove(messageAndUserToDelete);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges() > 0);
        }

    }
}
