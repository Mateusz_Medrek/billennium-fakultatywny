﻿using Microsoft.EntityFrameworkCore;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Infrastructure.Contexts;
using NieMogeMamSpanko.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xunit;

namespace NieMogeMamSpanko.Tests.Repositories.Tests
{
    public class FilesRepositoryTests
    {
        [Fact]
        public void AddingFileInfoTest()
        {
            IFilesRepository repository = GetInMemoryFilesRepository();
            FileInfoEntity fileInfo = new FileInfoEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Title = "example title",
            };

            repository.AddFileInfo(fileInfo);
            repository.SaveChanges();

            var file = repository.GetFileByIdAsync("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            Thread.Sleep(0);
            Assert.Equal("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", file.Result.Id);
            Assert.Equal("example title", file.Result.Title);
        }

        [Fact]
        public void RemoveFileTest()
        {
            IFilesRepository repository = GetInMemoryFilesRepository();
            FileInfoEntity fileInfo = new FileInfoEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Title = "example title",
            };

            repository.AddFileInfo(fileInfo);
            repository.SaveChanges();

            var file = repository.GetFileByIdAsync("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            Thread.Sleep(0);
            Assert.Equal("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", file.Result.Id);
            Assert.Equal("example title", file.Result.Title);
            repository.RemoveFile(file.Result);
            repository.SaveChanges();
            var fileAfterRemove = repository.GetFileByIdAsync("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            Thread.Sleep(0);
            Assert.Null(fileAfterRemove.Result);

        }

        [Fact]
        public void GetFilesForMessageByMessageIdAsyncTest()
        {
            IFilesRepository repository = GetInMemoryFilesRepository();
            FileInfoEntity fileInfo1 = new FileInfoEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Title = "example title",
                MessageId = "3efe8d1b-0f0b-4b2f-ac06-0354dd4686ae"
            };
            repository.AddFileInfo(fileInfo1);
            repository.SaveChanges();
            FileInfoEntity fileInfo2 = new FileInfoEntity()
            {
                Id = "2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Title = "example title",
                MessageId = "3efe8d1b-0f0b-4b2f-ac06-0354dd4686ae"
            };
            repository.AddFileInfo(fileInfo2);
            repository.SaveChanges();

            var files = repository.GetFilesForMessageByMessageIdAsync("3efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            Thread.Sleep(0);
            var filesList = files.Result.ToList();
            var filesListCount = filesList.Count;
            Assert.Equal(2, filesListCount);
        }
        [Fact]
        public void GetFilesAsyncTest()
        {
            IFilesRepository repository = GetInMemoryFilesRepository();
            FileInfoEntity fileInfo1 = new FileInfoEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Title = "example title",
                MessageId = "3efe8d1b-0f0b-4b2f-ac06-0354dd4686ae"
            };
            repository.AddFileInfo(fileInfo1);
            repository.SaveChanges();
            FileInfoEntity fileInfo2 = new FileInfoEntity()
            {
                Id = "2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Title = "example title",
                MessageId = "3efe8d1b-0f0b-4b2f-ac06-0354dd4686ae"
            };
            repository.AddFileInfo(fileInfo2);
            repository.SaveChanges();

            var files = repository.GetFilesAsync();
            Thread.Sleep(0);
            var filesList = files.Result.ToList();
            var filesListCount = filesList.Count;
            Assert.Equal(2, filesListCount);
            Assert.Equal("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", filesList[0].Id);
            Assert.Equal("2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", filesList[1].Id);
        }

        private IFilesRepository GetInMemoryFilesRepository()
        {
            DbContextOptions<UserContext> options;
            var builder = new DbContextOptionsBuilder<UserContext>();
            builder.UseInMemoryDatabase();
            options = builder.Options;
            UserContext userContext = new UserContext(options);
            userContext.Database.EnsureCreated();
            userContext.Database.EnsureDeleted();
            return new FilesRepository(userContext);
        }
    }
}
