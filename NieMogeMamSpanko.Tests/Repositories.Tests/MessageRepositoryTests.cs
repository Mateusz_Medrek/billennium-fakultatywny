﻿using Microsoft.EntityFrameworkCore;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Infrastructure.Contexts;
using NieMogeMamSpanko.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xunit;

namespace NieMogeMamSpanko.Tests.Repositories.Tests
{
    public class MessageRepositoryTests
    {
        [Fact]
        public void AddingMessageTest()
        {
            IMessageRepository repository = GetInMemoryMessageRepository();
            MessageEntity messageEntity = new MessageEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Description = "Example description",
                Sender = "user@example.com",
                IsAccepted = false,
                dateTime = DateTime.Now.ToString("dd MMMM yyyy")
            };

            repository.AddMessage(messageEntity);
            repository.SaveChanges();

            var message = repository.GetMessageByIdAsync("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            Thread.Sleep(0);
            Assert.Equal("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", message.Result.Id);
            Assert.Equal("Example description", message.Result.Description);
            Assert.Equal("user@example.com", message.Result.Sender);
            Assert.False(message.Result.IsAccepted);
            Assert.Equal(DateTime.Now.ToString("dd MMMM yyyy"), message.Result.dateTime);
        }

        [Fact]
        public void GetNotAcceptedMessagesTest()
        {
            IMessageRepository repository = GetInMemoryMessageRepository();
            MessageEntity messageEntity1 = new MessageEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Description = "Example description",
                Sender = "user@example.com",
                IsAccepted = false,
                dateTime = DateTime.Now.ToString("dd MMMM yyyy")
            };

            repository.AddMessage(messageEntity1);
            repository.SaveChanges();

            var messages = repository.GetAllMessagesWhichNeedToBeAcceptAsync();
            Thread.Sleep(2000);
            var amount = messages.Result.Count();
            Assert.Equal(1, amount);

            MessageEntity messageEntity2 = new MessageEntity()
            {
                Id = "2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Description = "Example description 2",
                Sender = "user2@example.com",
                IsAccepted = false,
                dateTime = DateTime.Now.ToString("dd MMMM yyyy")
            };

            repository.AddMessage(messageEntity2);
            repository.SaveChanges();

            messages = repository.GetAllMessagesWhichNeedToBeAcceptAsync();
            Thread.Sleep(0);
            amount = messages.Result.Count();
            Assert.Equal(2, amount);
            var messagesEntities = messages.Result.ToList();
            Assert.Equal("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", messagesEntities[0].Id);
            Assert.Equal("2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae", messagesEntities[1].Id);

        }


        [Fact]
        public void GetMessagesByTheirIdsTest()
        {
            IMessageRepository repository = GetInMemoryMessageRepository();
            MessageEntity messageEntity1 = new MessageEntity()
            {
                Id = "1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Description = "Example description",
                Sender = "user@example.com",
                IsAccepted = true,
                dateTime = DateTime.Now.ToString("dd MMMM yyyy")
            };

            repository.AddMessage(messageEntity1);
            repository.SaveChanges();

            MessageEntity messageEntity2 = new MessageEntity()
            {
                Id = "2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae",
                Description = "Example description 2",
                Sender = "user2@example.com",
                IsAccepted = true,
                dateTime = DateTime.Now.ToString("dd MMMM yyyy")
            };
            repository.AddMessage(messageEntity2);
            repository.SaveChanges();

            List<string> listOfMessagesId = new List<string>();
            // var messages = repository.GetMessagesByTheirIdsAsync(listOfMessagesId);
            //Thread.Sleep(0);
            //var messagesEntities = messages.Result;
            //var amountOfMessages = messagesEntities.Count();
            //Assert.Equal(0, amountOfMessages);
            listOfMessagesId.Add("1efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            listOfMessagesId.Add("2efe8d1b-0f0b-4b2f-ac06-0354dd4686ae");
            var messages = repository.GetMessagesByTheirIdsAsync(listOfMessagesId);
            Thread.Sleep(0);
            var messagesEntities = messages.Result;
            var amountOfMessages = messagesEntities.Count();
            Assert.Equal(2, amountOfMessages);

        }

        private IMessageRepository GetInMemoryMessageRepository()
        {
            DbContextOptions<UserContext> options;
            var builder = new DbContextOptionsBuilder<UserContext>();
            builder.UseInMemoryDatabase();
            options = builder.Options;
            UserContext userContext = new UserContext(options);
            userContext.Database.EnsureCreated();
            userContext.Database.EnsureDeleted();
            return new MessagesRepository(userContext);
        }
    }
}
