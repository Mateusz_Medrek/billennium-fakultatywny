﻿using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IUserConnectedWithMessageRepository
    {
        void AddUserConnectedWithMessage(MessageAndUser messageAndUser);
        Task<IEnumerable<MessageAndUser>> GetAllForMessageWithIdAsync(string id);
        Task<IEnumerable<string>> GetAllMessagesIdForUserByMailAsync(string userMail);
        Task<IEnumerable<string>> GetAllMailsForMessageReceiversAsync(string messageId);
        void DeleteMessageAndUser(MessageAndUser messageAndUserToDelete);
        bool SaveChanges();
    }
}
