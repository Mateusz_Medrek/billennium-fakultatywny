﻿using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IFilesRepository
    {
        Task<IEnumerable<FileInfoEntity>> GetFilesAsync();
        Task<FileInfoEntity> GetFileByIdAsync(string id);
        void AddFileInfo(FileInfoEntity fileToAdd);
        Task<IEnumerable<FileInfoEntity>> GetFilesForMessageByMessageIdAsync(string messageId);
        void RemoveFile(FileInfoEntity fileToRemove);
        bool SaveChanges();
    }
}
