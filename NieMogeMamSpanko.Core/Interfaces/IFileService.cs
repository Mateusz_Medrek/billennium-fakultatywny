﻿using Microsoft.AspNetCore.Http;
using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IFileService
    {
        FileInfoEntity AddFile(IFormFile file, string messageId);
        string GetContentType(string path);
        Task<string> GetFileNameByFileId(string fileId);
        Task DeleteFilesByMessageId(string messageId);
        Task<string> GetFileName(string fileId);
        Dictionary<string, string> GetMimeTypes();
    }
}
