﻿using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IMessageRepository
    {
        void AddMessage(MessageEntity messageToAdd);
        bool SaveChanges();
        Task<IEnumerable<MessageEntity>> GetAllMessagesWhichNeedToBeAcceptAsync();
        Task<IEnumerable<MessageEntity>> GetMessagesByTheirIdsAsync(IEnumerable<string> messagesIds);
        Task<MessageEntity> GetMessageByIdAsync(string id);
        void DeleteMessage(MessageEntity messageToDelete);
    }
}
