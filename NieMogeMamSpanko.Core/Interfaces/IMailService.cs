﻿using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IMailService
    {
        Task SendRetriveMessageMailAsync(string senderMail, string descriptionWhyRetrive);
        Task SendMailForMessageSenderAfterMessageAcceptAsync(MessageEntity messageEntity);
        Task SendMailForUserWithoutAccountAsync(MessageEntity messageEntity);
    }
}
