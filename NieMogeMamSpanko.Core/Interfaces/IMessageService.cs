﻿using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IMessageService
    {
        Task DeleteMessageByIdAsync(string messageId, string descriptionWhyRetrive);
        void AddMessage(MessageEntity messageToAdd);
        Task<IEnumerable<MessageToAccept>> GetMessagesNotAcceptedAsync();
        Task<IEnumerable<MessageEntity>> GetMessagesForUserByUserMailAsync(string userMail);
        void AcceptMessage(MessageEntity message);
        Task<MessageEntity> GetMessageByIdAsync(string id);
    }
}
