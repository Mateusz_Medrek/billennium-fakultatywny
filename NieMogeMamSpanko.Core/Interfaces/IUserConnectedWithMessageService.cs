﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Core.Interfaces
{
    public interface IUserConnectedWithMessageService
    {
        void AddUserConnectedWithMessage(string receivers, string messageId);
        Task RemoveUserConnectedWithMessage(string messageId);
    }
}
