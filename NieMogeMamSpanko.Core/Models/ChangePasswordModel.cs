﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class ChangePasswordModel
    {
        public string Email { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
