﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class ChangeEmailModel
    {
        public string OldEmail { get; set; }
        public string NewEmail { get; set; }
    }
}
