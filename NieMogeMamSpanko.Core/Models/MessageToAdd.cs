﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class MessageToAdd
    {
        public string Description { get; set; }
        public string Sender { get; set; }
    }
}
