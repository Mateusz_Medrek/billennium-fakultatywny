﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace NieMogeMamSpanko.Core.Models
{
    public class AppUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
