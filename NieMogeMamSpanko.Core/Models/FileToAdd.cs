﻿using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class FileToAdd
    {
        public string Title { get; set; }
        public int messageId { get; set; }
        public MessageEntity messageEntity { get; set; }
    }
}
