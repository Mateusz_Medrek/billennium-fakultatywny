﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class MessageAndFiles
    {
        public string sender { get; set; }
        public string receivers { get; set; }
        public List<IFormFile> filesToAdd { get; set; }
        public string description { get; set; }

    }
}
