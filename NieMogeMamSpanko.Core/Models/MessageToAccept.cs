﻿using NieMogeMamSpanko.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class MessageToAccept : BaseEntityWithStringId
    {
        public virtual User User { get; set; }
        public string Sender { get; set; }
        public string Description { get; set; }
        public bool IsAccepted { get; set; } = false;
        public IEnumerable<FileInfoEntity> Files { get; set; }
        public string dateTime { get; set; } = DateTime.Now.ToString("dd MMMM yyyy");
        public List<MessageAndUser> MessageReciever { get; set; }
        public IEnumerable<string> messageReceivers { get; set; }
    }
}
