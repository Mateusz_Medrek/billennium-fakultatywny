﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class MessageDelete
    {
        public string messageId { get; set; }
        public string descriptionWhyNotAccepted { get; set; }
    }
}
