﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class ChangeLoginModel
    {
        public string Email { get; set; }
        public string NewLogin { get; set; }
    }
}
