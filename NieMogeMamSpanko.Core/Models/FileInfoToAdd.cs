﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Models
{
    public class FileInfoToAdd
    {
        public string Title { get; set; }

        public string MessageId { get; set; }
    }
}
