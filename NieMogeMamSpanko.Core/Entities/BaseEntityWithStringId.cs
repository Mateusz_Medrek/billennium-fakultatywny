﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Entities
{
    public class BaseEntityWithStringId
    {
        public string Id { get; set; }
    }
}
