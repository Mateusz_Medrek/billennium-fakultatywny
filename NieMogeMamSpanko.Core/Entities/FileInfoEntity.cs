﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NieMogeMamSpanko.Core.Entities
{
    public class FileInfoEntity : BaseEntityWithStringId
    {
        [Required]
        public string Title { get; set; }

        public string MessageId { get; set; }

        public MessageEntity messageEntity { get; set; }

        //public virtual UploadedFile uploadedFile { get; set; }
    }
}
