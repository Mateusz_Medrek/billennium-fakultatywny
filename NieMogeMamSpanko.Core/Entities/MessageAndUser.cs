﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NieMogeMamSpanko.Core.Entities
{
    public class MessageAndUser : BaseEntityWithStringId
    { 
        public string MessageId { get; set; }
        public string UserIdOrEmail { get; set; }
    }
}
