﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Entities
{
    public class UserEntity : IdentityUser
    {
        public string Role { get; set; }
    }
}
