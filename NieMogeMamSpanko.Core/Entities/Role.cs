﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Entities
{
    public class Role
    {
        public const string Admin = "Admin";
        public const string SuperAdmin = "SuperAdmin";
        public const string User = "User";
    }
}
