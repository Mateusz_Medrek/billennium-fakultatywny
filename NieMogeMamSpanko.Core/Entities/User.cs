﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NieMogeMamSpanko.Core.Entities
{
    public class User : BaseEntity
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }

    }
}
