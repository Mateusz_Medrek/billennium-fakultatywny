﻿using Microsoft.AspNetCore.Http;
using NieMogeMamSpanko.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NieMogeMamSpanko.Core.Entities;
using Microsoft.Extensions.FileProviders;
using System.Collections;
using AutoMapper;
using NieMogeMamSpanko.Core.Models;

namespace NieMogeMamSpanko.Infrastructure
{
    public class FileService : IFileService
    {
        private IFilesRepository _filesRepository;
        private IFileProvider _fileProvider;
        private IMapper _mapper;

        public FileService(IFilesRepository filesRepository, IFileProvider fileProvider, IMapper mapper)
        {
            _filesRepository = filesRepository ?? throw new ArgumentNullException(nameof(filesRepository));
            _fileProvider = fileProvider ?? throw new ArgumentNullException(nameof(fileProvider));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public FileInfoEntity AddFile(IFormFile file, string messageId)
        {
            FileInfoToAdd fileInfo = new FileInfoToAdd()
            {
                MessageId = messageId,
                Title = file.FileName
            };
            var fileInfoToAdd = _mapper.Map<FileInfoEntity>(fileInfo);
            _filesRepository.AddFileInfo(fileInfoToAdd);
            _filesRepository.SaveChanges();
            return fileInfoToAdd;
        }

        public async Task DeleteFilesByMessageId(string messageId)
        {
            var filesToRemove = await _filesRepository.GetFilesForMessageByMessageIdAsync(messageId);
            foreach(var file in filesToRemove)
            {
                var fileExtension = file.Title.Substring(file.Title.LastIndexOf('.'));
                var fileDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", file.Id + fileExtension);
                if(File.Exists(fileDirectory))
                {
                    File.Delete(fileDirectory);
                }
                _filesRepository.RemoveFile(file);
            }
            _filesRepository.SaveChanges();
        }

        public async Task<string> GetFileNameByFileId(string fileId)
        {
            var fileInfoEntity = await _filesRepository.GetFileByIdAsync(fileId);
            if(fileInfoEntity == null)
            {
                throw new ArgumentNullException();
            }
            var fileName = fileInfoEntity.Title;
            return fileName;
        }


        public string GetContentType(string path)
        {
            var types = GetMimeTypes();
            var ext = Path.GetExtension(path).ToLowerInvariant();
            return types[ext];
        }

        public async Task<string> GetFileName(string fileId)
        {
            var fileEntity = await _filesRepository.GetFileByIdAsync(fileId);
            string fileName = fileEntity.Title;
            return fileName;
        }

        public Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {
                {".txt", "text/plain" },
                {".pdf", "application/pdf" },
                {".doc", "application/vnd.ms-word" },
                {".docx", "application/vnd.ms-word" },
                {".png", "image/png" },
                {".jpeg", "image/jpeg" },
                {".jpg", "image/jpg" },
            };
        }
    }
}
