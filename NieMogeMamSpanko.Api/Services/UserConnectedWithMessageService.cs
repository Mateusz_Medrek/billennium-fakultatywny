﻿using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Api.Services
{
    public class UserConnectedWithMessageService : IUserConnectedWithMessageService
    {
        private IUserConnectedWithMessageRepository _repository;

        public UserConnectedWithMessageService(IUserConnectedWithMessageRepository repository)
        {
            _repository = repository;
        }

        public void AddUserConnectedWithMessage(string receivers, string messageId)
        {
            var arrayOfUsersEmails = receivers.Split(',');
            foreach(var mail in arrayOfUsersEmails)
            {
                MessageAndUser x = new MessageAndUser()
                {
                    MessageId = messageId,
                    UserIdOrEmail = mail
                };
                _repository.AddUserConnectedWithMessage(x);
                _repository.SaveChanges();
            }
        }

        public async Task RemoveUserConnectedWithMessage(string messageId)
        {
            var connectionsToRemove = await _repository.GetAllForMessageWithIdAsync(messageId);
            foreach(var connection in connectionsToRemove)
            {
                _repository.DeleteMessageAndUser(connection);
                
            }
            _repository.SaveChanges();
        }


    }
}
