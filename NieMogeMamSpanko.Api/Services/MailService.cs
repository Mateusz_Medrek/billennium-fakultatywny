﻿using Microsoft.AspNetCore.Http;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Api.Services
{
    public class MailService : IMailService
    {
        private readonly IFilesRepository _filesRepository;
        private readonly IUserConnectedWithMessageRepository _userConnectedWithMessageRepository;
        public MailService(IUserConnectedWithMessageRepository userConnectedWithMessageRepository, IFilesRepository filesRepository)
        {
            _userConnectedWithMessageRepository = userConnectedWithMessageRepository ?? throw new ArgumentNullException(nameof(userConnectedWithMessageRepository));
            _filesRepository = filesRepository ?? throw new ArgumentNullException(nameof(filesRepository));
        }
        public async Task SendRetriveMessageMailAsync(string senderMail, string descriptionWhyRetrive)
        {
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("niemogemamspanko2019@gmail.com", "fakultet2019!")
            };
            using (var message = new MailMessage("niemogemamspanko2019@gmail.com", senderMail)
            {
                Subject = "Twoja wiadomość została odrzucona",
                Body = "Opis dlaczego odrzucono: " + descriptionWhyRetrive,
            })
            {
                await smtpClient.SendMailAsync(message);
            }
        }
        public async Task SendMailForMessageSenderAfterMessageAcceptAsync(MessageEntity messageEntity)
        {
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("niemogemamspanko2019@gmail.com", "fakultet2019!")
            };
            using (var message = new MailMessage("niemogemamspanko2019@gmail.com", messageEntity.Sender)
            {
                Subject = "Twoja wiadomość została przesłana dalej",
                Body = "Twoja wiadomość z dnia " + messageEntity.dateTime + " została zaakceptowana i przesłana dalej" 
            })
            {
                try
                {
                    await smtpClient.SendMailAsync(message);
                }
                catch
                {

                }
                
            }
        }
        
        public async Task SendMailForUserWithoutAccountAsync(MessageEntity messageEntity)
        {
            var receiversMails = await _userConnectedWithMessageRepository.GetAllMailsForMessageReceiversAsync(messageEntity.Id);
            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                Credentials = new NetworkCredential("niemogemamspanko2019@gmail.com", "fakultet2019!")
            };
            if(receiversMails == null)
            {
                throw new ArgumentNullException();
            }
            var files = await _filesRepository.GetFilesForMessageByMessageIdAsync(messageEntity.Id);
            List<string> paths = new List<string>();
            foreach(var file in files)
            {
                var fileExtension = file.Title.Substring(file.Title.LastIndexOf('.'));
                var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", file.Id + fileExtension);
                paths.Add(path);
            }
            
            //var attachments = new AttachmentCollection();
            foreach(var mail in receiversMails)
            {
                MailMessage mailMessage = new MailMessage("niemogemamspanko2019@gmail.com", mail);
                var filesList = files.ToList();
                int i = 0;
                foreach(var path in paths)
                {
                    mailMessage.Attachments.Add(new Attachment(path) { Name = filesList[i++].Title });
                }
                if (messageEntity.Description != null)
                {
                    mailMessage.Body = "Cześć,\nOd użytkownika o mailu " + messageEntity.Sender + " otrzymujesz następującą wiadomość: \n" + messageEntity.Description + "\nPliki do pobrania w załączniku";
                }
                else
                {
                    mailMessage.Body = "Cześć,\nOd użytkownika o mailu " + messageEntity.Sender + " otrzymujesz pliki.\nPliki do pobrania w załączniku";
                }
                mailMessage.Subject = "Otrzymałeś nowe pliki";
                try
                {
                    await smtpClient.SendMailAsync(mailMessage);
                }
                catch
                {

                }
            }

        }
        
    }
}
