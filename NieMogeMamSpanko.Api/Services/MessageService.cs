﻿using AutoMapper;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Api.Services
{
    public class MessageService : IMessageService
    {
        private IMessageRepository _messageRepository;
        private IFilesRepository _filesRepository;
        private IMailService _mailService;
        private IUserConnectedWithMessageRepository _userConnectedWithMessageRepository;
        private IMapper _mapper;

        public MessageService(IMessageRepository messageRepository, IFilesRepository filesRepository, IUserConnectedWithMessageRepository userConnectedWithMessageRepository, IMailService mailService, IMapper mapper)
        {
            _messageRepository = messageRepository ?? throw new ArgumentNullException(nameof(messageRepository));
            _filesRepository = filesRepository ?? throw new ArgumentNullException(nameof(filesRepository));
            _userConnectedWithMessageRepository = userConnectedWithMessageRepository ?? throw new ArgumentNullException(nameof(userConnectedWithMessageRepository));
            _mailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task DeleteMessageByIdAsync(string messageId, string descriptionWhyRetrive)
        {
            var messageToDelete = await _messageRepository.GetMessageByIdAsync(messageId);
            if (messageToDelete == null)
            {
                throw new ArgumentNullException();
            }
            await _mailService.SendRetriveMessageMailAsync(messageToDelete.Sender, descriptionWhyRetrive);
            _messageRepository.DeleteMessage(messageToDelete);
            _messageRepository.SaveChanges();
        }

        public void AddMessage(MessageEntity messageToAdd)
        {
            if(messageToAdd == null)
            {
                throw new ArgumentNullException();
            }

            _messageRepository.AddMessage(messageToAdd);
            _messageRepository.SaveChanges();
        }

        public void AcceptMessage(MessageEntity message)
        {
            message.IsAccepted = true;
            _messageRepository.SaveChanges();
        }

        public async Task<IEnumerable<MessageToAccept>> GetMessagesNotAcceptedAsync()
        {
            var messagesNotAccepted = await _messageRepository.GetAllMessagesWhichNeedToBeAcceptAsync();
            List<MessageToAccept> messagesToReurn = new List<MessageToAccept>();
            foreach(var message in messagesNotAccepted)
            {
                var files = await _filesRepository.GetFilesForMessageByMessageIdAsync(message.Id);
                message.Files = files;
                var messageToAccept = _mapper.Map<MessageToAccept>(message);
                messageToAccept.messageReceivers = await _userConnectedWithMessageRepository.GetAllMailsForMessageReceiversAsync(message.Id);
                messagesToReurn.Add(messageToAccept);
            }
            return messagesToReurn;
        }

        public async Task<MessageEntity> GetMessageByIdAsync(string id)
        {
            var messageEntity = await _messageRepository.GetMessageByIdAsync(id);
            return messageEntity;
        }

        public async Task<IEnumerable<MessageEntity>> GetMessagesForUserByUserMailAsync(string userMail)
        {
            var messagesIds = await _userConnectedWithMessageRepository.GetAllMessagesIdForUserByMailAsync(userMail);
            var messagesForUser = await _messageRepository.GetMessagesByTheirIdsAsync(messagesIds);
            if (messagesForUser != null)
            {
                foreach (var message in messagesForUser)
                {
                    var files = await _filesRepository.GetFilesForMessageByMessageIdAsync(message.Id);
                    message.Files = files;
                }
            }
            return messagesForUser;
        }
    }
}
