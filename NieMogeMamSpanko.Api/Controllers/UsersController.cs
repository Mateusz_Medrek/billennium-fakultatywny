﻿using WebApi.Services;
using System.Threading.Tasks;
using System;
using System.Linq;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Models;
using NieMogeMamSpanko.Infrastructure.Contexts;
using System.Collections.Generic;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private UserManager<UserEntity> _userManager;
        private readonly AppSettings _appSettings;
        private UserContext _context;
        private SignInManager<UserEntity> _signInMenager;

        public UsersController(IUserService userService, 
            UserManager<UserEntity> userManager,
            SignInManager<UserEntity> signInManager,
            UserContext context)
        {
            _userService = userService;
            _userManager = userManager;
            _context = context;
            _signInMenager = signInManager;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null && await _userManager.CheckPasswordAsync(user, model.Password))
                return BadRequest(new { message = "Email or password is incorrect." }); ;
            
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name,user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)), 
                    SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);
            return Ok(user);
        }
        
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userManager.Users.ToList();
            var usersList = new List<User>();
            foreach (var user in users)
            {
                // Do not return SuperAdmin;
                if (user.Role == Role.SuperAdmin) continue;
                usersList.Add(new User { Email = user.Email, Role = user.Role, UserName = user.UserName });
            }
            return Ok(usersList);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetById(id);

            if (user == null)
            {
                return NotFound();
            }

            // only allow admins to access other user records
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
            {
                return Forbid();
            }

            return Ok(user);
        }


        [HttpPut]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] ChangeEmailModel changeEmailModel)
        {
            var user = await _userManager.FindByEmailAsync(changeEmailModel.OldEmail);

            user.Email = changeEmailModel.NewEmail;

            var result = await _userManager.UpdateAsync(user);
            return Ok(result);
        }

        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> Delete()
        {
            string userId = User.Claims.First(c => c.Type == "Id").Value;
            var user = await _userManager.FindByIdAsync(userId);
            
            await _context.SaveChangesAsync();
            var result = _userManager.DeleteAsync(user);
            return Ok(result);
        }

        [HttpPost]
        [Route("ChangePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordModel changePasswordModel)
        {
            var user = await _userManager.FindByEmailAsync(changePasswordModel.Email);
            var result = await _userManager.ChangePasswordAsync(user, changePasswordModel.OldPassword, changePasswordModel.NewPassword);
            return Ok(result);
        }

        [HttpPost]
        [Route("ChangeLogin")]
        public async Task<IActionResult> ChangeLogin([FromBody] ChangeLoginModel changeLoginModel)
        {
            var user = await _userManager.FindByEmailAsync(changeLoginModel.Email);
            user.UserName = changeLoginModel.NewLogin;
            var result = await _userManager.UpdateAsync(user);
            return Ok(result);
        }

        [HttpPost]
        [Route("ChangeRole")]
        public async Task<IActionResult> ChangeRole([FromBody] ChangeRoleModel changeRoleModel)
        {
            var user = await _userManager.FindByEmailAsync(changeRoleModel.Email);
            try
            {
                var role = user.Role;
                await _userManager.RemoveFromRoleAsync(user, user.Role);
                if (role == "Admin") // Jeśli dotychczas user był Adminem, to teraz będzie Userem;
                {
                    await _userManager.AddToRoleAsync(user, Role.User);
                    user.Role = Role.User;
                }
                else // W przeciwnym razie będzie Adminem;
                {
                    await _userManager.AddToRoleAsync(user, Role.Admin);
                    user.Role = Role.Admin;
                }
                await _userManager.UpdateAsync(user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPost]
        [Route("ResetPassword")]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel resetPasswordModel)
        {
            var user = await _userManager.FindByEmailAsync(resetPasswordModel.Email);
            try
            {
                await _userManager.RemovePasswordAsync(user);
                await _userManager.AddPasswordAsync(user, resetPasswordModel.Password);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
    }
}