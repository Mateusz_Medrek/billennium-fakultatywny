﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace NieMogeMamSpanko.Api.Controllers
{
    [Route("api/message")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private IMessageService _messageService;
        private IFileService _fileService;
        private IUserConnectedWithMessageService _userConnectedWithMessageService;
        private IMailService _mailService;
        private IMapper _mapper;

        public MessageController(IMessageService messageService, IMapper mapper, IFileService fileService, IUserConnectedWithMessageService userConnectedWithMessageService, IMailService mailService)
        {
            _messageService = messageService ?? throw new ArgumentNullException(nameof(messageService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
            _userConnectedWithMessageService = userConnectedWithMessageService ?? throw new ArgumentNullException(nameof(userConnectedWithMessageService));
            _mailService = mailService ?? throw new ArgumentNullException(nameof(mailService));
        }

        [HttpPost]
        public async Task<IActionResult> AddMessage([FromForm] MessageAndFiles messageAndFiles)
        {
            if (messageAndFiles.filesToAdd == null)
            {
                throw new ArgumentNullException();
            }
            
            MessageToAdd messageToAdd = new MessageToAdd()
            {
                Description = messageAndFiles.description,
                Sender = messageAndFiles.sender

            };
            var messageEntity = _mapper.Map<MessageEntity>(messageToAdd);
            _messageService.AddMessage(messageEntity);
            
            _userConnectedWithMessageService.AddUserConnectedWithMessage(messageAndFiles.receivers, messageEntity.Id);
            foreach (var file in messageAndFiles.filesToAdd)
            {

                 var addedFile = _fileService.AddFile(file, messageEntity.Id);
                 string fileId = addedFile.Id;
                 var filename = file.FileName;
                 var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", fileId + filename.Substring(filename.IndexOf('.')));
                 using (var stream = new FileStream(path, FileMode.Create))
                 {
                     await file.CopyToAsync(stream);
                        
                 }
            }
            
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetMessagesNotAccepted()
        {
            var messagesNotAccepted = await _messageService.GetMessagesNotAcceptedAsync();
            return Ok(messagesNotAccepted);
        }

        [Route("accept")]
        [HttpPatch]
        public async Task<IActionResult> AcceptMessage([FromQuery] string id)
        {
            Task<MessageEntity> task = _messageService.GetMessageByIdAsync(id);
            await task;
            var messageToAccept = task.Result;
            _messageService.AcceptMessage(messageToAccept);
            await _mailService.SendMailForUserWithoutAccountAsync(messageToAccept);
            await _mailService.SendMailForMessageSenderAfterMessageAcceptAsync(messageToAccept);
            return Ok();
        }

        [Route("delete")]
        [HttpDelete]
        public async Task<IActionResult> NotAcceptMessage([FromQuery]MessageDelete messageDelete)
        {
            await _messageService.DeleteMessageByIdAsync(messageDelete.messageId, messageDelete.descriptionWhyNotAccepted);
            await _userConnectedWithMessageService.RemoveUserConnectedWithMessage(messageDelete.messageId);
            await _fileService.DeleteFilesByMessageId(messageDelete.messageId);
            return Ok();
        }
        
        [Route("messagesforuser")]
        [HttpGet]
        public async Task<IActionResult> MessagesForUSerByUserId([FromQuery] string userMail)
        {
            var messages = await _messageService.GetMessagesForUserByUserMailAsync(userMail);
            return Ok(messages);
        }
        

        [Route("download")]
        [HttpGet]
        public async Task<IActionResult> DownoladFileById([FromQuery] string fileId)
        {
            Task<string> task = _fileService.GetFileNameByFileId(fileId);
            await task;
            var fileName = task.Result;
            var fileExtension = fileName.Substring(fileName.LastIndexOf('.'));
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", fileId + fileExtension);

            var memory = new MemoryStream();
            try
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                     await stream.CopyToAsync(memory);
                }
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
            memory.Position = 0;
            System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
            {
                FileName = fileName,
                Inline = false  // false = prompt the user for downloading;  true = browser to try to show the file inline
            };
            Response.Headers.Add("Content-Disposition", cd.ToString());
            FileStreamResult fileStreamResult = new FileStreamResult(memory, _fileService.GetContentType(path))
            {
                FileDownloadName = fileName
            };
            return fileStreamResult;
        }
    }
}
