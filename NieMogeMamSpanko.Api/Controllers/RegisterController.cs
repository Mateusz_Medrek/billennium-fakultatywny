﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NieMogeMamSpanko.Core.Entities;
using NieMogeMamSpanko.Core.Models;

namespace NieMogeMamSpanko.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly UserManager<UserEntity> _userManager;
        private readonly SignInManager<UserEntity> _signInManager;
        private readonly AppSettings _appSettings;

        public RegisterController(UserManager<UserEntity> userManager, SignInManager<UserEntity> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpPost]
        [Route("Register")]
        //POST : /api/RegisterController/Register
        public async Task<Object> PostApplicationUser([FromBody] UserRegisterModel model)
        {
            var applicationUser = new UserEntity()
            {
                UserName = model.Login,
                Email = model.Email,
                Role = Role.User
            };
            var checkEmail = await _userManager.FindByEmailAsync(model.Email);
            if(checkEmail != null)
            {
                return "This mail is alredy used";
            }
            try
            {
                var result = await _userManager.CreateAsync(applicationUser, model.Password);
                if (result.Succeeded)
                {
                    var currentUser = await _userManager.FindByEmailAsync(model.Email);
                    var roleResult = await _userManager.AddToRoleAsync(currentUser, Role.User);
                    return Ok(result);
                }
                throw new Exception(result.Errors.ToString());
            }
            catch (Exception ex)
            {
                return ex.Message;
                throw ex;
            }
        }
        
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login ([FromBody] LoginModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
                };

                var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("MySuperSecurity"));

                var token = new JwtSecurityToken(
                    issuer: "http://oec.com",
                    audience: "http://oec.com",
                    expires: DateTime.UtcNow.AddHours(1),
                    claims: claims,
                    signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(signingKey,SecurityAlgorithms.HmacSha256)
                    );
                return Ok(user);
            }

            return BadRequest(new { message = "Email or password is incorrect." });
        }
    }
}