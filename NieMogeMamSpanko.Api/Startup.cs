﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NieMogeMamSpanko.Api.Services;
using NieMogeMamSpanko.Core.Interfaces;
using NieMogeMamSpanko.Infrastructure;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using NieMogeMamSpanko.Api.Helpers;
using NieMogeMamSpanko.Infrastructure.Contexts;
using NieMogeMamSpanko.Infrastructure.Repositories;
using NieMogeMamSpanko.Core.Entities;
using WebApi.Services;

namespace NieMogeMamSpanko.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFileProvider>(
                new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var appSettingsSection = Configuration.GetSection("AppSettings");
         
            // konfiguracja autoryzacji
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            var ConnectionString = Configuration["ConnectionString:NieMogeMamSpankoDBConnectionString"];
            services
                .AddDbContext<UserContext>(options =>
                options.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = NieMogeMamSpankoDB; Trusted_Connection = true;"));


            services.AddTransient<IFilesRepository, FilesRepository>();
            services.AddTransient<IMessageRepository, MessagesRepository>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IUserConnectedWithMessageRepository, UserConnectedWithMessageRepository>();
            services.AddTransient<IUserConnectedWithMessageService, UserConnectedWithMessageService>();
            services.AddTransient<IMailService, MailService>();

            services.AddAutoMapper();
            services.AddDefaultIdentity<UserEntity>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<UserContext>();

            services.Configure<IdentityOptions>(options =>
            {
                options.User.RequireUniqueEmail = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
            });

            // Dodane, żeby np w UserService dało się z appSettings pobrać secreta //
            services.Configure<AppSettings>(options =>
            {
                options.Secret = appSettings.Secret;
            });
            // --------------------------------------- //

            services.AddSwaggerGen(setupAction =>
            {
                setupAction.SwaggerDoc(
                    "NieMogeMamSpankoAPISpecification",
                    new Microsoft.OpenApi.Models.OpenApiInfo()
                    {
                        Title = "NieMogeMamSpanko API",
                        Version = "1"
                    });
            });

            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(setupAction =>
            {
                setupAction.SwaggerEndpoint(
                    "/swagger/NieMogeMamSpankoAPISpecification/swagger.json", "NieMogeMamSpanko API");
            });

            app.UseAuthentication();

            app.UseCors(builder =>
            builder.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()

            );

            app.UseMvc();
        }
    }
}
