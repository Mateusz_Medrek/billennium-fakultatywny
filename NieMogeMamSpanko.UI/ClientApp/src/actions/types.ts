import { Action } from 'redux';

// LOGIN ACTIONS;
export const CHANGE_EMAIL_SUCCEEDED = 'CHANGE_EMAIL_SUCCEEDED';
export const CHANGE_EMAIL_FAILED = 'CHANGE_EMAIL_FAILED';
export const CHANGE_LOGIN_SUCCEEDED = 'CHANGE_LOGIN_SUCCEEDED';
export const CHANGE_LOGIN_FAILED = 'CHANGE_LOGIN_FAILED';
export const CHANGE_PASSWORD_SUCCEEDED = 'CHANGE_PASSWORD_SUCCEEDED';
export const CHANGE_PASSWORD_FAILED = 'CHANGE_PASSWORD_FAILED';
export const LOGIN_SUCCEEDED = 'LOGIN_SUCCEEDED';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const REGISTER_SUCCEEDED = 'REGISTER_SUCCEEDED';
export const REGISTER_FAILED = 'REGISTER_FAILED';

export interface LoginDataResponse {
    userName: string;
    role: string;
    email: string;
    token: string;
    id: number;
}

interface LoginSucceededAction extends Action {
    type: typeof LOGIN_SUCCEEDED;
    payload: LoginDataResponse;
}

interface LoginFailedAction extends Action {
    type: typeof LOGIN_FAILED;
    payload: string;
}

interface RegisterSucceededAction extends Action {
    type: typeof REGISTER_SUCCEEDED;
    payload: undefined;
}

interface RegisterFailedAction extends Action {
    type: typeof REGISTER_FAILED;
    payload: string;
}

interface ChangeEmailSucceededAction extends Action {
    type: typeof CHANGE_EMAIL_SUCCEEDED;
    payload: string;
}

interface ChangeEmailFailedAction extends Action {
    type: typeof CHANGE_EMAIL_FAILED;
    payload: string;
}

interface ChangeLoginSucceededAction extends Action {
    type: typeof CHANGE_LOGIN_SUCCEEDED;
    payload: undefined;
}

interface ChangeLoginFailedAction extends Action {
    type: typeof CHANGE_LOGIN_FAILED;
    payload: string;
}

interface ChangePasswordSucceededAction extends Action {
    type: typeof CHANGE_PASSWORD_SUCCEEDED;
    payload: undefined;
}

interface ChangePasswordFailedAction extends Action {
    type: typeof CHANGE_PASSWORD_FAILED;
    payload: string;
}

export type LoginActions =
    | LoginFailedAction
    | LoginSucceededAction
    | RegisterSucceededAction
    | RegisterFailedAction
    | ChangeEmailSucceededAction
    | ChangeEmailFailedAction
    | ChangeLoginSucceededAction
    | ChangeLoginFailedAction
    | ChangePasswordSucceededAction
    | ChangePasswordFailedAction;

// MESSAGE ACTIONS;
export const ACCEPT_MESSAGE = 'ACCEPT_MESSAGE';
export const ACCEPT_MESSAGE_ERROR = 'ACCEPT_MESSAGE_ERROR';
export const CREATE_MESSAGE = 'CREATE_MESSAGE';
export const CREATE_MESSAGE_ERROR = 'CREATE_MESSAGE_ERROR';
export const DOWNLOAD_FILE = 'DOWNLOAD_FILE';
export const DOWNLOAD_FILE_ERROR = 'DOWNLOAD_FILE_ERROR';
export const GET_MESSAGES_FOR_USER = 'GET_MESSAGES_FOR_USER';
export const GET_MESSAGES_FOR_USER_ERROR = 'GET_MESSAGES_FOR_USER_ERROR';
export const GET_NOT_ACCEPTED_MESSAGES = 'GET_NOT_ACCEPTED_MESSAGES';
export const GET_NOT_ACCEPTED_MESSAGES_ERROR =
    'GET_NOT_ACCEPTED_MESSAGES_ERROR';
export const REJECT_MESSAGE = 'REJECT_MESSAGE';
export const REJECT_MESSAGE_ERROR = 'REJECT_MESSAGE_ERROR';

export interface MyFile extends File {
    id: string;
    messageId: string;
    title: string;
}

export interface ReceivedMessage {
    sender: string;
    files: MyFile[];
    description: string;
    id: string;
    date: Date;
}

export interface Message extends ReceivedMessage {
    messageReceivers: string | string[];
}

interface AcceptMessageAction extends Action {
    type: typeof ACCEPT_MESSAGE;
    payload: string;
}

interface AcceptMessageErrorAction extends Action {
    type: typeof ACCEPT_MESSAGE_ERROR;
    payload: string;
}

interface CreateMessageAction extends Action {
    type: typeof CREATE_MESSAGE;
    payload: undefined;
}

interface CreateMessageErrorAction extends Action {
    type: typeof CREATE_MESSAGE_ERROR;
    payload: string;
}

interface DownloadFileAction extends Action {
    type: typeof DOWNLOAD_FILE;
    payload: undefined;
}

interface DownloadFileActionError extends Action {
    type: typeof DOWNLOAD_FILE_ERROR;
    payload: string;
}

interface GetMessagesForUserAction extends Action {
    type: typeof GET_MESSAGES_FOR_USER;
    payload: ReceivedMessage[];
}

interface GetMessagesForUserActionError extends Action {
    type: typeof GET_MESSAGES_FOR_USER_ERROR;
    payload: string;
}

interface GetNotAcceptedMessagesAction extends Action {
    type: typeof GET_NOT_ACCEPTED_MESSAGES;
    payload: Message[];
}

interface GetNotAcceptedMessagesActionError extends Action {
    type: typeof GET_NOT_ACCEPTED_MESSAGES_ERROR;
    payload: string;
}

interface RejectMessageAction extends Action {
    type: typeof REJECT_MESSAGE;
    payload: string;
}

interface RejectMessageErrorAction extends Action {
    type: typeof REJECT_MESSAGE_ERROR;
    payload: string;
}

export type MessageActions =
    | AcceptMessageAction
    | AcceptMessageErrorAction
    | CreateMessageAction
    | CreateMessageErrorAction
    | DownloadFileAction
    | DownloadFileActionError
    | GetMessagesForUserAction
    | GetMessagesForUserActionError
    | GetNotAcceptedMessagesAction
    | GetNotAcceptedMessagesActionError
    | RejectMessageAction
    | RejectMessageErrorAction;

// SIGN ACTIONS;
export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

interface SignInAction extends Action {
    type: typeof SIGN_IN;
}

interface SignOutAction extends Action {
    type: typeof SIGN_OUT;
}

export type SignActions = SignInAction | SignOutAction;

// USER ACTIONS:
export const CHANGE_USER_EMAIL = 'CHANGE_USER_EMAIL';
export const CHANGE_USER_EMAIL_ERROR = 'CHANGE_USER_EMAIL_ERROR';
export const CHANGE_USER_LOGIN = 'CHANGE_USER_LOGIN';
export const CHANGE_USER_LOGIN_ERROR = 'CHANGE_USER_LOGIN_ERROR';
export const CHANGE_USER_ROLE = 'CHANGE_USER_ROLE';
export const CHANGE_USER_ROLE_ERROR = 'CHANGE_USER_ROLE_ERROR';
export const GET_ALL_USERS = 'GET_ALL_USERS';
export const GET_ALL_USERS_ERROR = 'GET_ALL_USERS_ERROR';
export const RESET_USER_PASSWORD = 'RESET_USER_PASSWORD';
export const RESET_USER_PASSWORD_ERROR = 'RESET_USER_PASSWORD_ERROR';

interface ChangeUserEmailAction extends Action {
    type: typeof CHANGE_USER_EMAIL;
    payload: undefined;
}

interface ChangeUserEmailActionError extends Action {
    type: typeof CHANGE_USER_EMAIL_ERROR;
    payload: string;
}

interface ChangeUserLoginAction extends Action {
    type: typeof CHANGE_USER_LOGIN;
    payload: undefined;
}

interface ChangeUserLoginActionError extends Action {
    type: typeof CHANGE_USER_LOGIN_ERROR;
    payload: string;
}

interface ChangeUserRoleAction extends Action {
    type: typeof CHANGE_USER_ROLE;
    payload: undefined;
}

interface ChangeUserRoleActionError extends Action {
    type: typeof CHANGE_USER_ROLE_ERROR;
    payload: string;
}

interface GetAllUsersAction extends Action {
    type: typeof GET_ALL_USERS;
    payload: User[];
}

interface GetAllUsersActionError extends Action {
    type: typeof GET_ALL_USERS_ERROR;
    payload: string;
}

interface ResetUserPasswordAction extends Action {
    type: typeof RESET_USER_PASSWORD;
    payload: undefined;
}

interface ResetUserPasswordActionError extends Action {
    type: typeof RESET_USER_PASSWORD_ERROR;
    payload: string;
}

export interface User {
    email: string;
    userName: string;
    role: string;
}

export type UserActions =
    | GetAllUsersAction
    | GetAllUsersActionError
    | ChangeUserRoleAction
    | ChangeUserRoleActionError
    | ChangeUserEmailAction
    | ChangeUserEmailActionError
    | ChangeUserLoginAction
    | ChangeUserLoginActionError
    | ResetUserPasswordAction
    | ResetUserPasswordActionError;

// FORM VALUES;
export interface FormValues {
    [key: string]: string;
}

export interface MessageFormValues {
    sender: string;
    receivers: string;
    files: File[];
    description: string;
}

// APP STATE;
interface AuthState {
    changeEmailError: string | null;
    changeLoginError: string | null;
    changePasswordError: string | null;
    email: string | null;
    loginError: string | null;
    isSignedIn: boolean | null;
    registerError: string | null;
    role: string | null;
    userId: string | number | null;
}

interface MessageState {
    error: string | null;
    messagesForUser: ReceivedMessage[];
    notAcceptedMessages: Message[];
}

interface UserState {
    changeUserLoginError: string;
    changeUserEmailError: string;
    changeUserRoleError: string;
    getAllUsersError: string;
    resetUserPasswordError: string;
    users: User[];
}

export interface AppState {
    auth: AuthState;
    message: MessageState;
    user: UserState;
}
