﻿import axios from 'axios';
import { ThunkAction } from 'redux-thunk';
import {
    ACCEPT_MESSAGE,
    ACCEPT_MESSAGE_ERROR,
    AppState,
    CHANGE_EMAIL_FAILED,
    CHANGE_EMAIL_SUCCEEDED,
    CHANGE_LOGIN_FAILED,
    CHANGE_LOGIN_SUCCEEDED,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_SUCCEEDED,
    CHANGE_USER_EMAIL,
    CHANGE_USER_EMAIL_ERROR,
    CHANGE_USER_LOGIN,
    CHANGE_USER_LOGIN_ERROR,
    CHANGE_USER_ROLE,
    CHANGE_USER_ROLE_ERROR,
    CREATE_MESSAGE,
    CREATE_MESSAGE_ERROR,
    DOWNLOAD_FILE,
    DOWNLOAD_FILE_ERROR,
    GET_ALL_USERS,
    GET_ALL_USERS_ERROR,
    GET_MESSAGES_FOR_USER,
    GET_MESSAGES_FOR_USER_ERROR,
    GET_NOT_ACCEPTED_MESSAGES,
    GET_NOT_ACCEPTED_MESSAGES_ERROR,
    LOGIN_FAILED,
    LOGIN_SUCCEEDED,
    LoginActions,
    LoginDataResponse,
    MessageActions,
    REGISTER_FAILED,
    REGISTER_SUCCEEDED,
    REJECT_MESSAGE,
    REJECT_MESSAGE_ERROR,
    RESET_USER_PASSWORD,
    RESET_USER_PASSWORD_ERROR,
    SIGN_IN,
    SIGN_OUT,
    SignActions,
    UserActions
} from './types';

export const authenticateUser = (
    email: string,
    password: string
): ThunkAction<
    Promise<LoginActions>,
    AppState,
    void,
    LoginActions
> => async dispatch => {
    const data = JSON.stringify({
        UserName: email,
        Email: email,
        Password: password
    });
    let response;
    try {
        response = await axios.post<LoginDataResponse>(
            'http://localhost:62795/api/register/Login',
            data,
            {
                headers: { 'Content-Type': 'application/json' }
            }
        );
        return dispatch({
            type: LOGIN_SUCCEEDED,
            payload: response!.data
        });
    } catch (error) {
        return dispatch({
            type: LOGIN_FAILED,
            payload: 'Podano nieprawidłowe dane logowania'
        });
    }
};

export const acceptMessage = (
    messageId: string
): ThunkAction<
    Promise<MessageActions>,
    AppState,
    void,
    MessageActions
> => async dispatch => {
    try {
        const response = await axios.patch(
            'http://localhost:62795/api/message/accept',
            {},
            {
                params: {
                    id: messageId
                }
            }
        );
        return dispatch({
            type: ACCEPT_MESSAGE,
            payload: messageId
        });
    } catch (error) {
        return dispatch({
            type: ACCEPT_MESSAGE_ERROR,
            payload: error.message
        });
    }
};

export const changeEmail = (
    oldEmail: string,
    newEmail: string
): ThunkAction<
    Promise<LoginActions>,
    AppState,
    void,
    LoginActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            OldEmail: oldEmail,
            NewEmail: newEmail
        });
        const response = await axios.put(
            'http://localhost:62795/Users/Update',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        return dispatch({
            type: CHANGE_EMAIL_SUCCEEDED,
            payload: newEmail
        });
    } catch (error) {
        return dispatch({
            type: CHANGE_EMAIL_FAILED,
            payload: error.message
        });
    }
};

export const changeLogin = (
    email: string,
    newLogin: string
): ThunkAction<
    Promise<LoginActions>,
    AppState,
    void,
    LoginActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            Email: email,
            NewLogin: newLogin
        });
        const response = await axios.post(
            'http://localhost:62795/Users/ChangeLogin',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        return dispatch({
            type: CHANGE_LOGIN_SUCCEEDED,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: CHANGE_LOGIN_FAILED,
            payload: error.message
        });
    }
};

export const changePassword = (
    email: string,
    oldPassword: string,
    newPassword: string
): ThunkAction<
    Promise<LoginActions>,
    AppState,
    void,
    LoginActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            email,
            oldPassword,
            newPassword
        });
        const response = await axios.post(
            'http://localhost:62795/Users/ChangePassword',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        return dispatch({
            type: CHANGE_PASSWORD_SUCCEEDED,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: CHANGE_PASSWORD_FAILED,
            payload: error.message
        });
    }
};

export const changeUserEmail = (
    oldEmail: string,
    newEmail: string
): ThunkAction<
    Promise<UserActions>,
    AppState,
    void,
    UserActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            OldEmail: oldEmail,
            NewEmail: newEmail
        });
        const response = await axios.put(
            'http://localhost:62795/Users/Update',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        dispatch(getAllUsers());
        return dispatch({
            type: CHANGE_USER_EMAIL,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: CHANGE_USER_EMAIL_ERROR,
            payload: error.message
        });
    }
};

export const changeUserLogin = (
    email: string,
    newLogin: string
): ThunkAction<
    Promise<UserActions>,
    AppState,
    void,
    UserActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            Email: email,
            NewLogin: newLogin
        });
        const response = await axios.post(
            'http://localhost:62795/Users/ChangeLogin',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        dispatch(getAllUsers());
        return dispatch({
            type: CHANGE_USER_LOGIN,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: CHANGE_USER_LOGIN_ERROR,
            payload: error.message
        });
    }
};

export const changeUserRole = (
    email: string
): ThunkAction<
    Promise<UserActions>,
    AppState,
    void,
    UserActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            email
        });
        const response = await axios.post(
            'http://localhost:62795/Users/ChangeRole',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        dispatch(getAllUsers());
        return dispatch({
            type: CHANGE_USER_ROLE,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: CHANGE_USER_ROLE_ERROR,
            payload: error.message
        });
    }
};

export const createMessage = (
    sender: string,
    receivers: string[],
    files: File[],
    description: string
): ThunkAction<
    Promise<MessageActions>,
    AppState,
    void,
    MessageActions
> => async dispatch => {
    const form = new FormData();
    form.append('sender', sender);
    form.append('receivers', receivers.toString());
    files.map(file => form.append('filesToAdd', file));
    form.append('description', description);
    try {
        const response = await axios.post(
            'http://localhost:62795/api/message',
            form,
            {
                headers: { 'Content-Type': 'multipart/form-data' }
            }
        );
        return dispatch({
            type: CREATE_MESSAGE,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: CREATE_MESSAGE_ERROR,
            payload: error.message
        });
    }
};

export const downloadFile = (
    fileId: string,
    fileName: string
): ThunkAction<
    Promise<MessageActions>,
    AppState,
    void,
    MessageActions
> => async dispatch => {
    try {
        const response = await axios.get(
            'http://localhost:62795/api/message/download',
            {
                params: {
                    fileId
                }
            }
        );
        // SAVING FILE;
        const file = new File([response.data], fileName, {
            type: response.headers['content-type']
        });
        const url = window.URL.createObjectURL(file);
        const a = document.createElement('a');
        a.href = url;
        a.download = file.name;
        a.click();
        a.remove();
        document.addEventListener('focus', w =>
            window.URL.revokeObjectURL(url)
        );
        //
        return dispatch({
            type: DOWNLOAD_FILE,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: DOWNLOAD_FILE_ERROR,
            payload: error.message
        });
    }
};

export const getAllUsers = (): ThunkAction<
    Promise<UserActions>,
    AppState,
    void,
    UserActions
> => async dispatch => {
    try {
        const response = await axios.get('http://localhost:62795/Users');
        return dispatch({
            type: GET_ALL_USERS,
            payload: response.data
        });
    } catch (error) {
        return dispatch({
            type: GET_ALL_USERS_ERROR,
            payload: error.message
        });
    }
};

export const getMessagesForUser = (
    userMail: string
): ThunkAction<
    Promise<MessageActions>,
    AppState,
    void,
    MessageActions
> => async dispatch => {
    try {
        const response = await axios.get(
            'http://localhost:62795/api/message/messagesforuser',
            {
                params: {
                    userMail
                }
            }
        );
        return dispatch({
            type: GET_MESSAGES_FOR_USER,
            payload: response.data
        });
    } catch (error) {
        return dispatch({
            type: GET_MESSAGES_FOR_USER_ERROR,
            payload: error.message
        });
    }
};

export const getNotAcceptedMessages = (): ThunkAction<
    Promise<MessageActions>,
    AppState,
    void,
    MessageActions
> => async dispatch => {
    try {
        const response = await axios.get('http://localhost:62795/api/message');
        return dispatch({
            type: GET_NOT_ACCEPTED_MESSAGES,
            payload: response.data
        });
    } catch (error) {
        return dispatch({
            type: GET_NOT_ACCEPTED_MESSAGES_ERROR,
            payload: error.message
        });
    }
};

export const registerUser = (
    email: string,
    login: string,
    password: string
): ThunkAction<
    Promise<LoginActions>,
    AppState,
    void,
    LoginActions
> => async dispatch => {
    const data = JSON.stringify({
        Login: login,
        Email: email,
        Password: password
    });
    try {
        const response = await axios.post(
            'http://localhost:62795/api/register/Register',
            data,
            {
                headers: { 'Content-type': 'application/json' }
            }
        );
        dispatch(authenticateUser(email, password));
        return dispatch({
            type: REGISTER_SUCCEEDED,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: REGISTER_FAILED,
            payload: error.message
        });
    }
};

export const rejectMessage = (
    messageId: string,
    descriptionWhyNotAccepted: string
): ThunkAction<
    Promise<MessageActions>,
    AppState,
    void,
    MessageActions
> => async dispatch => {
    try {
        const response = await axios.delete(
            'http://localhost:62795/api/message/delete',
            {
                params: {
                    messageId,
                    descriptionWhyNotAccepted
                }
            }
        );
        return dispatch({
            type: REJECT_MESSAGE,
            payload: messageId
        });
    } catch (error) {
        return dispatch({
            type: REJECT_MESSAGE_ERROR,
            payload: error.message
        });
    }
};

export const resetUserPassword = (
    email: string,
    password: string
): ThunkAction<
    Promise<UserActions>,
    AppState,
    void,
    UserActions
> => async dispatch => {
    try {
        const data = JSON.stringify({
            Email: email,
            Password: password
        });
        const response = await axios.post(
            'http://localhost:62795/Users/ResetPassword',
            data,
            { headers: { 'Content-Type': 'application/json' } }
        );
        dispatch(getAllUsers());
        return dispatch({
            type: RESET_USER_PASSWORD,
            payload: undefined
        });
    } catch (error) {
        return dispatch({
            type: RESET_USER_PASSWORD_ERROR,
            payload: error.message
        });
    }
};

export const signIn = (): SignActions => ({
    type: SIGN_IN
});

export const signOut = (): SignActions => ({
    type: SIGN_OUT
});
