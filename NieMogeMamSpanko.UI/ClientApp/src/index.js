import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import RootProvider from './components/RootProvider';

ReactDOM.render(
    <RootProvider>
        <App />
    </RootProvider>,
    document.querySelector('#root')
);
