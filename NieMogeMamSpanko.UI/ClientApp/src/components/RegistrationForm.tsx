import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Field,
    InjectedFormProps,
    reduxForm,
    WrappedFieldMetaProps,
    WrappedFieldProps
} from 'redux-form';
import { registerUser } from '../actions';
import { AppState, FormValues, LoginActions } from '../actions/types';

const renderError = ({ touched, error }: WrappedFieldMetaProps) => {
    if (touched && error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

const renderField = ({
    input,
    label,
    meta,
    type
}: WrappedFieldProps & { label: string; type: string }) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    return (
        <div className={className}>
            <label>{label}</label>
            <input {...input} autoComplete="off" type={type} />
            {renderError(meta)}
        </div>
    );
};

interface RegistrationFormProps {
    registerError: string | null;
    registerUser: (
        email: string,
        login: string,
        password: string
    ) => Promise<LoginActions>;
}

class RegistrationForm extends Component<
    RegistrationFormProps & InjectedFormProps<FormValues, RegistrationFormProps>
> {
    public onSubmit = (formValues: FormValues) => {
        const { email, login, password } = formValues;
        this.props.registerUser(email, login, password);
    };
    public render() {
        return (
            <form
                className="ui form error"
                onSubmit={this.props.handleSubmit(this.onSubmit)}
            >
                <Field
                    name="email"
                    component={renderField}
                    {...{
                        label: 'Email',
                        type: 'text'
                    }}
                />
                <Field
                    name="login"
                    component={renderField}
                    {...{
                        label: 'Login',
                        type: 'text'
                    }}
                />
                <Field
                    name="password"
                    component={renderField}
                    {...{
                        label: 'Hasło',
                        type: 'password'
                    }}
                />
                <Field
                    name="confirmPassword"
                    component={renderField}
                    {...{
                        label: 'Powtórz hasło',
                        type: 'password'
                    }}
                />
                <button className="ui primary button" type="submit">
                    Zarejestruj
                </button>
            </form>
        );
    }
}

const validate = (formValues: FormValues) => {
    let errors: FormValues;
    errors = {};
    const regex = // tslint:disable-next-line: quotemark
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
    if (!formValues.email) {
        errors.email = 'Musisz podać email';
    } else if (!formValues.email!.match(regex)) {
        errors.email = 'Musisz podać email w prawidłowym formacie';
    }
    if (!formValues.login) {
        errors.login = 'Musisz podać login';
    }
    if (!formValues.password) {
        errors.password = 'Musisz podać hasło';
    }
    if (
        !formValues.confirmPassword ||
        formValues.password !== formValues.confirmPassword
    ) {
        errors.confirmPassword = 'Musisz potwierdzić hasło';
    }
    return errors;
};

const mapStateToProps = (state: AppState) => ({
    registerError: state.auth.registerError
});

export { RegistrationForm };

export default connect(
    mapStateToProps,
    { registerUser }
)(
    reduxForm({
        form: 'RegistrationForm',
        validate
    })(RegistrationForm as any)
);
