import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import {
    acceptMessage,
    downloadFile,
    getNotAcceptedMessages,
    rejectMessage
} from '../actions';
import { AppState, Message, MessageActions, MyFile } from '../actions/types';
import FileElement from './FileElement';
import MessageCard from './MessageCard';
import Receiver from './Receiver';

const columns = [
    {
        key: 'date1',
        text: 'od najmłodszego',
        value: 0
    },
    {
        key: 'date2',
        text: 'od najstarszego',
        value: 1
    },
    {
        key: 'senderAZ',
        text: 'nadawca (A-Z)',
        value: 2
    },
    {
        key: 'senderZA',
        text: 'nadawca (Z-A)',
        value: 3
    }
];

interface AcceptMessagesPanelProps {
    acceptMessage: (messageId: string) => Promise<MessageActions>;
    downloadFile: (fileId: string, fileName: string) => Promise<MessageActions>;
    getNotAcceptedMessages: () => Promise<MessageActions>;
    messages: Message[];
    rejectMessage: (
        messageId: string,
        reason: string
    ) => Promise<MessageActions>;
}

interface AcceptMessagesPanelState {
    filterPhrase: string;
    selectedMessage: Message | null;
    sortColumn: number;
    textAreaColor: string;
    textAreaValue: string;
}

class AcceptMessagesPanel extends Component<
    AcceptMessagesPanelProps,
    AcceptMessagesPanelState
> {
    public state: AcceptMessagesPanelState = {
        filterPhrase: '',
        selectedMessage: null,
        sortColumn: 0,
        textAreaColor: 'transparent',
        textAreaValue: ''
    };
    public componentDidMount() {
        this.props.getNotAcceptedMessages();
    }
    public doesContainPhrase = (phrase: string, message: Message) => {
        const keys = Object.keys(message);
        return keys.find(key => {
            switch (key) {
                case 'sender':
                case 'description':
                    return message[key]
                        .toLowerCase()
                        .includes(phrase.toLowerCase());
                case 'messageReceivers':
                    if (
                        (message[key] as string[]).find(receiver => {
                            return receiver
                                .toLowerCase()
                                .includes(phrase.toLowerCase());
                        })
                    ) {
                        return true;
                    }
                    return false;
                case 'files':
                    if (
                        (message[key] as MyFile[]).find(file => {
                            return file.name
                                .toLowerCase()
                                .includes(phrase.toLowerCase());
                        })
                    ) {
                        return true;
                    }
                    return false;
                default:
                    return false;
            }
        });
    };
    public downloadFile = (id: string, fileName: string) => {
        this.props.downloadFile(id, fileName);
    };
    public onAcceptingMessage = () => {
        this.props.acceptMessage(this.state.selectedMessage!.id);
        this.setState({ selectedMessage: null });
    };
    public onRejectingMessage = () => {
        this.props.rejectMessage(
            this.state.selectedMessage!.id,
            this.state.textAreaValue
        );
        this.setState({ selectedMessage: null });
    };
    public renderButtons = () => (
        <div className="acceptOrReject" style={styles.messageButtons}>
            <div className="fluid ui buttons">
                <button
                    className="ui positive button"
                    onClick={() => this.onAcceptingMessage()}
                >
                    Zaakceptuj
                </button>
                <div className="or" data-text="lub" />
                <button
                    className="ui negative button"
                    onClick={() => {
                        if (this.state.textAreaValue) {
                            this.onRejectingMessage();
                        } else {
                            this.setState({ textAreaColor: '#E37A6F' });
                        }
                    }}
                >
                    Odrzuć
                </button>
            </div>
            <div style={styles.rejectTextArea}>
                <textarea
                    onChange={event => {
                        event.persist();
                        this.setState({
                            textAreaColor: 'transparent',
                            textAreaValue: event.target.value
                        });
                    }}
                    placeholder="Podaj powód odrzucenia..."
                    rows={8}
                    style={{
                        ...{
                            border: 'none',
                            outline: 'none',
                            resize: 'none',
                            width: '100%'
                        },
                        ...{ backgroundColor: this.state.textAreaColor }
                    }}
                    value={this.state.textAreaValue}
                    wrap="on"
                />
            </div>
        </div>
    );
    public renderMessages() {
        if (this.props.messages && this.props.messages.length) {
            return this.props.messages
                .filter(message =>
                    this.doesContainPhrase(this.state.filterPhrase, message)
                )
                .sort((a, b) => {
                    switch (this.state.sortColumn) {
                        case 0:
                            return a.date > b.date ? 1 : -1;
                        case 1:
                            return a.date > b.date ? -1 : 1;
                        case 2:
                            return a.sender.toLowerCase() >
                                b.sender.toLowerCase()
                                ? 1
                                : -1;
                        case 3:
                            return a.sender.toLowerCase() >
                                b.sender.toLowerCase()
                                ? -1
                                : 1;
                        default:
                            return a.date > b.date ? 1 : -1;
                    }
                })
                .map((message, index) => {
                    return (
                        <React.Fragment key={index}>
                            <MessageCard
                                message={message}
                                onMessageClick={selectedMessage =>
                                    this.setState({ selectedMessage })
                                }
                            />
                        </React.Fragment>
                    );
                });
        }
        return <div className="noMessages" />;
    }
    public renderMessageWindow() {
        if (this.state.selectedMessage) {
            const {
                sender,
                messageReceivers,
                files,
                description
            } = this.state.selectedMessage;
            return (
                <div className="messageWindow" style={styles.messageWindow}>
                    <div style={{ display: 'flex' }}>
                        <div style={styles.messageDetails}>
                            <div className="header">Nadawca:</div>
                            <div style={styles.content}>{sender}</div>
                            <div>
                                <div className="header">Odbiorcy:</div>
                                <div style={styles.content}>
                                    {messageReceivers.length ? (
                                        (messageReceivers as string[]).map(
                                            (receiver, receiverIndex) => {
                                                return (
                                                    <Receiver
                                                        key={receiverIndex}
                                                        value={receiver}
                                                    />
                                                );
                                            }
                                        )
                                    ) : (
                                        <Receiver
                                            value={messageReceivers as string}
                                        />
                                    )}
                                </div>
                            </div>
                            <div>
                                <div className="header">Pliki:</div>
                                <div style={styles.content}>
                                    {files.map((file, fileIndex) => {
                                        return (
                                            <FileElement
                                                fileId={file.id}
                                                key={fileIndex}
                                                onDownload={(id, fileName) =>
                                                    this.downloadFile(
                                                        id,
                                                        fileName
                                                    )
                                                }
                                                value={file}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="header">Opis:</div>
                            <div style={styles.content}>{description}</div>
                        </div>
                        <div
                            className="negative ui button"
                            onClick={() =>
                                this.setState({ selectedMessage: null })
                            }
                            style={styles.deleteButton}
                        >
                            &#10006;
                        </div>
                        <div style={{ clear: 'both' }} />
                        {this.renderButtons()}
                    </div>
                </div>
            );
        }
        return (
            <div className="emptyMessageWindow" style={styles.messageWindow}>
                <div style={styles.emptyMessageWindowText}>
                    Wybierz jedną z wiadomości po lewej
                </div>
            </div>
        );
    }
    public render() {
        return (
            <div>
                <div style={{ display: 'flex', float: 'right' }}>
                    <div style={styles.filterSorterInput}>
                        <div
                            className="header"
                            style={styles.filterSorterInput}
                        >
                            Sortuj
                        </div>
                    </div>
                    <div style={styles.filterSorterInput}>
                        <Dropdown
                            onChange={(e, d) => {
                                this.setState({
                                    sortColumn: d.value as number
                                });
                            }}
                            options={columns}
                            placeholder="Wybierz po czym sortować"
                            selection={true}
                            value={this.state.sortColumn}
                        />
                    </div>
                    <div style={styles.filterSorterInput}>
                        <div className="ui icon input">
                            <input
                                type="text"
                                placeholder="Filtruj..."
                                onChange={e =>
                                    this.setState({
                                        filterPhrase: e.target.value
                                    })
                                }
                                value={this.state.filterPhrase}
                            />
                            <i className="circular search link icon" />
                        </div>
                    </div>
                </div>
                <div style={{ clear: 'both' }} />
                <div style={{ display: 'flex' }}>
                    <div className="ui list" style={styles.messagesList}>
                        {this.renderMessages()}
                    </div>
                    {this.renderMessageWindow()}
                </div>
            </div>
        );
    }
}

const styles: { [key: string]: React.CSSProperties } = {
    content: {
        display: 'inline-block',
        marginLeft: '40px'
    },
    deleteButton: {
        float: 'right',
        height: 25,
        padding: 5,
        width: 25
    },
    emptyMessageWindowText: {
        fontSize: '20px',
        opacity: 0.6,
        textAlign: 'center'
    },
    filterSorterInput: {
        margin: '5px',
        padding: '5px'
    },
    messageButtons: {
        display: 'inline-block',
        width: '25%'
    },
    messageDetails: {
        display: 'inline-block',
        top: -50,
        width: '75%'
    },
    messagesList: {
        display: 'inline-block',
        width: '25%'
    },
    messageWindow: {
        alignItems: 'center',
        border: 'solid 2px #123456',
        borderRadius: 5,
        display: 'inline-block',
        justifyContent: 'center',
        margin: '5px',
        padding: '5px',
        width: '75%'
    },
    rejectTextArea: {
        border: 'solid 2px #234A3B',
        borderRadius: 5,
        margin: '5px 0px',
        padding: 5,
        width: '100%'
    }
};

const mapStateToProps = (state: AppState) => ({
    messages: state.message.notAcceptedMessages
});

export { AcceptMessagesPanel };

export default connect(
    mapStateToProps,
    {
        acceptMessage,
        downloadFile,
        getNotAcceptedMessages,
        rejectMessage
    }
)(AcceptMessagesPanel);
