import React from 'react';
import { MyFile } from '../actions/types';

interface FileElementProps {
    fileId: string;
    fileKey?: number;
    onDelete?: (key: number) => void;
    onDownload?: (fileId: string, fileName: string) => void;
    value: File;
}

const FileElement = (props: FileElementProps) => {
    const deleteFile = () => {
        if (props.fileKey !== undefined && props.onDelete) {
            props.onDelete(props.fileKey);
        }
    };
    const renderButton = () => {
        if (props.onDelete) {
            return (
                <div
                    className="negative ui button"
                    onClick={deleteFile}
                    style={styles.button}
                >
                    &#10006;
                </div>
                /** &#10006; -> X */
            );
        } else if (props.onDownload) {
            return (
                <div
                    className="downloadButton"
                    onClick={() =>
                        props.onDownload &&
                        props.onDownload(
                            props.fileId,
                            props.value.name || (props.value as MyFile).title
                        )
                    }
                    style={styles.button}
                >
                    <i className="download icon" />
                </div>
            );
        }
        return <div className="withoutButton" />;
    };
    const renderIcon = (type: string) => {
        switch (type) {
            case 'application/x-freearc':
            case 'application/x-bzip':
            case 'application/x-bzip2':
            case 'application/java-archive':
            case 'application/x-rar-compressed':
            case 'application/x-tar':
            case 'application/zip':
            case 'application/x-7z-compressed':
                return <i className="file archive outline icon" />;
            case 'audio/aac':
            case 'audio/midi':
            case 'audio/x-midi':
            case 'audio/mpeg':
            case 'audio/ogg':
            case 'audio/wav':
            case 'audio/webm':
            case 'audio/3gpp':
            case 'audio/3gpp2':
                return <i className="file audio outline icon" />;
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            case 'application/vnd.oasis.opendocument.spreadsheet':
                return <i className="file excel outline icon" />;
            case 'image/bmp':
            case 'image/gif':
            case 'image/vnd.microsoft.icon':
            case 'image/jpeg':
            case 'image/png':
            case 'image/svg+xml':
            case 'image/tiff':
            case 'image/webp':
                return <i className="file image outline icon" />;
            case 'application/pdf':
                return <i className="file pdf outline icon" />;
            case 'application/vnd.ms-powerpoint':
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
            case 'application/vnd.oasis.opendocument.presentation':
                return <i className="file powerpoint outline icon" />;
            case 'video/x-msvideo':
            case 'video/mpeg':
            case 'video/ogg':
            case 'video/webm':
            case 'video/3gpp':
            case 'video/3gpp2':
                return <i className="file video outline icon" />;
            case 'application/msword':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            case 'application/vnd.oasis.opendocument.text':
                return <i className="file word outline icon" />;
            default:
                return <i className="file outline icon" />;
        }
    };
    return (
        <div style={styles.container}>
            <div style={styles.icon}>{renderIcon(props.value.type)}</div>
            <div style={styles.value}>
                {props.value.name || (props.value as MyFile).title}
            </div>
            {renderButton()}
        </div>
    );
};

const styles: { [key: string]: React.CSSProperties } = {
    button: {
        display: 'inline',
        padding: 5,
        margin: '0 auto 0 auto'
    },
    container: {
        border: 'solid 2px #3C81A2',
        borderRadius: 5,
        display: 'inline-block',
        paddingBottom: 5,
        paddingTop: 5
    },
    icon: {
        display: 'inline'
    },
    value: {
        display: 'inline',
        marginLeft: 10,
        marginRight: 10
    }
};

export default FileElement;
