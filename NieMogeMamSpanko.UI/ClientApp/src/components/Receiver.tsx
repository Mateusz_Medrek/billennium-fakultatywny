import React from 'react';

interface ReceiverProps {
    receiverKey?: number;
    onDelete?: (key: number) => void;
    value: string;
}

const Receiver = (props: ReceiverProps) => {
    const deleteReceiver = () => {
        if (props.receiverKey !== undefined && props.onDelete) {
            props.onDelete(props.receiverKey);
        }
    };
    const renderButton = () => {
        if (props.onDelete) {
            return (
                <div
                    className="negative ui button"
                    onClick={deleteReceiver}
                    style={styles.deleteButton}
                >
                    &#10006;
                </div>
                /** &#10006; -> X */
            );
        }
        return <div />;
    };
    return (
        <div style={styles.container}>
            <div style={styles.value}>{props.value}</div>
            {renderButton()}
        </div>
    );
};

const styles: { [key: string]: React.CSSProperties } = {
    container: {
        border: 'solid 2px #3C81A2',
        borderRadius: 5,
        display: 'inline-block',
        paddingBottom: 5,
        paddingTop: 5
    },
    value: {
        display: 'inline',
        marginLeft: 10,
        marginRight: 10
    },
    deleteButton: {
        display: 'inline',
        padding: 5,
        margin: '0 auto 0 auto'
    }
};

export default Receiver;
