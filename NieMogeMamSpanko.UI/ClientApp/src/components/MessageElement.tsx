import React from 'react';
import { ReceivedMessage } from '../actions/types';

interface MessageElementProps {
    message: ReceivedMessage;
    onMessageClick?: (message: ReceivedMessage) => void;
}

const MessageElement = (props: MessageElementProps) => {
    const renderDescription = () => {
        const maxLength = 50;
        if (!props.message.description.length) {
            return 'Brak';
        } else if (props.message.description.length > maxLength) {
            return props.message.description.slice(0, maxLength) + '...';
        }
        return props.message.description;
    };
    return (
        <div
            className="item"
            onClick={() =>
                props.onMessageClick && props.onMessageClick(props.message)
            }
            style={styles.item}
        >
            <div className="header" style={styles.notFullViewHeaders}>
                Nadawca:
            </div>
            <div style={styles.content}>{props.message.sender}</div>
            <div className="header" style={styles.notFullViewHeaders}>
                Liczba plików:
            </div>
            <div style={styles.content}>{props.message.files.length}</div>
            <div className="header" style={styles.notFullViewHeaders}>
                Opis:
            </div>
            <div style={styles.content}>{renderDescription()}</div>
        </div>
    );
};

const styles: { [key: string]: React.CSSProperties } = {
    content: {
        marginLeft: '40px'
    },
    item: {
        border: 'solid 2px #123456',
        borderRadius: 10,
        margin: '5px',
        padding: '5px'
    },
    notFullViewHeaders: {
        margin: '0px 5px'
    }
};

export default MessageElement;
