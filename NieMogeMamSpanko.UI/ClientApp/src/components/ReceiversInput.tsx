import React, { Component } from 'react';
import { WrappedFieldInputProps } from 'redux-form';
import Receiver from './Receiver';

interface ReceiversInputState {
    error: string;
    inputValue: string;
    receivers: string[];
}

class ReceiversInput extends Component<
    WrappedFieldInputProps,
    ReceiversInputState
> {
    public state: ReceiversInputState = {
        error: '',
        inputValue: '',
        receivers: []
    };
    public onReceiverDelete(key: number) {
        const newReceivers = this.state.receivers.filter((receiver, index) => {
            if (key !== index) {
                return receiver;
            }
        });
        this.setState({ error: '', receivers: newReceivers });
        this.props.onChange(newReceivers.toString());
    }
    public renderReceivers() {
        if (this.state.receivers && this.state.receivers.length) {
            return this.state.receivers.map((receiver, index) => {
                return (
                    <Receiver
                        key={index}
                        onDelete={this.onReceiverDelete.bind(this)}
                        receiverKey={index}
                        value={receiver}
                    />
                );
            });
        }
        return <div />;
    }
    public onAddingReceiver() {
        const oldReceivers = this.state.receivers;
        const newReceiver = this.state.inputValue;
        const regex = // tslint:disable-next-line: quotemark
            "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
        if (oldReceivers.find(receiver => receiver === newReceiver)) {
            this.setState({
                error: 'Podałeś już tego odbiorcę'
                // inputValue: ''
            });
        } else if (!newReceiver.match(regex)) {
            this.setState({
                error: 'Musisz podać email w prawidłowym formacie'
                // inputValue: ''
            });
        } else {
            this.setState({
                error: '',
                inputValue: '',
                receivers: [...oldReceivers, newReceiver]
            });
            this.props.onChange([...oldReceivers, newReceiver].toString());
        }
    }
    public onInputChange(value: string) {
        // this.setState({ error: '', inputValue: event.target.value });
        const shouldAddReceiver = value[value.length - 1] === ';';
        this.setState({
            error: '',
            inputValue: value.replace(';', '')
        });
        if (shouldAddReceiver) {
            this.onAddingReceiver();
        }
    }
    public renderError() {
        if (!this.state.error) {
            return <div />;
        }
        return (
            <div className="ui error message">
                <div className="header">{this.state.error}</div>
            </div>
        );
    }
    public render() {
        return (
            <div
                style={{
                    border: '2px solid #3C81A2',
                    borderRadius: 10,
                    padding: 10
                }}
            >
                <div>{this.renderReceivers()}</div>
                <input
                    autoComplete="off"
                    onChange={e => {
                        e.preventDefault();
                        this.onInputChange(e.target.value);
                    }}
                    onKeyDown={e => {
                        if (e.keyCode === 13) {
                            e.preventDefault();
                            this.onAddingReceiver();
                        }
                    }}
                    onSubmit={e => e.preventDefault()}
                    type="text"
                    style={{ border: 'none', width: '100%' }}
                    value={this.state.inputValue}
                />
                {this.renderError()}
                <input hidden={true} {...this.props} />
            </div>
        );
    }
}

export default ReceiversInput;
