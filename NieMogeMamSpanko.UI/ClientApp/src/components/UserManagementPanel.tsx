import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import {
    changeUserEmail,
    changeUserLogin,
    changeUserRole,
    getAllUsers,
    resetUserPassword
} from '../actions';
import { AppState, User, UserActions } from '../actions/types';
import UserCard from './UserCard';

const renderError = (error: string) => {
    if (error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

interface UserManagementPanelProps {
    changeUserEmail: (
        oldEmail: string,
        newEmail: string
    ) => Promise<UserActions>;
    changeUserEmailError: string;
    changeUserLogin: (email: string, newLogin: string) => Promise<UserActions>;
    changeUserLoginError: string;
    changeUserRole: (email: string) => Promise<UserActions>;
    changeUserRoleError: string;
    getAllUsers: () => Promise<UserActions>;
    getAllUsersError: string;
    resetUserPassword: (
        email: string,
        password: string
    ) => Promise<UserActions>;
    resetUserPasswordError: string;
    users: User[];
}

interface UserManagementPanelState {
    error: string;
    filterPhrase: string;
    newEmailValue: string;
    newLoginValue: string;
    newPasswordValue: string;
    selectedUser: User | null;
    sortColumn: number;
}

const columns = [
    {
        key: 'loginAZ',
        text: 'login (A-Z)',
        value: 0
    },
    {
        key: 'loginZA',
        text: 'login (Z-A)',
        value: 1
    },
    {
        key: 'emailAZ',
        text: 'email (A-Z)',
        value: 2
    },
    {
        key: 'emailZA',
        text: 'email (Z-A)',
        value: 3
    },
    {
        key: 'roleAZ',
        text: 'rola (A-Z)',
        value: 4
    },
    {
        key: 'roleZA',
        text: 'rola (Z-A)',
        value: 5
    }
];

class UserManagementPanel extends Component<
    UserManagementPanelProps,
    UserManagementPanelState
> {
    public state: UserManagementPanelState = {
        error: '',
        filterPhrase: '',
        newEmailValue: '',
        newLoginValue: '',
        newPasswordValue: '',
        selectedUser: null,
        sortColumn: 0
    };
    public componentDidMount() {
        this.props.getAllUsers();
    }
    public doesContainPhrase = (phrase: string, user: User) => {
        const keys = Object.keys(user);
        return keys.find(key => {
            switch (key) {
                case 'email':
                case 'role':
                case 'userName':
                    return user[key]
                        .toLowerCase()
                        .includes(phrase.toLowerCase());
                default:
                    return false;
            }
        });
    };
    public onChangeUserEmail = () => {
        const regex = // tslint:disable-next-line: quotemark
            "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
        if (!this.state.newEmailValue) {
            this.setState({ error: 'Wpisz nowy email' });
        } else if (!this.state.newEmailValue.match(regex)) {
            this.setState({ error: 'Podany email ma nieprawidłowy format' });
        } else {
            this.props.changeUserEmail(
                this.state.selectedUser!.email,
                this.state.newEmailValue
            );
            this.setState({ error: '', newEmailValue: '' });
        }
    };
    public onChangeUserLogin = () => {
        if (!this.state.newLoginValue) {
            this.setState({ error: 'Wpisz nowy login' });
        } else {
            this.props.changeUserLogin(
                this.state.selectedUser!.email,
                this.state.newLoginValue
            );
            this.setState({ error: '', newLoginValue: '' });
        }
    };
    public onChangeUserRole = () => {
        this.props.changeUserRole(this.state.selectedUser!.email);
        const selectedUser = this.state.selectedUser!;
        if (selectedUser.role === 'User') {
            selectedUser.role = 'Admin';
        } else {
            selectedUser.role = 'User';
        }
        this.setState({ selectedUser });
    };
    public onEmailValueChange = (newEmailValue: string) => {
        this.setState({ newEmailValue });
    };
    public onLoginValueChange = (newLoginValue: string) => {
        this.setState({ newLoginValue });
    };
    public onPasswordValueChange = (newPasswordValue: string) => {
        this.setState({ newPasswordValue });
    };
    public onResetUserPassword = () => {
        if (!this.state.newPasswordValue) {
            this.setState({ error: 'Wpisz nowe hasło' });
        } else {
            this.props.resetUserPassword(
                this.state.selectedUser!.email,
                this.state.newPasswordValue
            );
            this.setState({ error: '', newPasswordValue: '' });
        }
    };
    public renderUsers() {
        if (this.props.users && this.props.users.length) {
            return this.props.users
                .filter(user =>
                    this.doesContainPhrase(this.state.filterPhrase, user)
                )
                .sort((a, b) => {
                    switch (this.state.sortColumn) {
                        case 0:
                            return a.userName.toLowerCase() >
                                b.userName.toLowerCase()
                                ? 1
                                : -1;
                        case 1:
                            return a.userName.toLowerCase() >
                                b.userName.toLowerCase()
                                ? -1
                                : 1;
                        case 2:
                            return a.email.toLowerCase() > b.email.toLowerCase()
                                ? 1
                                : -1;
                        case 3:
                            return a.email.toLowerCase() > b.email.toLowerCase()
                                ? -1
                                : 1;
                        case 4:
                            return a.role.toLowerCase() > b.role.toLowerCase()
                                ? 1
                                : -1;
                        case 5:
                            return a.role.toLowerCase() > b.role.toLowerCase()
                                ? -1
                                : 1;
                        default:
                            return a.userName.toLowerCase() >
                                b.userName.toLowerCase()
                                ? 1
                                : -1;
                    }
                })
                .map((user, index) => {
                    return (
                        <React.Fragment key={index}>
                            <UserCard
                                user={user}
                                onUserClick={selectedUser =>
                                    this.setState({ selectedUser })
                                }
                            />
                        </React.Fragment>
                    );
                });
        }
        return <div className="noUsers" />;
    }
    public renderUserWindow() {
        if (this.state.selectedUser) {
            const { email, userName: login, role } = this.state.selectedUser;
            let changeRoleButtonText = 'Zmień rolę na: ';
            if (role === 'User') {
                changeRoleButtonText += 'Admin';
            } else {
                changeRoleButtonText += 'User';
            }
            return (
                <div className="userWindow" style={styles.userWindow}>
                    <div style={{ display: 'flex' }}>
                        <div style={styles.userDetails}>
                            <div className="email" style={{ display: 'flex' }}>
                                <div style={{ width: '40%' }}>
                                    <div
                                        className="header"
                                        style={styles.headers}
                                    >
                                        Email:
                                    </div>
                                    <div style={styles.content}>{email}</div>
                                </div>
                                <div className="ui input">
                                    <input
                                        onChange={e => {
                                            e.preventDefault();
                                            this.onEmailValueChange(
                                                e.target.value
                                            );
                                        }}
                                        placeholder="Podaj nowy email"
                                        style={styles.input}
                                        type="text"
                                        value={this.state.newEmailValue}
                                    />
                                </div>
                                <button
                                    className="ui primary button"
                                    onClick={this.onChangeUserEmail}
                                    style={styles.button}
                                >
                                    Zmień email
                                </button>
                            </div>
                            <div className="login" style={{ display: 'flex' }}>
                                <div style={{ width: '40%' }}>
                                    <div
                                        className="header"
                                        style={styles.headers}
                                    >
                                        Login:
                                    </div>
                                    <div style={styles.content}>{login}</div>
                                </div>
                                <div className="ui input">
                                    <input
                                        onChange={e => {
                                            e.preventDefault();
                                            this.onLoginValueChange(
                                                e.target.value
                                            );
                                        }}
                                        placeholder="Podaj nowy login"
                                        style={styles.input}
                                        type="text"
                                        value={this.state.newLoginValue}
                                    />
                                </div>
                                <button
                                    className="ui primary button"
                                    onClick={this.onChangeUserLogin}
                                    style={styles.button}
                                >
                                    Zmień login
                                </button>
                            </div>
                            <div
                                className="password"
                                style={{ display: 'flex' }}
                            >
                                <div style={{ width: '40%' }}>
                                    <div
                                        className="header"
                                        style={styles.headers}
                                    >
                                        Rola:
                                    </div>
                                    <div style={styles.content}>{role}</div>
                                </div>
                                <div className="ui input">
                                    <input
                                        onChange={e => {
                                            e.preventDefault();
                                            this.onPasswordValueChange(
                                                e.target.value
                                            );
                                        }}
                                        placeholder="Podaj nowe hasło"
                                        style={styles.input}
                                        type="text"
                                        value={this.state.newPasswordValue}
                                    />
                                </div>
                                <button
                                    className="ui primary button"
                                    onClick={this.onResetUserPassword}
                                    style={styles.button}
                                >
                                    Zresetuj hasło na nowe
                                </button>
                            </div>
                        </div>
                        <div
                            className="negative ui button"
                            onClick={() =>
                                this.setState({ selectedUser: null })
                            }
                            style={styles.deleteButton}
                        >
                            &#10006;
                        </div>
                        <div style={{ clear: 'both' }} />
                    </div>
                    <div
                        className="primary ui button"
                        onClick={this.onChangeUserRole}
                        style={styles.button}
                    >
                        {changeRoleButtonText}
                    </div>
                </div>
            );
        }
        return (
            <div className="emptyUserWindow" style={styles.userWindow}>
                <div style={styles.emptyUserWindowText}>
                    Wybierz jednego z użytkowników po lewej
                </div>
            </div>
        );
    }
    public render() {
        return (
            <div>
                <div style={{ display: 'flex', float: 'right' }}>
                    <div style={styles.filterSorterInput}>
                        <div
                            className="header"
                            style={styles.filterSorterInput}
                        >
                            Sortuj
                        </div>
                    </div>
                    <div style={styles.filterSorterInput}>
                        <Dropdown
                            onChange={(e, d) => {
                                this.setState({
                                    sortColumn: d.value as number
                                });
                            }}
                            options={columns}
                            placeholder="Wybierz po czym sortować"
                            selection={true}
                            value={this.state.sortColumn}
                        />
                    </div>
                    <div style={styles.filterSorterInput}>
                        <div className="ui icon input">
                            <input
                                type="text"
                                placeholder="Filtruj..."
                                onChange={e =>
                                    this.setState({
                                        filterPhrase: e.target.value
                                    })
                                }
                                value={this.state.filterPhrase}
                            />
                            <i className="circular search link icon" />
                        </div>
                    </div>
                </div>
                <div style={{ clear: 'both' }} />
                <div style={{ display: 'flex' }}>
                    <div className="ui list" style={styles.usersList}>
                        {this.renderUsers()}
                    </div>
                    {this.renderUserWindow()}
                </div>
                {renderError(this.state.error)}
                {renderError(this.props.changeUserEmailError)}
                {renderError(this.props.changeUserLoginError)}
                {renderError(this.props.changeUserRoleError)}
                {renderError(this.props.resetUserPasswordError)}
            </div>
        );
    }
}

const styles: { [key: string]: React.CSSProperties } = {
    button: {
        margin: '5px',
        padding: '5px',
        width: '20%'
    },
    content: {
        display: 'inline-block',
        marginLeft: '40px'
    },
    deleteButton: {
        float: 'right',
        height: 25,
        padding: 5,
        width: 25
    },
    emptyUserWindowText: {
        fontSize: '20px',
        opacity: 0.6,
        textAlign: 'center'
    },
    filterSorterInput: {
        margin: '5px',
        padding: '5px'
    },
    input: {
        margin: '5px'
    },
    headers: {
        margin: '5px'
    },
    userDetails: {
        display: 'inline-block',
        top: -50,
        width: '100%'
    },
    usersList: {
        display: 'inline-block',
        width: '25%'
    },
    userWindow: {
        alignItems: 'center',
        border: 'solid 2px #123456',
        borderRadius: 5,
        display: 'inline-block',
        justifyContent: 'center',
        margin: '5px',
        padding: '5px',
        width: '75%'
    }
};

export { UserManagementPanel };

const mapStateToProps = (state: AppState) => ({
    changeUserEmailError: state.user.changeUserEmailError,
    changeUserLoginError: state.user.changeUserLoginError,
    changeUserRoleError: state.user.changeUserRoleError,
    getAllUsersError: state.user.getAllUsersError,
    resetUserPasswordError: state.user.resetUserPasswordError,
    users: state.user.users
});

export default connect(
    mapStateToProps,
    {
        changeUserEmail,
        changeUserLogin,
        changeUserRole,
        getAllUsers,
        resetUserPassword
    }
)(UserManagementPanel);
