import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from '../actions';
import { AppState } from '../actions/types';
import AcceptMessagesPanel from './AcceptMessagesPanel';
import AccountManagement from './AccountManagement';
import LoginForm from './LoginForm';
import MessageForm from './MessageForm';
import MessageInbox from './MessageInbox';
import Navbar from './Navbar';
import RegistrationForm from './RegistrationForm';
import UserManagementPanel from './UserManagementPanel';

interface AppProps {
    isSignedIn: boolean | null;
    role: string | null;
    signIn: () => void;
    signOut: () => void;
    userId: string | number | null;
}

interface AppComponentState {
    pageIndex: number;
}

class App extends Component<AppProps, AppComponentState> {
    public state: AppComponentState = {
        pageIndex: 1
    };
    public componentDidMount() {
        if (!this.props.userId) {
            this.onSignOut();
        } else {
            this.onSignIn();
        }
    }
    public componentDidUpdate(oldProps: AppProps) {
        if (oldProps.userId !== this.props.userId) {
            if (!this.props.userId) {
                this.onSignOut();
            } else {
                this.onSignIn();
            }
        }
    }
    public onSignIn = () => {
        this.setState({ pageIndex: 0 });
        this.props.signIn();
    };
    public onSignOut = () => {
        this.props.signOut();
    };
    public renderAcceptMessagesPanel = () => {
        return (
            <div className="ui segment">
                <AcceptMessagesPanel />
            </div>
        );
    };
    public renderAccountManagement = () => {
        return (
            <div className="ui segment">
                <AccountManagement />
            </div>
        );
    };
    public renderGuestContent = () => {
        switch (this.state.pageIndex) {
            case 0:
                return this.renderRegistrationForm();
            case 1:
                return this.renderLoginAndMessageForms();
        }
    };
    public renderGuestNavbar = () => {
        return (
            <div className="right menu">
                <div className="item">
                    <div
                        className="ui secondary button"
                        onClick={() => this.setState({ pageIndex: 0 })}
                    >
                        Zarejestruj się
                    </div>
                </div>
                <div className="item">
                    <div
                        className="ui green button"
                        onClick={() => this.setState({ pageIndex: 1 })}
                    >
                        Zaloguj się
                    </div>
                </div>
            </div>
        );
    };
    public renderContent = (signedIn: boolean | null) => {
        if (!signedIn || !this.props.role) {
            return (
                <React.Fragment>
                    <Navbar>{this.renderGuestNavbar()}</Navbar>
                    {this.renderGuestContent()}
                </React.Fragment>
            );
        }
        return (
            <React.Fragment>
                <Navbar>{this.renderLoggedInNavbar()}</Navbar>
                {this.renderLoggedInContent()}
            </React.Fragment>
        );
    };
    public renderLoggedInContent = () => {
        switch (this.state.pageIndex) {
            case 0:
                return this.renderMessageInbox();
            case 1:
                return this.renderMessageForm();
            case 2:
                return this.renderAccountManagement();
            case 3:
                return this.renderAcceptMessagesPanel();
            case 4:
                return this.renderUserManagementPanel();
        }
    };
    public renderLoggedInNavbar = () => {
        let itemsLimit: number;
        itemsLimit = 0;
        if (this.props.role === 'User') {
            itemsLimit = 3;
        } else if (this.props.role === 'Admin') {
            itemsLimit = 4;
        } else if (this.props.role === 'SuperAdmin') {
            itemsLimit = 5;
        }
        const items = [
            'Skrzynka odbiorcza',
            'Wyślij wiadomość',
            'Zarządzaj kontem',
            'Moderuj wiadomości',
            'Zarządzaj użytkownikami'
        ];
        return (
            <React.Fragment>
                {items.map((item, index) => {
                    if (index < itemsLimit) {
                        let buttonClassName: string;
                        if (index === this.state.pageIndex) {
                            buttonClassName = 'active item';
                        } else {
                            buttonClassName = 'item';
                        }
                        return (
                            <a
                                className={buttonClassName}
                                key={index}
                                onClick={() =>
                                    this.setState({ pageIndex: index })
                                }
                            >
                                {item}
                            </a>
                        );
                    }
                })}
                <div className="right menu">
                    <div className="item">
                        <div
                            className="ui red button right"
                            onClick={() => {
                                this.setState({ pageIndex: 1 });
                                this.onSignOut();
                            }}
                        >
                            Wyloguj się
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    };
    public renderLoginAndMessageForms = () => {
        return (
            <div className="ui segment">
                <div className="ui two column very relaxed grid">
                    <div className="column" style={{ height: '100%' }}>
                        <div className="ui header">Wyślij wiadomość</div>
                        <MessageForm />
                    </div>
                    <div className="column" style={{ height: '100%' }}>
                        <div className="ui header">Zaloguj się</div>
                        <LoginForm />
                    </div>
                </div>
                <div className="ui vertical divider">Lub</div>
            </div>
        );
    };
    public renderMessageForm = () => {
        return (
            <div className="ui segment">
                <MessageForm />
            </div>
        );
    };
    public renderMessageInbox = () => {
        return (
            <div className="ui segment">
                <MessageInbox />
            </div>
        );
    };
    public renderRegistrationForm = () => {
        return (
            <div className="ui segment" style={{ width: '100%' }}>
                <div style={{ margin: 'auto', width: '50vw' }}>
                    <RegistrationForm />
                </div>
            </div>
        );
    };
    public renderUserManagementPanel = () => {
        return (
            <div className="ui segment">
                <UserManagementPanel />
            </div>
        );
    };
    public render() {
        return (
            <div className="ui container" style={{ margin: '10px 0px' }}>
                {this.renderContent(this.props.isSignedIn)}
            </div>
        );
    }
}

export { App };

const mapStateToProps = (state: AppState) => ({
    isSignedIn: state.auth.isSignedIn,
    role: state.auth.role,
    userId: state.auth.userId
});

export default connect(
    mapStateToProps,
    {
        signIn,
        signOut
    }
)(App);
