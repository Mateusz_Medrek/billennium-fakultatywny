import React from 'react';
import { Provider } from 'react-redux';
import store from '../store';

const RootProvider = (props: { children: JSX.Element | JSX.Element[] }) => {
    return <Provider store={store}>{props.children}</Provider>;
};

export default RootProvider;
