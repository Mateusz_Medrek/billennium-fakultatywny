import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    Field,
    InjectedFormProps,
    reduxForm,
    WrappedFieldMetaProps,
    WrappedFieldProps
} from 'redux-form';
import { authenticateUser } from '../actions';
import { AppState, FormValues, LoginActions } from '../actions/types';

interface LoginFormProps {
    authenticateUser: (
        login: string,
        password: string
    ) => Promise<LoginActions>;
    loginError: string | null;
}

const renderError = ({ touched, error }: WrappedFieldMetaProps) => {
    if (touched && error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

const renderRequestError = (error: string | null) => {
    if (error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

const renderField = ({
    input,
    label,
    meta,
    type
}: WrappedFieldProps & { label: string; type: string }) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    return (
        <div className={className}>
            <label>{label}</label>
            <input {...input} autoComplete="off" type={type} />
            {renderError(meta)}
        </div>
    );
};

class LoginForm extends Component<
    LoginFormProps & InjectedFormProps<FormValues, LoginFormProps>
> {
    public onSubmit = (formValues: FormValues) => {
        this.props.authenticateUser(formValues.email, formValues.password);
    };
    public render() {
        return (
            <form
                className="ui form error"
                onSubmit={this.props.handleSubmit(this.onSubmit)}
            >
                <Field
                    name="email"
                    component={renderField}
                    type="text"
                    {...{
                        label: 'Email'
                    }}
                />
                <Field
                    name="password"
                    component={renderField}
                    type="password"
                    {...{
                        label: 'Hasło'
                    }}
                />
                <button className="ui primary button" type="submit">
                    Zaloguj
                </button>
                {renderRequestError(this.props.loginError)}
            </form>
        );
    }
}

const validate = (formValues: FormValues) => {
    let errors: FormValues;
    errors = {};
    if (!formValues.email) {
        errors.email = 'Musisz podać email';
    }
    if (!formValues.password) {
        errors.password = 'Musisz podać hasło';
    }
    return errors;
};

const mapStateToProps = (state: AppState) => ({
    loginError: state.auth.loginError
});

export { LoginForm };

export default connect(
    mapStateToProps,
    {
        authenticateUser
    }
)(
    reduxForm({
        form: 'LoginForm',
        validate
    })(LoginForm as any)
);
