import React from 'react';
import { User } from '../actions/types';

interface UserCardProps {
    onUserClick?: (user: User) => void;
    user: User;
}

const UserCard = (props: UserCardProps) => {
    return (
        <div
            className="item"
            onClick={() => props.onUserClick && props.onUserClick(props.user)}
            style={styles.item}
        >
            <div className="header" style={styles.notFullViewHeaders}>
                Login:
            </div>
            <div style={styles.content}>{props.user.userName}</div>
            <div className="header" style={styles.notFullViewHeaders}>
                Email:
            </div>
            <div style={styles.content}>{props.user.email}</div>
            <div className="header" style={styles.notFullViewHeaders}>
                Rola:
            </div>
            <div style={styles.content}>{props.user.role}</div>
        </div>
    );
};

const styles: { [key: string]: React.CSSProperties } = {
    content: {
        marginLeft: '40px'
    },
    item: {
        border: 'solid 2px #123456',
        borderRadius: 10,
        margin: '5px',
        padding: '5px'
    },
    notFullViewHeaders: {
        margin: '0px 5px'
    }
};

export default UserCard;
