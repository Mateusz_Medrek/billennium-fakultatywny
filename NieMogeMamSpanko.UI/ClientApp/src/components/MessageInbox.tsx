import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dropdown } from 'semantic-ui-react';
import { downloadFile, getMessagesForUser } from '../actions';
import {
    AppState,
    MessageActions,
    MyFile,
    ReceivedMessage
} from '../actions/types';
import FileElement from './FileElement';
import MessageElement from './MessageElement';

const columns = [
    {
        key: 'date1',
        text: 'od najmłodszego',
        value: 0
    },
    {
        key: 'date2',
        text: 'od najstarszego',
        value: 1
    },
    {
        key: 'senderAZ',
        text: 'nadawca (A-Z)',
        value: 2
    },
    {
        key: 'senderZA',
        text: 'nadawca (Z-A)',
        value: 3
    }
];

interface MessageInboxProps {
    downloadFile: (fileId: string, fileName: string) => Promise<MessageActions>;
    getMessagesForUser: (userMail: string) => Promise<MessageActions>;
    messages: ReceivedMessage[];
    userMail: string | null;
}

interface MessageInboxState {
    filterPhrase: string;
    selectedMessage: ReceivedMessage | null;
    sortColumn: number;
}

class MessageInbox extends Component<MessageInboxProps, MessageInboxState> {
    public state: MessageInboxState = {
        filterPhrase: '',
        selectedMessage: null,
        sortColumn: 0
    };
    public componentDidMount() {
        this.props.getMessagesForUser(this.props.userMail!);
        // TODO: dopisać przycisk odświeżający skrzynkę;
    }
    public doesContainPhrase = (phrase: string, message: ReceivedMessage) => {
        const keys = Object.keys(message);
        return keys.find(key => {
            switch (key) {
                case 'sender':
                case 'description':
                    return message[key]
                        .toLowerCase()
                        .includes(phrase.toLowerCase());
                case 'files':
                    if (
                        (message[key] as MyFile[]).find(file => {
                            return file.title
                                .toLowerCase()
                                .includes(phrase.toLowerCase());
                        })
                    ) {
                        return true;
                    }
                    return false;
                default:
                    return false;
            }
        });
    };
    public downloadFile = (id: string, fileName: string) => {
        this.props.downloadFile(id, fileName);
    };
    public renderMessages() {
        if (this.props.messages && this.props.messages.length) {
            return this.props.messages
                .filter(message =>
                    this.doesContainPhrase(this.state.filterPhrase, message)
                )
                .sort((a, b) => {
                    switch (this.state.sortColumn) {
                        case 0:
                            return a.date > b.date ? 1 : -1;
                        case 1:
                            return a.date > b.date ? -1 : 1;
                        case 2:
                            return a.sender.toLowerCase() >
                                b.sender.toLowerCase()
                                ? 1
                                : -1;
                        case 3:
                            return a.sender.toLowerCase() >
                                b.sender.toLowerCase()
                                ? -1
                                : 1;
                        default:
                            return a.date > b.date ? 1 : -1;
                    }
                })
                .map((message, index) => {
                    return (
                        <React.Fragment key={index}>
                            <MessageElement
                                message={message}
                                onMessageClick={selectedMessage =>
                                    this.setState({ selectedMessage })
                                }
                            />
                        </React.Fragment>
                    );
                });
        }
        return <div />;
    }
    public renderMessageWindow() {
        if (this.state.selectedMessage) {
            const { sender, files, description } = this.state.selectedMessage;
            return (
                <div className="messageWindow" style={styles.messageWindow}>
                    <div style={styles.messageDetails}>
                        <div className="header">Nadawca:</div>
                        <div style={styles.content}>{sender}</div>
                        <div>
                            <div className="header">Pliki:</div>
                            <div style={styles.content}>
                                {files.map((file, fileIndex) => {
                                    return (
                                        <FileElement
                                            fileId={file.id}
                                            key={fileIndex}
                                            onDownload={(id, fileName) =>
                                                this.downloadFile(id, fileName)
                                            }
                                            value={file}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                        <div className="header">Opis:</div>
                        <div style={styles.content}>{description}</div>
                    </div>
                    <div
                        className="negative ui button"
                        onClick={() => this.setState({ selectedMessage: null })}
                        style={styles.deleteButton}
                    >
                        &#10006;
                    </div>
                    <div style={{ clear: 'both' }} />
                </div>
            );
        }
        return (
            <div className="emptyMessageWindow" style={styles.messageWindow}>
                <div style={styles.emptyMessageWindowText}>
                    Wybierz jedną z wiadomości po lewej
                </div>
            </div>
        );
    }
    public render() {
        return (
            <div>
                <div style={{ display: 'flex', float: 'right' }}>
                    <div style={styles.filterSorterInput}>
                        <div
                            className="header"
                            style={styles.filterSorterInput}
                        >
                            Sortuj
                        </div>
                    </div>
                    <div style={styles.filterSorterInput}>
                        <Dropdown
                            onChange={(e, d) => {
                                this.setState({
                                    sortColumn: d.value as number
                                });
                            }}
                            options={columns}
                            placeholder="Wybierz po czym sortować"
                            selection={true}
                            value={this.state.sortColumn}
                        />
                    </div>
                    <div style={styles.filterSorterInput}>
                        <div className="ui icon input">
                            <input
                                type="text"
                                placeholder="Filtruj..."
                                onChange={e =>
                                    this.setState({
                                        filterPhrase: e.target.value
                                    })
                                }
                                value={this.state.filterPhrase}
                            />
                            <i className="circular search link icon" />
                        </div>
                    </div>
                </div>
                <div style={{ clear: 'both' }} />
                <div style={{ display: 'flex' }}>
                    <div className="ui list" style={styles.messagesList}>
                        {this.renderMessages()}
                    </div>
                    {this.renderMessageWindow()}
                </div>
            </div>
        );
    }
}

const styles: { [key: string]: React.CSSProperties } = {
    content: {
        display: 'inline-block',
        marginLeft: '80px'
    },
    deleteButton: {
        display: 'inline',
        float: 'right',
        padding: 5
    },
    emptyMessageWindowText: {
        fontSize: '20px',
        opacity: 0.6,
        textAlign: 'center'
    },
    filterSorterInput: {
        margin: '5px',
        padding: '5px'
    },
    messageDetails: {
        display: 'inline-block',
        width: '75%'
    },
    messagesList: {
        display: 'inline-block',
        width: '35%'
    },
    messageWindow: {
        alignItems: 'center',
        border: 'solid 2px #123456',
        borderRadius: 5,
        display: 'inline-block',
        justifyContent: 'center',
        margin: '5px',
        padding: '5px',
        width: '65%'
    }
};

export { MessageInbox };

const mapStateToProps = (state: AppState) => ({
    messages: state.message.messagesForUser,
    userMail: state.auth.email
});

export default connect(
    mapStateToProps,
    {
        downloadFile,
        getMessagesForUser
    }
)(MessageInbox);
