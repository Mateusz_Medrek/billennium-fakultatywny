import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeEmail, changeLogin, changePassword } from '../actions';
import { AppState, LoginActions } from '../actions/types';

const renderError = (error: string) => {
    if (error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

interface AccountManagementProps {
    changeEmail: (oldEmail: string, newEmail: string) => Promise<LoginActions>;
    changeLogin: (email: string, newLogin: string) => Promise<LoginActions>;
    changePassword: (
        email: string,
        oldPassword: string,
        newPassword: string
    ) => Promise<LoginActions>;
    userEmail: string | null;
    changeEmailError: string | null;
    changeLoginError: string | null;
    changePasswordError: string | null;
}

interface AccountManagementState {
    emailValue: string;
    error: string;
    loginValue: string;
    oldPasswordValue: string;
    newPasswordValue: string;
}

class AccountManagement extends Component<
    AccountManagementProps,
    AccountManagementState
> {
    public state: AccountManagementState = {
        emailValue: '',
        error: '',
        loginValue: '',
        oldPasswordValue: '',
        newPasswordValue: ''
    };
    public onSubmittingEmailChange = () => {
        const regex = // tslint:disable-next-line: quotemark
            "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
        const { emailValue } = this.state;
        if (!emailValue) {
            this.setState({ error: 'Musisz podać email' });
        } else if (!emailValue.match(regex)) {
            this.setState({
                error: 'Musisz podać email w prawidłowym formacie'
            });
        } else {
            this.props.changeEmail(this.props.userEmail!, emailValue);
            this.setState({ emailValue: '', error: '' });
        }
    };
    public onSubmittingLoginChange = () => {
        if (!this.state.loginValue) {
            this.setState({ error: 'Musisz podać login' });
        } else {
            this.props.changeLogin(
                this.props.userEmail!,
                this.state.loginValue
            );
            this.setState({ error: '', loginValue: '' });
        }
    };
    public onSubmittingPasswordChange = () => {
        const { oldPasswordValue, newPasswordValue } = this.state;
        if (!oldPasswordValue || !newPasswordValue) {
            this.setState({
                error: 'Musisz podać hasła w obydwu polach tekstowych'
            });
        } else {
            this.props.changePassword(
                this.props.userEmail!,
                oldPasswordValue,
                newPasswordValue
            );
            this.setState({
                error: '',
                oldPasswordValue: '',
                newPasswordValue: ''
            });
        }
    };
    public onChangingEmailValue = (emailValue: string) => {
        this.setState({ emailValue });
    };
    public onChangingLoginValue = (loginValue: string) => {
        this.setState({ loginValue });
    };
    public onChangingOldPasswordValue = (oldPasswordValue: string) => {
        this.setState({ oldPasswordValue });
    };
    public onChangingNewPasswordValue = (newPasswordValue: string) => {
        this.setState({ newPasswordValue });
    };
    public render() {
        return (
            <div>
                <div className="ui field" style={styles.field}>
                    <div>
                        <label style={styles.label}>Zmień email</label>
                    </div>
                    <div className="ui input">
                        <input
                            onChange={e => {
                                e.preventDefault();
                                this.onChangingEmailValue(e.target.value);
                            }}
                            placeholder="Podaj swój nowy email"
                            style={styles.input}
                            type="text"
                            value={this.state.emailValue}
                        />
                    </div>
                    <button
                        className="ui primary button"
                        onClick={this.onSubmittingEmailChange}
                        style={styles.button}
                    >
                        Zmień email
                    </button>
                </div>
                <div className="ui field" style={styles.field}>
                    <div>
                        <label style={styles.label}>Zmień login</label>
                    </div>
                    <div className="ui input">
                        <input
                            onChange={e => {
                                e.preventDefault();
                                this.onChangingLoginValue(e.target.value);
                            }}
                            placeholder="Podaj swój nowy login"
                            style={styles.input}
                            type="text"
                            value={this.state.loginValue}
                        />
                    </div>
                    <button
                        className="ui primary button"
                        onClick={this.onSubmittingLoginChange}
                        style={styles.button}
                    >
                        Zmień login
                    </button>
                </div>
                <div className="ui field" style={styles.field}>
                    <div>
                        <label style={styles.label}>Zmień hasło</label>
                    </div>
                    <div>
                        <div className="ui input" style={{ width: '100%' }}>
                            <input
                                onChange={e => {
                                    e.preventDefault();
                                    this.onChangingOldPasswordValue(
                                        e.target.value
                                    );
                                }}
                                placeholder="Podaj swoje dotychczasowe hasło"
                                style={styles.input}
                                type="password"
                                value={this.state.oldPasswordValue}
                            />
                        </div>
                        <div className="ui input">
                            <input
                                onChange={e => {
                                    e.preventDefault();
                                    this.onChangingNewPasswordValue(
                                        e.target.value
                                    );
                                }}
                                placeholder="Podaj swoje nowe hasło"
                                style={styles.input}
                                type="password"
                                value={this.state.newPasswordValue}
                            />
                        </div>
                        <button
                            className="ui primary button"
                            onClick={this.onSubmittingPasswordChange}
                            style={styles.button}
                        >
                            Zmień hasło
                        </button>
                    </div>
                </div>
                {renderError(this.state.error)}
                {renderError(this.props.changeEmailError || '')}
                {renderError(this.props.changeLoginError || '')}
                {renderError(this.props.changePasswordError || '')}
            </div>
        );
    }
}

const styles: { [key: string]: React.CSSProperties } = {
    button: {
        margin: '5px'
    },
    field: {
        display: 'inline-block',
        margin: '10px'
    },
    input: {
        margin: '5px'
    },
    label: {
        margin: '5px'
    }
};

const mapStateToProps = (state: AppState) => ({
    changeEmailError: state.auth.changeEmailError,
    changeLoginError: state.auth.changeLoginError,
    changePasswordError: state.auth.changePasswordError,
    userEmail: state.auth.email
});

export { AccountManagement };

export default connect(
    mapStateToProps,
    { changeEmail, changeLogin, changePassword }
)(AccountManagement);
