import React, { Component } from 'react';
import Dropzone, { DropzoneState } from 'react-dropzone';
import { connect } from 'react-redux';
import {
    Field,
    InjectedFormProps,
    reduxForm,
    reset,
    WrappedFieldMetaProps,
    WrappedFieldProps
} from 'redux-form';
import { createMessage } from '../actions';
import { AppState, MessageActions, MessageFormValues } from '../actions/types';
import FileElement from './FileElement';
import ReceiversInput from './ReceiversInput';

const renderDropzone = ({
    input: { value, ...input },
    label,
    meta,
    onFilesChange,
    type,
    files,
    onFileDeleted,
    filesError
}: WrappedFieldProps & {
    label: string;
    onFilesChange: (files: File[]) => void;
    type: string;
    files: File[];
    onFileDeleted: (key: number) => void;
    filesError: string[];
}) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    const renderFiles = () => {
        if (files && files.length) {
            return files.map((file, index) => {
                return (
                    <FileElement
                        fileId="ZmienicToId"
                        fileKey={index}
                        key={index}
                        onDelete={onFileDeleted}
                        value={file}
                    />
                );
            });
        }
        return <div />;
    };
    return (
        <div className={className}>
            <label>{label}</label>
            <div
                style={{
                    border: '2px solid #547598',
                    borderRadius: 10,
                    padding: 10
                }}
            >
                <div
                    style={{
                        display: 'inline-block'
                    }}
                >
                    <div style={{ display: 'inline', margin: '0 auto 0 auto' }}>
                        <Dropzone
                            onDrop={acceptedFiles => {
                                console.log(
                                    `I accepted: ${acceptedFiles} from MessageForm`
                                );
                                acceptedFiles.map((file, index) =>
                                    console.log(index, file)
                                );
                                onFilesChange(acceptedFiles);
                                input.onChange(acceptedFiles);
                            }}
                        >
                            {({
                                getRootProps,
                                getInputProps
                            }: DropzoneState) => (
                                <div {...getRootProps()}>
                                    <input
                                        {...getInputProps({
                                            ...input,
                                            type
                                        })}
                                    />
                                    <button onSubmit={e => e.stopPropagation()}>
                                        Naciśnij aby wybrać pliki z dysku
                                    </button>
                                    <p>lub przeciągnij je tutaj</p>
                                </div>
                            )}
                        </Dropzone>
                    </div>
                    <div style={{ display: 'inline' }}>{renderFiles()}</div>
                </div>
            </div>
            {renderError(meta)}
            {renderFilesError(filesError)}
        </div>
    );
};

const renderError = ({ touched, error }: WrappedFieldMetaProps) => {
    if (touched && error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

const renderField = ({
    readOnly,
    readOnlyValue,
    input,
    label,
    meta,
    type
}: WrappedFieldProps & {
    readOnly?: boolean;
    readOnlyValue?: string;
    label: string;
    type: string;
}) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    if (readOnly) {
        input.onChange(readOnlyValue);
        return (
            <div className={className} style={{ display: 'flex' }}>
                <label>{label}</label>
                <div style={{ marginLeft: 10, opacity: 0.6 }}>
                    {readOnlyValue}
                </div>
                <input
                    {...input}
                    autoComplete="off"
                    readOnly={readOnly}
                    type={type}
                    hidden={true}
                />
            </div>
        );
    }
    return (
        <div className={className}>
            <label>{label}</label>
            <input
                {...input}
                autoComplete="off"
                readOnly={readOnly}
                type={type}
            />
            {renderError(meta)}
        </div>
    );
};

const renderFilesError = (errors: string[]) => {
    if (errors && errors.length) {
        return (
            <div className="ui error message">
                <div className="header">
                    {errors.map((error, i) => (
                        <div key={i}>{error}</div>
                    ))}
                </div>
            </div>
        );
    }
};

const renderReceiversField = ({
    input,
    label,
    meta
}: WrappedFieldProps & {
    label: string;
}) => {
    const className = `field ${meta.error && meta.touched ? 'error' : ''}`;
    return (
        <div className={className}>
            <label>{label}</label>
            <ReceiversInput {...input} />
            {renderError(meta)}
        </div>
    );
};

const renderRequestError = (error: any) => {
    if (error) {
        return (
            <div className="ui error message">
                <div className="header">{error}</div>
            </div>
        );
    }
};

interface MessageFormProps {
    requestError: string | null;
    userEmail: string | null;
    messageCreate: (
        sender: string,
        receivers: string[],
        files: File[],
        description: string
    ) => Promise<MessageActions>;
}

interface MessageFormState {
    files: File[];
    filesError: string[];
}

class MessageForm extends Component<
    MessageFormProps & InjectedFormProps<MessageFormValues, MessageFormProps>,
    MessageFormState
> {
    public state: MessageFormState = {
        files: [],
        filesError: []
    };
    public areFilesEqual(file1: File, file2: File) {
        if (
            file1.lastModified !== file2.lastModified ||
            file1.name !== file2.name ||
            file1.size !== file2.size ||
            file1.type !== file2.type
        ) {
            return false;
        }
        return true;
    }
    public onFilesAdded = (files: File[]) => {
        const filteredFiles = files.filter(file => {
            const length = this.state.files.filter(stateFile =>
                this.areFilesEqual(stateFile, file)
            ).length;
            if (!length) {
                return true;
            }
            this.setState({
                filesError: [
                    ...this.state.filesError,
                    'Plik ' + file.name + ' już jest'
                ]
            });
            return false;
        });
        if (filteredFiles.length === files.length) {
            this.setState({ filesError: [] });
        }
        const newFiles = [...this.state.files, ...filteredFiles];
        this.setState({ files: newFiles });
    };
    public onFileDeleted = (key: number) => {
        const newFiles = this.state.files.filter((file, index) => {
            if (key !== index) {
                return file;
            }
        });
        this.setState({ files: newFiles });
    };
    public onSubmit = (formValues: MessageFormValues) => {
        const { sender, receivers, files, description } = formValues;
        this.props.messageCreate(
            sender,
            receivers.split(','),
            files,
            description
        );
        this.setState({ files: [] });
    };
    public render() {
        let readOnly = false;
        let readOnlyValue = '';
        if (this.props.userEmail) {
            readOnly = true;
            readOnlyValue = this.props.userEmail;
        }
        return (
            <React.Fragment>
                <form
                    className="ui form error"
                    onSubmit={this.props.handleSubmit(this.onSubmit)}
                >
                    <Field
                        name="sender"
                        component={renderField}
                        {...{
                            readOnly,
                            readOnlyValue,
                            label: 'Nadawca',
                            type: 'text'
                        }}
                    />
                    <Field
                        name="receivers"
                        component={renderReceiversField}
                        {...{
                            label: 'Odbiorcy'
                        }}
                    />
                    <Field
                        name="files"
                        component={renderDropzone}
                        value={undefined}
                        {...{
                            label: 'Pliki',
                            onFilesChange: this.onFilesAdded,
                            type: 'file',
                            files: this.state.files,
                            onFileDeleted: this.onFileDeleted,
                            filesError: this.state.filesError
                        }}
                    />
                    <Field
                        name="description"
                        component={renderField}
                        {...{
                            label: 'Opis (opcjonalny)',
                            type: 'text'
                        }}
                    />
                    <button className="ui primary button" type="submit">
                        Zatwierdź
                    </button>
                </form>
                {renderRequestError(this.props.requestError)}
            </React.Fragment>
        );
    }
}

const validate = (formValues: MessageFormValues) => {
    let errors: any;
    errors = {};
    const regex = // tslint:disable-next-line: quotemark
        "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$";
    if (!formValues.sender) {
        errors.sender = 'Musisz podać email';
    } else if (!formValues.sender!.match(regex)) {
        errors.sender = 'Musisz podać email w prawidłowym formacie';
    }
    if (!formValues.receivers) {
        errors.receivers = 'Musisz podać przynajmniej jednego odbiorcę';
    }
    if (!formValues.files) {
        errors.files = 'Musisz podać przynajmniej jeden plik';
    }
    return errors;
};

const mapStateToProps = (state: AppState) => ({
    requestError: state.message.error,
    userEmail: state.auth.email
});

export { MessageForm };

export default connect(
    mapStateToProps,
    {
        messageCreate: createMessage
    }
)(
    reduxForm({
        form: 'MessageForm',
        onSubmitSuccess: (result, dispatch) => dispatch(reset('MessageForm')),
        validate
    })(MessageForm as any)
);
