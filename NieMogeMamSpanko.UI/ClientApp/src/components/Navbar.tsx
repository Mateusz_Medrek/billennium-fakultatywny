import React from 'react';

interface NavbarProps {
    children: JSX.Element | JSX.Element[];
}

const Navbar = (props: NavbarProps) => {
    return <div className="ui blue three inverted menu">{props.children}</div>;
};

export default Navbar;
