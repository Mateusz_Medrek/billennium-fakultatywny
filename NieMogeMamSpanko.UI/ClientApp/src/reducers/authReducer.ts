import {
    AppState,
    CHANGE_EMAIL_FAILED,
    CHANGE_EMAIL_SUCCEEDED,
    CHANGE_LOGIN_FAILED,
    CHANGE_LOGIN_SUCCEEDED,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_SUCCEEDED,
    LOGIN_FAILED,
    LOGIN_SUCCEEDED,
    LoginActions,
    REGISTER_FAILED,
    REGISTER_SUCCEEDED,
    SIGN_IN,
    SIGN_OUT,
    SignActions
} from '../actions/types';

const initialState: AppState['auth'] = {
    changeEmailError: null,
    changeLoginError: null,
    changePasswordError: null,
    email: null,
    loginError: null,
    isSignedIn: null,
    registerError: null,
    role: null,
    userId: null
};

export default (
    state: AppState['auth'] = initialState,
    action: SignActions | LoginActions
) => {
    switch (action.type) {
        case LOGIN_SUCCEEDED:
            return {
                ...state,
                email: action.payload.email,
                loginError: null,
                registerError: null,
                role: action.payload.role,
                userId: action.payload.id
            };
        case SIGN_IN:
            return { ...state, isSignedIn: true };
        case LOGIN_FAILED:
            return { ...state, loginError: action.payload };
        case SIGN_OUT:
            return {
                ...state,
                isSignedIn: false,
                email: null,
                loginError: null,
                role: null,
                userId: null
            };
        case REGISTER_SUCCEEDED:
            return { ...state, registerError: null };
        case REGISTER_FAILED:
            return { ...state, registerError: action.payload };
        case CHANGE_EMAIL_SUCCEEDED:
            return { ...state, email: action.payload, changeEmailError: null };
        case CHANGE_EMAIL_FAILED:
            return { ...state, changeEmailError: action.payload };
        case CHANGE_LOGIN_SUCCEEDED:
            return { ...state, changeLoginError: null };
        case CHANGE_LOGIN_FAILED:
            return { ...state, changeLoginError: action.payload };
        case CHANGE_PASSWORD_SUCCEEDED:
            return { ...state, changePasswordError: null };
        case CHANGE_PASSWORD_FAILED:
            return { ...state, changePasswordError: action.payload };
        default:
            return state;
    }
};
