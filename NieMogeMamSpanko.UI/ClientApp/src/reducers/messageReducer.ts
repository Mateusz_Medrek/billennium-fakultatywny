import {
    ACCEPT_MESSAGE,
    ACCEPT_MESSAGE_ERROR,
    AppState,
    CREATE_MESSAGE,
    CREATE_MESSAGE_ERROR,
    DOWNLOAD_FILE,
    DOWNLOAD_FILE_ERROR,
    GET_MESSAGES_FOR_USER,
    GET_MESSAGES_FOR_USER_ERROR,
    GET_NOT_ACCEPTED_MESSAGES,
    GET_NOT_ACCEPTED_MESSAGES_ERROR,
    Message,
    MessageActions,
    ReceivedMessage,
    REJECT_MESSAGE,
    REJECT_MESSAGE_ERROR
} from '../actions/types';

const initialState: AppState['message'] = {
    error: null,
    messagesForUser: [] as ReceivedMessage[],
    notAcceptedMessages: [] as Message[]
};

export default (
    state: AppState['message'] = initialState,
    action: MessageActions
) => {
    switch (action.type) {
        case ACCEPT_MESSAGE:
        case REJECT_MESSAGE:
            return {
                ...state,
                error: null,
                notAcceptedMessages: state.notAcceptedMessages.filter(
                    message => message.id !== action.payload
                )
            };
        case CREATE_MESSAGE:
        case DOWNLOAD_FILE:
            return { ...state, error: null };
        case GET_MESSAGES_FOR_USER:
            return { ...state, messagesForUser: action.payload, error: null };
        case GET_NOT_ACCEPTED_MESSAGES:
            return {
                ...state,
                notAcceptedMessages: action.payload,
                error: null
            };
        case ACCEPT_MESSAGE_ERROR:
        case CREATE_MESSAGE_ERROR:
        case DOWNLOAD_FILE_ERROR:
        case REJECT_MESSAGE_ERROR:
            return { ...state, error: action.payload };
        case GET_MESSAGES_FOR_USER_ERROR:
            return {
                ...state,
                messagesForUser: [] as ReceivedMessage[],
                error: action.payload
            };
        case GET_NOT_ACCEPTED_MESSAGES_ERROR:
            return {
                ...state,
                notAcceptedMessages: [] as Message[],
                error: action.payload
            };
        default:
            return state;
    }
};
