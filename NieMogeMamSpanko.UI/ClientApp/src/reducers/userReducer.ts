import {
    AppState,
    CHANGE_USER_EMAIL,
    CHANGE_USER_EMAIL_ERROR,
    CHANGE_USER_LOGIN,
    CHANGE_USER_LOGIN_ERROR,
    CHANGE_USER_ROLE,
    CHANGE_USER_ROLE_ERROR,
    GET_ALL_USERS,
    GET_ALL_USERS_ERROR,
    RESET_USER_PASSWORD,
    RESET_USER_PASSWORD_ERROR,
    User,
    UserActions
} from '../actions/types';

const initialState: AppState['user'] = {
    changeUserEmailError: '',
    changeUserLoginError: '',
    changeUserRoleError: '',
    getAllUsersError: '',
    resetUserPasswordError: '',
    users: [] as User[]
};

export default (
    state: AppState['user'] = initialState,
    action: UserActions
) => {
    switch (action.type) {
        case CHANGE_USER_EMAIL:
            return { ...state, changeUserEmailError: '' };
        case CHANGE_USER_EMAIL_ERROR:
            return { ...state, changeUserEmailError: action.payload };
        case CHANGE_USER_LOGIN:
            return { ...state, changeUserLoginError: '' };
        case CHANGE_USER_LOGIN_ERROR:
            return { ...state, changeUserLoginError: action.payload };
        case CHANGE_USER_ROLE:
            return { ...state, changeUserRoleError: '' };
        case CHANGE_USER_ROLE_ERROR:
            return { ...state, changeUserRoleError: action.payload };
        case GET_ALL_USERS:
            return { ...state, getAllUsersError: '', users: action.payload };
        case GET_ALL_USERS_ERROR:
            return { ...state, getAllUsersError: action.payload };
        case RESET_USER_PASSWORD:
            return { ...state, resetUserPasswordError: '' };
        case RESET_USER_PASSWORD_ERROR:
            return { ...state, resetUserPasswordError: action.payload };
        default:
            return state;
    }
};
