import { shallow } from 'enzyme';
import React from 'react';
import RootProvider from '../components/RootProvider';

describe('RootProvider', () => {
    const App = () => {
        return <div>I am dummy component</div>;
    };
    it('should have a <Provider />', () => {
        const wrapper = shallow(
            <RootProvider>
                <App />
            </RootProvider>
        );
        expect(wrapper.find('Provider')).toHaveLength(1);
    });
    it('should have a <App />', () => {
        const wrapper = shallow(
            <RootProvider>
                <App />
            </RootProvider>
        );
        expect(wrapper.find('App')).toHaveLength(1);
    });
});
