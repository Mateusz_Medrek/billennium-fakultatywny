import { shallow } from 'enzyme';
import React from 'react';
import UserCard from '../components/UserCard';

const props = {
    user: {
        email: 'janusz@gmail.com',
        role: 'User',
        userName: 'Janusz69'
    }
};

describe('UserCard', () => {
    it('should render <UserCard />', () => {
        const styles: { [key: string]: React.CSSProperties } = {
            content: {
                marginLeft: '40px'
            },
            notFullViewHeaders: {
                margin: '0px 5px'
            }
        };
        const wrapper = shallow(<UserCard {...props} />);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Login:
                </div>
            )
        ).toBe(true);
        expect(wrapper.contains(props.user.userName)).toBe(true);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Email:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>{props.user.email}</div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Rola:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>{props.user.role}</div>
            )
        ).toBe(true);
    });
    it('should call onUserClick prop after clicking on one of users', () => {
        const onUserClick = jest.fn();
        const wrapper = shallow(
            <UserCard {...props} onUserClick={onUserClick} />
        );
        wrapper
            .find('div.item')
            .first()
            .simulate('click');
        expect(onUserClick).toBeCalledWith(props.user);
    });
});
