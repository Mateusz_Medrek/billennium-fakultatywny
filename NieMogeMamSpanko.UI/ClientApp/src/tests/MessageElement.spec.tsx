import { mount } from 'enzyme';
import React from 'react';
import MessageElement from '../components/MessageElement';

const props = {
    message: {
        sender: 'janusz@gmail.com',
        files: [
            {
                ...new File(['0o0101010101', '0o101010101101010'], 'file1.txt'),
                id: 'jakiesId',
                messageId: '3',
                title: 'file1.txt'
            },
            {
                ...new File(['0o0001001111', '0o11101100011100'], 'file2.txt'),
                id: 'inneId',
                messageId: '3',
                title: 'file2.txt'
            }
        ],
        description: '',
        id: '3',
        date: new Date('2019-05-12T13:16:46.581Z')
    }
};

describe('MessageElement', () => {
    it('should render sender', () => {
        const styles: { [key: string]: React.CSSProperties } = {
            notFullViewHeaders: {
                margin: '0px 5px'
            }
        };
        const wrapper = mount(<MessageElement {...props} />);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Nadawca:
                </div>
            )
        ).toBe(true);
        expect(wrapper.contains(props.message.sender)).toBe(true);
        wrapper.unmount();
    });
    it('should render shorten description', () => {
        const shortenProps = {
            message: {
                sender: 'janusz@gmail.com',
                files: [
                    {
                        ...new File(
                            ['0o0101010101', '0o101010101101010'],
                            'file1.txt'
                        ),
                        id: 'jakiesId',
                        messageId: '3',
                        title: 'file1.txt'
                    },
                    {
                        ...new File(
                            ['0o0001001111', '0o11101100011100'],
                            'file2.txt'
                        ),
                        id: 'inneId',
                        messageId: '3',
                        title: 'file2.txt'
                    }
                ],
                description:
                    'abcdefghijklmnoprstuvwxyzabcdefghijklmnoprstuvwxyz0123456789',
                id: '3',
                date: new Date('2019-05-12T13:16:46.581Z')
            }
        };
        const styles: { [key: string]: React.CSSProperties } = {
            content: {
                marginLeft: '40px'
            },
            notFullViewHeaders: {
                margin: '0px 5px'
            }
        };
        const wrapper = mount(<MessageElement {...shortenProps} />);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Opis:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>
                    {shortenProps.message.description.slice(0, 50) + '...'}
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render full description', () => {
        const fullProps = {
            message: {
                sender: 'janusz@gmail.com',
                files: [
                    {
                        ...new File(
                            ['0o0101010101', '0o101010101101010'],
                            'file1.txt'
                        ),
                        id: 'jakiesId',
                        messageId: '3',
                        title: 'file1.txt'
                    },
                    {
                        ...new File(
                            ['0o0001001111', '0o11101100011100'],
                            'file2.txt'
                        ),
                        id: 'inneId',
                        messageId: '3',
                        title: 'file2.txt'
                    }
                ],
                description: 'abcdefghijklmnoprstuvwxyz',
                id: '3',
                date: new Date('2019-05-12T13:16:46.581Z')
            }
        };
        const styles: { [key: string]: React.CSSProperties } = {
            content: {
                marginLeft: '40px'
            },
            notFullViewHeaders: {
                margin: '0px 5px'
            }
        };
        const wrapper = mount(<MessageElement {...fullProps} />);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Opis:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>
                    {fullProps.message.description}
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render "Brak" when description is not passed', () => {
        const styles: { [key: string]: React.CSSProperties } = {
            notFullViewHeaders: {
                margin: '0px 5px'
            }
        };
        const wrapper = mount(<MessageElement {...props} />);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Opis:
                </div>
            )
        ).toBe(true);
        expect(wrapper.contains('Brak')).toBe(true);
        wrapper.unmount();
    });
    it('should onMessageClick after clicking on the component', () => {
        const onMessageClick = jest.fn();
        const wrapper = mount(
            <MessageElement {...props} onMessageClick={onMessageClick} />
        );
        wrapper
            .find('div.item')
            .first()
            .simulate('click');
        expect(onMessageClick).toBeCalledWith(props.message);
        wrapper.unmount();
    });
});
