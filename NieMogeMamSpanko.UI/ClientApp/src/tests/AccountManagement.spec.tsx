import { mount, shallow } from 'enzyme';
import moxios from 'moxios';
import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import reduxThunk from 'redux-thunk';
import { AppState, Message, ReceivedMessage, User } from '../actions/types';
import ConnectedAccountManagement, {
    AccountManagement
} from '../components/AccountManagement';

const mockStore = configureMockStore<AppState>([reduxThunk]);

describe('AccountManagement', () => {
    it('should render 3 <label/> and 4 <input /> and 3 <button />', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        expect(wrapper.find('label')).toHaveLength(3);
        expect(wrapper.find('input')).toHaveLength(4);
        expect(wrapper.find('button')).toHaveLength(3);
        wrapper.unmount();
    });
    it('should call onChangingEmailValue after typing some value in email input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const onChangingEmailValue = jest.spyOn(
            wrapper
                .find('AccountManagement')
                .first()
                .instance() as AccountManagement,
            'onChangingEmailValue'
        );
        const inputValue = 'someValue';
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(onChangingEmailValue).toBeCalledWith(inputValue);
        wrapper.unmount();
    });
    it('should call onChangingLoginValue after typing some value in email input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const onChangingLoginValue = jest.spyOn(
            wrapper
                .find('AccountManagement')
                .first()
                .instance() as AccountManagement,
            'onChangingLoginValue'
        );
        const inputValue = 'someValue';
        shallow(wrapper.find('input').get(1)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(onChangingLoginValue).toBeCalledWith(inputValue);
        wrapper.unmount();
    });
    it('should call onChangingOldPasswordValue after typing some value in old password input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const onChangingOldPasswordValue = jest.spyOn(
            wrapper
                .find('AccountManagement')
                .first()
                .instance() as AccountManagement,
            'onChangingOldPasswordValue'
        );
        const inputValue = 'someValue';
        shallow(wrapper.find('input').get(2)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(onChangingOldPasswordValue).toBeCalledWith(inputValue);
        wrapper.unmount();
    });
    it('should call onChangingNewPasswordValue after typing some value in new password input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const onChangingNewPasswordValue = jest.spyOn(
            wrapper
                .find('AccountManagement')
                .first()
                .instance() as AccountManagement,
            'onChangingNewPasswordValue'
        );
        const inputValue = 'someValue';
        shallow(wrapper.find('input').get(3)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(onChangingNewPasswordValue).toBeCalledWith(inputValue);
        wrapper.unmount();
    });
    it('should call onSubmittingEmailChange after clicking on submit button', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const onSubmittingEmailChange = jest.spyOn(
            wrapper
                .find('AccountManagement')
                .first()
                .instance() as AccountManagement,
            'onSubmittingEmailChange'
        );
        const inputValue = 'someValue';
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        wrapper
            .find('button.primary')
            .first()
            .simulate('click');
        expect(onSubmittingEmailChange).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onSubmittingPasswordChange after clicking on submit button', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const onSubmittingPasswordChange = jest.spyOn(
            wrapper
                .find('AccountManagement')
                .first()
                .instance() as AccountManagement,
            'onSubmittingPasswordChange'
        );
        const inputValue = 'someValue';
        const otherInputValue = 'otherInputValue';
        shallow(wrapper.find('input').get(1)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        shallow(wrapper.find('input').get(2)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: otherInputValue }
        });
        wrapper
            .find('button')
            .last()
            .simulate('click');
        expect(onSubmittingPasswordChange).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should render error after not typing any value in email input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        wrapper
            .find('button')
            .first()
            .simulate('click');
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać email</div>
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render error after typing invalid value in email input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const inputValue = 'someInvalidValue';
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        wrapper
            .find('button')
            .first()
            .simulate('click');
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać email w prawidłowym formacie
                    </div>
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render error after not typing any value in login input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        shallow(wrapper.find('div.field').get(1))
            .find('button')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać login</div>
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render error after not typing any value in password inputs', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        wrapper
            .find('button')
            .last()
            .simulate('click');
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać hasła w obydwu polach tekstowych
                    </div>
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render error after email change was not succesful', () => {
        const errorMessage = 'Sth went wrong';
        const store = mockStore({
            auth: {
                changeEmailError: errorMessage,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        expect(wrapper.contains(errorMessage)).toBe(true);
        wrapper.unmount();
    });
    it('should render error after login change was not succesful', () => {
        const errorMessage = 'Sth went wrong';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: errorMessage,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        expect(wrapper.contains(errorMessage)).toBe(true);
        wrapper.unmount();
    });
    it('should render error after password change was not succesful', () => {
        const errorMessage = 'Sth went wrong';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: errorMessage,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        expect(wrapper.contains(errorMessage)).toBe(true);
        wrapper.unmount();
    });
    it('should not render error after email change was succesful', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const inputValue = 'someValue@gmail.com';
        moxios.install();
        moxios.stubRequest(new RegExp('/Users/Update'), {
            status: 200
        });
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(wrapper.find('div.error')).toHaveLength(0);
        moxios.uninstall();
        wrapper.unmount();
    });
    it('should not render error after login change was succesful', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const inputValue = 'someValue';
        moxios.install();
        moxios.stubRequest(new RegExp('/Users/ChangeLogin'), {
            status: 200
        });
        shallow(wrapper.find('input').get(1)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(wrapper.find('div.error')).toHaveLength(0);
        moxios.uninstall();
        wrapper.unmount();
    });
    it('should not render error after password change was succesful', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AccountManagement>(
            <Provider store={store}>
                <ConnectedAccountManagement />
            </Provider>
        );
        const inputValue = 'someValue';
        moxios.install();
        moxios.stubRequest(new RegExp('/Users/ChangePassword'), {
            status: 200
        });
        shallow(wrapper.find('input').get(2)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        shallow(wrapper.find('input').get(3)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: inputValue }
        });
        expect(wrapper.find('div.error')).toHaveLength(0);
        moxios.uninstall();
        wrapper.unmount();
    });
});
