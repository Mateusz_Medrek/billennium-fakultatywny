import {
    AppState,
    CHANGE_USER_EMAIL,
    CHANGE_USER_EMAIL_ERROR,
    CHANGE_USER_LOGIN,
    CHANGE_USER_LOGIN_ERROR,
    CHANGE_USER_ROLE,
    CHANGE_USER_ROLE_ERROR,
    GET_ALL_USERS,
    GET_ALL_USERS_ERROR,
    RESET_USER_PASSWORD,
    RESET_USER_PASSWORD_ERROR,
    User,
    UserActions
} from '../actions/types';
import userReducer from '../reducers/userReducer';

const users: User[] = [
    {
        email: 'janusz@gmail.com',
        userName: 'Janusz69',
        role: 'Admin'
    },
    {
        email: 'grazyna@gmail.com',
        userName: 'Grazyna84',
        role: 'User'
    },
    {
        email: 'pjoter@gmail.com',
        userName: 'Pjoter',
        role: 'User'
    }
];

describe('userReducer', () => {
    it('should return new piece of state after dispatching CHANGE_USER_EMAIL action', () => {
        const action: UserActions = {
            type: CHANGE_USER_EMAIL,
            payload: undefined
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserEmailError).toEqual('');
    });
    it('should return new piece of state after dispatching CHANGE_USER_EMAIL_ERROR action', () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: UserActions = {
            type: CHANGE_USER_EMAIL_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserEmailError).toEqual(errorMessage);
    });
    it('should return new piece of state after dispatching CHANGE_USER_LOGIN action', () => {
        const action: UserActions = {
            type: CHANGE_USER_LOGIN,
            payload: undefined
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserLoginError).toEqual('');
    });
    it('should return new piece of state after dispatching CHANGE_USER_LOGIN_ERROR action', () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: UserActions = {
            type: CHANGE_USER_LOGIN_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserLoginError).toEqual(errorMessage);
    });
    it('should return new piece of state after dispatching CHANGE_USER_ROLE action', () => {
        const action: UserActions = {
            type: CHANGE_USER_ROLE,
            payload: undefined
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserRoleError).toEqual('');
    });
    it('should return new piece of state after dispatching CHANGE_USER_ROLE_ERROR action', () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: UserActions = {
            type: CHANGE_USER_ROLE_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserRoleError).toEqual(errorMessage);
    });
    it('should return new piece of state after dispatching GET_ALL_USERS action', () => {
        const action: UserActions = {
            type: GET_ALL_USERS,
            payload: users
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.getAllUsersError).toEqual('');
        expect(newState.users).toEqual(users);
    });
    it('should return new piece of state after dispatching GET_ALL_USERS_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: UserActions = {
            type: GET_ALL_USERS_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.getAllUsersError).toEqual(errorMessage);
    });
    it('should return new piece of state after dispatching RESET_USER_PASSWORD action', () => {
        const action: UserActions = {
            type: RESET_USER_PASSWORD,
            payload: undefined
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.resetUserPasswordError).toEqual('');
    });
    it('should return new piece of state after dispatching RESET_USER_PASSWORD_ERROR action', () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: UserActions = {
            type: RESET_USER_PASSWORD_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.resetUserPasswordError).toEqual(errorMessage);
    });
    it('should return the same piece of state after dispatching some other action', () => {
        const action: any = {
            type: 'SOME_ACTION'
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(initialState, action);
        expect(newState.changeUserEmailError).toEqual(
            initialState.changeUserEmailError
        );
        expect(newState.changeUserLoginError).toEqual(
            initialState.changeUserEmailError
        );
        expect(newState.changeUserRoleError).toEqual(
            initialState.changeUserRoleError
        );
        expect(newState.getAllUsersError).toEqual(
            initialState.getAllUsersError
        );
        expect(newState.resetUserPasswordError).toEqual(
            initialState.resetUserPasswordError
        );
        expect(newState.users).toEqual(initialState.users);
    });
    it('should return initial piece of state after dispatching some other action', () => {
        const action: any = {
            type: 'SOME_OTHER_ACTION'
        };
        const initialState: AppState['user'] = {
            changeUserEmailError: '',
            changeUserLoginError: '',
            changeUserRoleError: '',
            getAllUsersError: '',
            resetUserPasswordError: '',
            users: [] as User[]
        };
        const newState: AppState['user'] = userReducer(undefined, action);
        expect(newState.changeUserEmailError).toEqual(
            initialState.changeUserEmailError
        );
        expect(newState.changeUserLoginError).toEqual(
            initialState.changeUserEmailError
        );
        expect(newState.changeUserRoleError).toEqual(
            initialState.changeUserRoleError
        );
        expect(newState.getAllUsersError).toEqual(
            initialState.getAllUsersError
        );
        expect(newState.resetUserPasswordError).toEqual(
            initialState.resetUserPasswordError
        );
        expect(newState.users).toEqual(initialState.users);
    });
});
