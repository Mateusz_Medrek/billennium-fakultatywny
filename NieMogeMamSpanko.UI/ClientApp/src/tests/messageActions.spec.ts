import moxios from 'moxios';
import configureMockStore from 'redux-mock-store';
import reduxThunk, { ThunkDispatch } from 'redux-thunk';
import {
    acceptMessage,
    createMessage,
    downloadFile,
    getMessagesForUser,
    getNotAcceptedMessages,
    rejectMessage
} from '../actions';
import {
    ACCEPT_MESSAGE,
    ACCEPT_MESSAGE_ERROR,
    AppState,
    CREATE_MESSAGE,
    CREATE_MESSAGE_ERROR,
    DOWNLOAD_FILE,
    DOWNLOAD_FILE_ERROR,
    GET_MESSAGES_FOR_USER,
    GET_MESSAGES_FOR_USER_ERROR,
    GET_NOT_ACCEPTED_MESSAGES,
    GET_NOT_ACCEPTED_MESSAGES_ERROR,
    Message,
    MessageActions,
    ReceivedMessage,
    REJECT_MESSAGE,
    REJECT_MESSAGE_ERROR,
    User
} from '../actions/types';

const mockStore = configureMockStore<AppState>([reduxThunk]);

describe('MessageActions', () => {
    let action: MessageActions;
    beforeEach(() => {
        moxios.install();
    });
    afterEach(() => {
        moxios.uninstall();
    });
    it('should return action with type ACCEPT_MESSAGE', async () => {
        const messageId = 'someMessageIdValue';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest(new RegExp('/api/message/accept'), {
            status: 200
        });
        action = {
            type: ACCEPT_MESSAGE,
            payload: messageId
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            acceptMessage(messageId)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type ACCEPT_MESSAGE_ERROR', async () => {
        const messageId = 'someMessageIdValue';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest(new RegExp('/api/message/accept'), {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        action = {
            type: ACCEPT_MESSAGE_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            acceptMessage(messageId)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type CREATE_MESSAGE', async () => {
        const sender = 'janusz@gmail.com';
        const receivers = [
            'halyna@gmail.com',
            'pjoter@gmail.com',
            'grazyna@gmail.com'
        ];
        const files: File[] = [
            new File(['0o0101010101', '0o101010101101010'], 'file1.txt'),
            new File(['0o0001001111', '0o11101100011100'], 'file2.txt')
        ];
        const description =
            'To jest wiadomość do mojej rodzinki, załonczam 2 pliki';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest('http://localhost:62795/api/message', {
            status: 200
        });
        action = {
            type: CREATE_MESSAGE,
            payload: undefined
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            createMessage(sender, receivers, files, description)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type CREATE_MESSAGE_ERROR', async () => {
        const sender = 'janusz@gmail.com';
        const receivers = [
            'halyna@gmail.com',
            'pjoter@gmail.com',
            'grazyna@gmail.com'
        ];
        const files: File[] = [
            new File(['0o0101010101', '0o101010101101010'], 'file1.txt'),
            new File(['0o0001001111', '0o11101100011100'], 'file2.txt')
        ];
        const description =
            'To jest wiadomość do mojej rodzinki, załonczam 2 pliki';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 415;
        moxios.stubRequest('http://localhost:62795/api/message', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        action = {
            type: CREATE_MESSAGE_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            createMessage(sender, receivers, files, description)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type DOWNLOAD_FILE', async () => {
        const fileData = '0o01111000101';
        const fileId = 'someFileIdValue';
        const fileName = 'plik1.txt';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest(new RegExp('/api/message/download'), {
            status: 200,
            headers: {
                'content-type': 'text/plain'
            },
            response: fileData
        });
        action = {
            type: DOWNLOAD_FILE,
            payload: undefined
        };
        window.URL.createObjectURL = jest.fn(val => 'url');
        (document.addEventListener = jest.fn()),
            (document.createElement = jest.fn(
                (): any => ({
                    href: '',
                    download: '',
                    click: jest.fn(),
                    remove: jest.fn()
                })
            ));
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            downloadFile(fileId, fileName)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type DOWNLOAD_FILE_ERROR', async () => {
        const fileId = 'someFileIdValue';
        const fileName = 'someFileName';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest(new RegExp('/api/message/download'), {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        action = {
            type: DOWNLOAD_FILE_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            downloadFile(fileId, fileName)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type GET_MESSAGES_FOR_USER', async () => {
        const userMail = 'janusz@gmail.com';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const response: ReceivedMessage[] = [];
        moxios.stubRequest(new RegExp('/api/message/messagesforuser'), {
            status: 200,
            response
        });
        action = {
            type: GET_MESSAGES_FOR_USER,
            payload: response
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            getMessagesForUser(userMail)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type GET_MESSAGES_FOR_USER_ERROR', async () => {
        const userMail = 'janusz@gmail.com';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest(new RegExp('/api/message/messagesforuser'), {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        action = {
            type: GET_MESSAGES_FOR_USER_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            getMessagesForUser(userMail)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type GET_NOT_ACCEPTED_MESSAGES', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const response: Message[] = [];
        moxios.stubRequest('http://localhost:62795/api/message', {
            status: 200,
            response
        });
        action = {
            type: GET_NOT_ACCEPTED_MESSAGES,
            payload: response
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            getNotAcceptedMessages()
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type GET_NOT_ACCEPTED_MESSAGES_ERROR', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest('http://localhost:62795/api/message', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        action = {
            type: GET_NOT_ACCEPTED_MESSAGES_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            getNotAcceptedMessages()
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type REJECT_MESSAGE', async () => {
        const messageId = 'someMessageIdValue';
        const descriptionWhyNotAccepted = 'Bo tak';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest(new RegExp('/api/message/delete'), {
            status: 200
        });
        action = {
            type: REJECT_MESSAGE,
            payload: messageId
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            rejectMessage(messageId, descriptionWhyNotAccepted)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with type REJECT_MESSAGE_ERROR', async () => {
        const messageId = 'someMessageIdValue';
        const reason = 'Bo tak';
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest(new RegExp('/api/message/delete'), {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        action = {
            type: REJECT_MESSAGE_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, MessageActions>)(
            rejectMessage(messageId, reason)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
});
