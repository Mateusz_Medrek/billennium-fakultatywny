import { mount } from 'enzyme';
import React from 'react';
import Receiver from '../components/Receiver';

describe('Receiver', () => {
    it('should render value and X button', () => {
        const props = {
            receiverKey: 2,
            onDelete: jest.fn(),
            value: 'someValue'
        };
        const wrapper = mount(<Receiver {...props} />);
        const styles: { [key: string]: React.CSSProperties } = {
            value: {
                display: 'inline',
                marginLeft: 10,
                marginRight: 10
            }
        };
        expect(
            wrapper.contains(<div style={styles.value}>{props.value}</div>)
        ).toBe(true);
        expect(wrapper.find('div.negative')).toHaveLength(1);
    });
    it('should render only value', () => {
        const props = {
            value: 'someValue'
        };
        const wrapper = mount(<Receiver {...props} />);
        const styles: { [key: string]: React.CSSProperties } = {
            value: {
                display: 'inline',
                marginLeft: 10,
                marginRight: 10
            }
        };
        expect(
            wrapper.contains(<div style={styles.value}>{props.value}</div>)
        ).toBe(true);
        expect(wrapper.find('div.negative')).toHaveLength(0);
    });
    it('should call onDelete prop after clicking on X button', () => {
        const props = {
            receiverKey: 2,
            onDelete: jest.fn(),
            value: 'someValue'
        };
        const wrapper = mount(<Receiver {...props} />);
        wrapper
            .find('div.negative')
            .first()
            .simulate('click');
        expect(props.onDelete).toBeCalledWith(props.receiverKey);
    });
});
