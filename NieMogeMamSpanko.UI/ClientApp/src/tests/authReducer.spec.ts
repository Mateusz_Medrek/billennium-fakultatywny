import {
    AppState,
    CHANGE_EMAIL_FAILED,
    CHANGE_EMAIL_SUCCEEDED,
    CHANGE_LOGIN_FAILED,
    CHANGE_LOGIN_SUCCEEDED,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_SUCCEEDED,
    LOGIN_FAILED,
    LOGIN_SUCCEEDED,
    LoginActions,
    REGISTER_FAILED,
    REGISTER_SUCCEEDED,
    SIGN_IN,
    SIGN_OUT,
    SignActions
} from '../actions/types';
import authReducer from '../reducers/authReducer';

describe('authReducer', () => {
    it('should return new auth piece of state after dispatching CHANGE_EMAIL_SUCCEEDED action', async () => {
        const newEmail = 'newEmail@gmail.com';
        const action: LoginActions = {
            type: CHANGE_EMAIL_SUCCEEDED,
            payload: newEmail
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.changeEmailError).toEqual(null);
    });
    it('should return new auth piece of state after dispatching CHANGE_EMAIL_FAILED action', async () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: LoginActions = {
            type: CHANGE_EMAIL_FAILED,
            payload: errorMessage
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.changeEmailError).toEqual(errorMessage);
    });
    it('should return new auth piece of state after dispatching CHANGE_LOGIN_SUCCEEDED action', async () => {
        const action: LoginActions = {
            type: CHANGE_LOGIN_SUCCEEDED,
            payload: undefined
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.changeLoginError).toEqual(null);
    });
    it('should return new auth piece of state after dispatching CHANGE_LOGIN_FAILED action', async () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: LoginActions = {
            type: CHANGE_LOGIN_FAILED,
            payload: errorMessage
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.changeLoginError).toEqual(errorMessage);
    });
    it('should return new auth piece of state after dispatching CHANGE_PASSWORD_SUCCEEDED action', async () => {
        const action: LoginActions = {
            type: CHANGE_PASSWORD_SUCCEEDED,
            payload: undefined
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.changePasswordError).toEqual(null);
    });
    it('should return new auth piece of state after dispatching CHANGE_PASSWORD_FAILED action', async () => {
        const statusError = 400;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: LoginActions = {
            type: CHANGE_PASSWORD_FAILED,
            payload: errorMessage
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.changePasswordError).toEqual(errorMessage);
    });
    it('should return new auth piece of state after dispatching LOGIN_SUCCEEDED action', async () => {
        const response = {
            data: {
                userName: 'Janusz69',
                role: 'admin',
                email: 'janusz@gmail.com',
                token: 'TOKENTOKENTOKENTOKENTOKENTOKENTOKENTOKENTOKENTOKEN',
                id: 69
            }
        };
        const action: LoginActions = {
            type: LOGIN_SUCCEEDED,
            payload: response.data
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.email).toEqual(response.data.email);
        expect(newState.role).toEqual(response.data.role);
        expect(newState.userId).toEqual(response.data.id);
    });
    it('should return new auth piece of state after dispatching LOGIN_FAILED action', async () => {
        const action: LoginActions = {
            type: LOGIN_FAILED,
            payload: 'Podano nieprawidłowe dane logowania'
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.loginError).toEqual(
            'Podano nieprawidłowe dane logowania'
        );
    });
    it('should return new auth piece of state after dispatching REGISTER_SUCCEEDED action', async () => {
        const action: LoginActions = {
            type: REGISTER_SUCCEEDED,
            payload: undefined
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.registerError).toEqual(initialState.registerError);
    });
    it('should return new auth piece of state after dispatching REGISTER_FAILED action', async () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: LoginActions = {
            type: REGISTER_FAILED,
            payload: errorMessage
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: false,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.registerError).toEqual(errorMessage);
    });
    it('should return new auth piece of state after dispatching SIGN_IN action', () => {
        const action: SignActions = {
            type: SIGN_IN
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: 'janusz@gmail.com',
            loginError: null,
            registerError: null,
            role: 'admin',
            isSignedIn: false,
            userId: 'someValue'
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.isSignedIn).toBe(true);
    });
    it('should return new auth piece of state after dispatching SIGN_OUT action', () => {
        const action: SignActions = {
            type: SIGN_OUT
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: 'janusz@gmail.com',
            loginError: null,
            registerError: null,
            role: 'admin',
            isSignedIn: true,
            userId: 'someValue'
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.isSignedIn).toBe(false);
        expect(newState.userId).toEqual(null);
    });
    it('should return the same piece of state after dispatching other action', () => {
        const action: any = {
            type: 'SOME_ACTION'
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: 'janusz@gmail.com',
            loginError: null,
            registerError: null,
            role: 'admin',
            isSignedIn: true,
            userId: 'someValue'
        };
        const newState: AppState['auth'] = authReducer(initialState, action);
        expect(newState.email).toEqual(initialState.email);
        expect(newState.loginError).toEqual(initialState.loginError);
        expect(newState.role).toEqual(initialState.role);
        expect(newState.isSignedIn).toEqual(initialState.isSignedIn);
        expect(newState.userId).toEqual(initialState.userId);
    });
    it('should return initial piece of state after dispatching other action', () => {
        const action: any = {
            type: 'SOME_ACTION'
        };
        const initialState: AppState['auth'] = {
            changeEmailError: null,
            changeLoginError: null,
            changePasswordError: null,
            email: null,
            loginError: null,
            registerError: null,
            role: null,
            isSignedIn: null,
            userId: null
        };
        const newState: AppState['auth'] = authReducer(undefined, action);
        expect(newState.email).toEqual(initialState.email);
        expect(newState.loginError).toEqual(initialState.loginError);
        expect(newState.role).toEqual(initialState.role);
        expect(newState.isSignedIn).toEqual(initialState.isSignedIn);
        expect(newState.userId).toEqual(initialState.userId);
    });
});
