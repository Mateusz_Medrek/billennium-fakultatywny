import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import reduxThunk from 'redux-thunk';
import wait from 'waait';
import { AppState, Message, User } from '../actions/types';
import ConnectedAcceptMessagesPanel, {
    AcceptMessagesPanel
} from '../components/AcceptMessagesPanel';
import RootProvider from '../components/RootProvider';

const mockStore = configureMockStore<AppState>([reduxThunk]);

const file = new File(['0o0101110010001', '0o111000011010100'], 'plik1.txt', {
    type: 'text/plain'
});

const messages: Message[] = [
    {
        id: '123',
        sender: 'janusz@gmail.com',
        messageReceivers: [
            'grazyna@gmail.com',
            'pjoter@gmail.com',
            'ksbdvkjsbvkjsbvkjsbdvksbvksjbdv',
            'abksjbvksbvksjbvksjbvksj',
            'ajbksjbvdkjsbdvkjsbdvksdvkjbskvskdbvkjsbdvkjsbvkjsdbv'
        ],
        files: [
            {
                id: 'jakieśId',
                lastModified: file.lastModified,
                messageId: '123',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            }
        ],
        description: 'Wiadomosc do mojej rodzinki',
        date: new Date('2019-05-12T13:16:46.581Z')
    },
    {
        id: '456',
        sender: 'somsiad@gmail.com',
        messageReceivers: ['janusz@gmail.com'],
        files: [
            {
                id: 'inneId',
                lastModified: file.lastModified,
                messageId: '456',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            },
            {
                id: 'jeszczeInneId',
                lastModified: file.lastModified,
                messageId: '456',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            }
        ],
        description: 'Dla mojego najlepszego somsiada, Janusza',
        date: new Date('2019-05-09T13:16:46.581Z')
    },
    {
        id: '789',
        sender: 'grazyna@gmail.com',
        messageReceivers: ['janusz@gmail.com'],
        files: [
            {
                id: 'dziwneId',
                lastModified: file.lastModified,
                messageId: '789',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            },
            {
                id: 'normalneId',
                lastModified: file.lastModified,
                messageId: '789',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            }
        ],
        description: 'Nie pij tyle',
        date: new Date('2019-05-06T13:16:46.581Z')
    }
];

describe('AcceptMessagesPanel', () => {
    it('should render sort label, sort <Dropdown /> and filter input with icon', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedAcceptMessagesPanel />
            </RootProvider>
        );
        const styles: { [key: string]: React.CSSProperties } = {
            filterSorterInput: {
                margin: '5px',
                padding: '5px'
            }
        };
        expect(
            wrapper.contains(
                <div className="header" style={styles.filterSorterInput}>
                    Sortuj
                </div>
            )
        ).toBe(true);
        expect(wrapper.find('Dropdown')).toHaveLength(1);
        expect(
            wrapper
                .find('div.input')
                .first()
                .find('input')
        ).toHaveLength(1);
        expect(
            wrapper.contains(<i className="circular search link icon" />)
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render emptyMessageWindow when none of messages are clicked', () => {
        const wrapper = mount<AcceptMessagesPanel>(
            <RootProvider>
                <ConnectedAcceptMessagesPanel />
            </RootProvider>
        );
        expect(
            wrapper
                .find('div.emptyMessageWindow')
                .first()
                .contains('Wybierz jedną z wiadomości po lewej')
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render noMessages <div /> when there are no messages', () => {
        const wrapper = mount<AcceptMessagesPanel>(
            <RootProvider>
                <ConnectedAcceptMessagesPanel />
            </RootProvider>
        );
        expect(wrapper.find('div.noMessages')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should have <MessageCard /> components rendered for every message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        await wait(0);
        expect(wrapper.find('MessageCard')).toHaveLength(
            (wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel).props.messages.length
        );
        wrapper.unmount();
    });
    it('should call doesContainPhrase for every message after typing some value in filter input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        const doesContainPhrase = jest.spyOn(
            wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel,
            'doesContainPhrase'
        );
        wrapper
            .find('div.input')
            .first()
            .find('input')
            .first()
            .simulate('change', { target: { value: 'someValue' } });
        wrapper.update();
        expect(doesContainPhrase).toBeCalledTimes(
            (wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel).props.messages.length
        );
        wrapper.unmount();
    });
    it('should render messageWindow after clicking on one of messages', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('MessageCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Nadawca:</div>)
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(
                    (wrapper
                        .find('AcceptMessagesPanel')
                        .first()
                        .instance() as AcceptMessagesPanel).state
                        .selectedMessage!.sender
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Odbiorcy:</div>)
        ).toBe(true);
        expect(wrapper.find('Receiver')).toHaveLength(
            (wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel).state.selectedMessage!
                .messageReceivers.length
        );
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Pliki:</div>)
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .find('FileElement')
        ).toHaveLength(
            (wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel).state.selectedMessage!.files
                .length
        );
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Opis:</div>)
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(
                    (wrapper
                        .find('AcceptMessagesPanel')
                        .first()
                        .instance() as AcceptMessagesPanel).state
                        .selectedMessage!.description
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.acceptOrReject')
                .first()
                .find('button.positive')
        ).toHaveLength(1);
        expect(
            wrapper
                .find('div.acceptOrReject')
                .first()
                .contains(<div className="or" data-text="lub" />)
        ).toBe(true);
        expect(wrapper.find('textarea')).toHaveLength(1);
        expect(
            wrapper
                .find('div.acceptOrReject')
                .first()
                .find('button.negative')
        ).toHaveLength(1);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .find('div.negative')
        ).toHaveLength(1);
        wrapper.unmount();
    });
    it('should call downloadFile prop after clicking on one of files icon', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        const downloadFile = jest.spyOn(
            wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel,
            'downloadFile'
        );
        await wait(0);
        wrapper
            .find('MessageCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.messageWindow')
            .first()
            .find('FileElement')
            .find('div.downloadButton')
            .first()
            .simulate('click');
        expect(downloadFile).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should make messageWindow empty after closing current selected message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('MessageCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.messageWindow')
            .first()
            .find('div.negative')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.emptyMessageWindow')
                .first()
                .contains('Wybierz jedną z wiadomości po lewej')
        ).toBe(true);
        wrapper.unmount();
    });
    it('should call acceptMessage action creator after accepting message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        const onAcceptingMessage = jest.spyOn(
            wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel,
            'onAcceptingMessage'
        );
        await wait(0);
        wrapper
            .find('MessageCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.acceptOrReject')
            .first()
            .find('button.positive')
            .first()
            .simulate('click');
        expect(onAcceptingMessage).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call rejectMessage action creator after rejecting message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        const onRejectingMessage = jest.spyOn(
            wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel,
            'onRejectingMessage'
        );
        await wait(0);
        wrapper
            .find('MessageCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('textarea')
            .first()
            .simulate('change', {
                persist: jest.fn(),
                target: { value: 'Bo tak' }
            });
        wrapper
            .find('div.acceptOrReject')
            .first()
            .find('button.negative')
            .first()
            .simulate('click');
        expect(onRejectingMessage).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should not call rejectMessage action creator when rejecting reason is not typed', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: messages
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<AcceptMessagesPanel>(
            <Provider store={store}>
                <ConnectedAcceptMessagesPanel />
            </Provider>
        );
        const onRejectingMessage = jest.spyOn(
            wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel,
            'onRejectingMessage'
        );
        await wait(0);
        wrapper
            .find('MessageCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.acceptOrReject')
            .first()
            .find('button.negative')
            .first()
            .simulate('click');
        expect(onRejectingMessage).toBeCalledTimes(0);
        expect(
            (wrapper
                .find('AcceptMessagesPanel')
                .first()
                .instance() as AcceptMessagesPanel).state.textAreaColor
        ).not.toEqual('trnasparent');
        wrapper.unmount();
    });
});
