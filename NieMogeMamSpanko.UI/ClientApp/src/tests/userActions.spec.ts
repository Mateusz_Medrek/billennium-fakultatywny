import moxios from 'moxios';
import configureMockStore from 'redux-mock-store';
import reduxThunk, { ThunkDispatch } from 'redux-thunk';
import {
    changeUserEmail,
    changeUserLogin,
    changeUserRole,
    getAllUsers,
    resetUserPassword
} from '../actions';
import {
    AppState,
    CHANGE_USER_EMAIL,
    CHANGE_USER_EMAIL_ERROR,
    CHANGE_USER_LOGIN,
    CHANGE_USER_LOGIN_ERROR,
    CHANGE_USER_ROLE,
    CHANGE_USER_ROLE_ERROR,
    GET_ALL_USERS,
    GET_ALL_USERS_ERROR,
    Message,
    ReceivedMessage,
    RESET_USER_PASSWORD,
    RESET_USER_PASSWORD_ERROR,
    User,
    UserActions
} from '../actions/types';

const mockStore = configureMockStore<AppState>([reduxThunk]);

describe('userActions', () => {
    beforeEach(() => {
        moxios.install();
    });
    afterEach(() => {
        moxios.uninstall();
    });
    it('should return action with CHANGE_USER_EMAIL type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest('http://localhost:62795/Users/Update', {
            status: 200
        });
        const oldEmail = 'someOldEmail@gmail.com';
        const newEmail = 'someEmail@gmail.com';
        const action: UserActions = {
            type: CHANGE_USER_EMAIL,
            payload: undefined
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            changeUserEmail(oldEmail, newEmail)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with CHANGE_USER_EMAIL_ERROR type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest('http://localhost:62795/Users/Update', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const oldEmail = 'someOldEmail@gmail.com';
        const newEmail = 'someInvalidEmail@gmail.com';
        const action: UserActions = {
            type: CHANGE_USER_EMAIL_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            changeUserEmail(oldEmail, newEmail)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with CHANGE_USER_LOGIN type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest('http://localhost:62795/Users/ChangeLogin', {
            status: 200
        });
        const oldLogin = 'someOldLogin';
        const newLogin = 'someLogin';
        const action: UserActions = {
            type: CHANGE_USER_LOGIN,
            payload: undefined
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            changeUserLogin(oldLogin, newLogin)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with CHANGE_USER_LOGIN_ERROR type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest('http://localhost:62795/Users/ChangeLogin', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const oldLogin = 'someOldLogin';
        const newLogin = 'someInvalidLogin';
        const action: UserActions = {
            type: CHANGE_USER_LOGIN_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            changeUserLogin(oldLogin, newLogin)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with CHANGE_USER_ROLE type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest('http://localhost:62795/Users/ChangeRole', {
            status: 200
        });
        const email = 'someEmail@gmail.com';
        const action: UserActions = {
            type: CHANGE_USER_ROLE,
            payload: undefined
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            changeUserRole(email)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with CHANGE_USER_ROLE_ERROR type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest('http://localhost:62795/Users/ChangeRole', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const email = 'someInvalidEmail@gmail.com';
        const action: UserActions = {
            type: CHANGE_USER_ROLE_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            changeUserRole(email)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with GET_ALL_USERS type', async () => {
        const users: User[] = [
            {
                email: 'janusz@gmail.com',
                userName: 'Janusz69',
                role: 'Admin'
            },
            {
                email: 'grazyna@gmail.com',
                userName: 'Grazyna84',
                role: 'User'
            },
            {
                email: 'pjoter@gmail.com',
                userName: 'Pjoter',
                role: 'User'
            }
        ];
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest('http://localhost:62795/Users', {
            status: 200,
            response: users
        });
        const action: UserActions = {
            type: GET_ALL_USERS,
            payload: users
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            getAllUsers()
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with GET_ALL_USERS_ERROR type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest('http://localhost:62795/Users', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: UserActions = {
            type: GET_ALL_USERS_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            getAllUsers()
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with RESET_USER_PASSWORD type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        moxios.stubRequest('http://localhost:62795/Users/ResetPassword', {
            status: 200
        });
        const email = 'someEmail@gmail.com';
        const password = 'somePassword';
        const action: UserActions = {
            type: RESET_USER_PASSWORD,
            payload: undefined
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            resetUserPassword(email, password)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
    it('should return action with RESET_USER_PASSWORD_ERROR type', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: null,
                loginError: null,
                isSignedIn: false,
                registerError: null,
                role: null,
                userId: null
            },
            message: {
                error: null,
                messagesForUser: [] as ReceivedMessage[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const statusError = 400;
        moxios.stubRequest('http://localhost:62795/Users/ResetPassword', {
            status: statusError
        });
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const email = 'someEmail@gmail.com';
        const someInvalidPassword = 'someInvalidPassword';
        const action: UserActions = {
            type: RESET_USER_PASSWORD_ERROR,
            payload: errorMessage
        };
        await (store.dispatch as ThunkDispatch<AppState, void, UserActions>)(
            resetUserPassword(email, someInvalidPassword)
        );
        expect(store.getActions()[0]).toEqual(action);
    });
});
