import moxios from 'moxios';
import configureMockStore from 'redux-mock-store';
import reduxThunk, { ThunkDispatch } from 'redux-thunk';
import {
    authenticateUser,
    changeEmail,
    changeLogin,
    changePassword,
    registerUser
} from '../actions';
import {
    AppState,
    CHANGE_EMAIL_FAILED,
    CHANGE_EMAIL_SUCCEEDED,
    CHANGE_LOGIN_FAILED,
    CHANGE_LOGIN_SUCCEEDED,
    CHANGE_PASSWORD_FAILED,
    CHANGE_PASSWORD_SUCCEEDED,
    LOGIN_FAILED,
    LOGIN_SUCCEEDED,
    LoginActions,
    Message,
    REGISTER_FAILED,
    REGISTER_SUCCEEDED,
    User
} from '../actions/types';

const mockStore = configureMockStore<AppState>([reduxThunk]);

describe('LoginActions', () => {
    let action: LoginActions;
    describe('AuthenticateUser action creator', () => {
        beforeEach(() => {
            moxios.install();
        });
        afterEach(() => {
            moxios.uninstall();
        });
        it('should return action with CHANGE_EMAIL_SUCCEEDED type', async () => {
            const oldEmail = 'someOldEmail@gmail.com';
            const newEmail = 'someNewEmail@gmail.com';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            moxios.stubRequest('http://localhost:62795/Users/Update', {
                status: 200
            });
            action = {
                type: CHANGE_EMAIL_SUCCEEDED,
                payload: newEmail
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(changeEmail(oldEmail, newEmail));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with CHANGE_EMAIL_FAILED type', async () => {
            const oldEmail = 'someOldEmail@gmail.com';
            const newEmail = 'someInvalidEmail@gmail.com';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            const statusError = 400;
            moxios.stubRequest('http://localhost:62795/Users/Update', {
                status: statusError
            });
            const errorMessage = new Error(
                `Request failed with status code ${statusError}`
            ).message;
            action = {
                type: CHANGE_EMAIL_FAILED,
                payload: errorMessage
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(changeEmail(oldEmail, newEmail));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with CHANGE_LOGIN_SUCCEEDED type', async () => {
            const email = 'someEmail@gmail.com';
            const newLogin = 'someLogin';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            moxios.stubRequest('http://localhost:62795/Users/ChangeLogin', {
                status: 200
            });
            action = {
                type: CHANGE_LOGIN_SUCCEEDED,
                payload: undefined
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(changeLogin(email, newLogin));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with CHANGE_LOGIN_FAILED type', async () => {
            const email = 'someEmail@gmail.com';
            const newLogin = 'someInvalidLogin';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            const statusError = 400;
            moxios.stubRequest('http://localhost:62795/Users/ChangeLogin', {
                status: statusError
            });
            const errorMessage = new Error(
                `Request failed with status code ${statusError}`
            ).message;
            action = {
                type: CHANGE_LOGIN_FAILED,
                payload: errorMessage
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(changeLogin(email, newLogin));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with CHANGE_PASSWORD_SUCCEEDED type', async () => {
            const email = 'someValidEmail@gmail.com';
            const oldPassword = 'someOldPassword';
            const newPassword = 'someNewPassword';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            moxios.stubRequest('http://localhost:62795/Users/ChangePassword', {
                status: 200
            });
            action = {
                type: CHANGE_PASSWORD_SUCCEEDED,
                payload: undefined
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(changePassword(email, oldPassword, newPassword));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with CHANGE_PASSWORD_FAILED type', async () => {
            const email = 'someInvalidEmail@gmail.com';
            const oldPassword = 'someOldPassword';
            const newPassword = 'someInvalidPassword';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            const statusError = 400;
            moxios.stubRequest('http://localhost:62795/Users/ChangePassword', {
                status: statusError
            });
            const errorMessage = new Error(
                `Request failed with status code ${statusError}`
            ).message;
            action = {
                type: CHANGE_PASSWORD_FAILED,
                payload: errorMessage
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(changePassword(email, oldPassword, newPassword));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with LOGIN_FAILED type', async () => {
            const login = 'someInvalidLogin';
            const password = 'someInvalidPassword';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            moxios.stubRequest('http://localhost:62795/api/register/Login', {
                status: 400
            });
            action = {
                type: LOGIN_FAILED,
                payload: 'Podano nieprawidłowe dane logowania'
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(authenticateUser(login, password));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with LOGIN_SUCCEEDED type', async () => {
            const login = 'someValidLogin';
            const password = 'someValidPassword';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            const response = {
                data: {
                    userName: 'Janusz69',
                    role: 'admin',
                    email: 'janusz@gmail.com',
                    token: 'TOKENTOKENTOKENTOKENTOKENTOKENTOKENTOKENTOKENTOKEN',
                    id: 69
                }
            };
            moxios.stubRequest('http://localhost:62795/api/register/Login', {
                status: 200,
                response: response.data
            });
            action = {
                type: LOGIN_SUCCEEDED,
                payload: response.data
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(authenticateUser(login, password));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with REGISTER_SUCCEEDED type', async () => {
            const email = 'someValidEmail@gmail.com';
            const login = 'someValidLogin';
            const password = 'someValidPassword';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            moxios.stubRequest('http://localhost:62795/api/register/Register', {
                status: 200
            });
            action = {
                type: REGISTER_SUCCEEDED,
                payload: undefined
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(registerUser(email, login, password));
            expect(store.getActions()[0]).toEqual(action);
        });
        it('should return action with REGISTER_FAILED type', async () => {
            const email = 'someInvalidEmail@gmail.com';
            const login = 'someInvalidLogin';
            const password = 'someInvalidPassword';
            const store = mockStore({
                auth: {
                    changeEmailError: null,
                    changeLoginError: null,
                    changePasswordError: null,
                    email: null,
                    loginError: null,
                    isSignedIn: false,
                    registerError: null,
                    role: null,
                    userId: null
                },
                message: {
                    error: null,
                    messagesForUser: [] as Message[],
                    notAcceptedMessages: [] as Message[]
                },
                user: {
                    changeUserEmailError: '',
                    changeUserLoginError: '',
                    changeUserRoleError: '',
                    getAllUsersError: '',
                    resetUserPasswordError: '',
                    users: [] as User[]
                }
            });
            const statusError = 415;
            moxios.stubRequest('http://localhost:62795/api/register/Register', {
                status: statusError
            });
            const errorMessage = new Error(
                `Request failed with status code ${statusError}`
            ).message;
            action = {
                type: REGISTER_FAILED,
                payload: errorMessage
            };
            await (store.dispatch as ThunkDispatch<
                AppState,
                void,
                LoginActions
            >)(registerUser(email, login, password));
            expect(store.getActions()[0]).toEqual(action);
        });
    });
});
