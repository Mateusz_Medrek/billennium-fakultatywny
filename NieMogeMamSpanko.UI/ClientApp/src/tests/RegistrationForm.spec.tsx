import { mount, ReactWrapper, shallow } from 'enzyme';
import React from 'react';
import ConnectedRegistrationForm, {
    RegistrationForm
} from '../components/RegistrationForm';
import RootProvider from '../components/RootProvider';

describe('RegistrationForm', () => {
    let wrapper: ReactWrapper;
    beforeEach(() => {
        wrapper = mount(
            <RootProvider>
                <ConnectedRegistrationForm />
            </RootProvider>
        );
    });
    afterEach(() => {
        wrapper.unmount();
    });
    it('should have <form />', () => {
        expect(wrapper.find('form')).toHaveLength(1);
    });
    it('should have 4 <Field /> components', () => {
        // <Field /> is wrapped in hoc and that's why if LoginForm has 4 <Field />,
        // we should expect 8 in this test;
        expect(wrapper.find('Field')).toHaveLength(8);
    });
    it('should have submit <button />', () => {
        expect(
            wrapper.contains(
                <button className="ui primary button" type="submit">
                    Zarejestruj
                </button>
            )
        ).toBe(true);
    });
    it('should display <div /> with error messages after leaving blank inputs', () => {
        shallow(wrapper.find('input').get(0)).simulate('change', '');
        shallow(wrapper.find('input').get(0)).simulate('blur');
        shallow(wrapper.find('input').get(1)).simulate('change', '');
        shallow(wrapper.find('input').get(1)).simulate('blur');
        shallow(wrapper.find('input').get(2)).simulate('change', '');
        shallow(wrapper.find('input').get(2)).simulate('blur');
        shallow(wrapper.find('input').get(3)).simulate('change', '');
        shallow(wrapper.find('input').get(3)).simulate('blur');
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać email</div>
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać login</div>
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać hasło</div>
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz potwierdzić hasło</div>
                </div>
            )
        ).toBe(true);
    });
    it('should display <div /> with error message after typing invalid email', () => {
        shallow(wrapper.find('input').get(0)).simulate(
            'change',
            'someInvalidEmail'
        );
        shallow(wrapper.find('input').get(0)).simulate('blur');
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać email w prawidłowym formacie
                    </div>
                </div>
            )
        ).toBe(true);
    });
    it('should display <div /> with error message after typing different values in password inputs', () => {
        shallow(wrapper.find('input').get(2)).simulate(
            'change',
            'somePasswordValue'
        );
        shallow(wrapper.find('input').get(2)).simulate('blur');
        shallow(wrapper.find('input').get(3)).simulate(
            'change',
            'someOtherPasswordValue'
        );
        shallow(wrapper.find('input').get(3)).simulate('blur');
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz potwierdzić hasło</div>
                </div>
            )
        ).toBe(true);
    });
    it('should call onSubmit method after submitting form', () => {
        const onSubmit = jest.spyOn(
            wrapper
                .find('RegistrationForm')
                .first()
                .instance() as RegistrationForm,
            'onSubmit'
        );
        shallow(wrapper.find('input').get(0)).simulate(
            'change',
            'grazyna@gmail.com'
        );
        shallow(wrapper.find('input').get(0)).simulate('blur');
        shallow(wrapper.find('input').get(1)).simulate('change', 'Grazyna34');
        shallow(wrapper.find('input').get(1)).simulate('blur');
        shallow(wrapper.find('input').get(2)).simulate('change', 'grazyna');
        shallow(wrapper.find('input').get(2)).simulate('blur');
        shallow(wrapper.find('input').get(3)).simulate('change', 'grazyna');
        shallow(wrapper.find('input').get(3)).simulate('blur');
        wrapper
            .find('form')
            .first()
            .simulate('submit');
        expect(onSubmit).toBeCalledTimes(1);
    });
});
