import { mount, ReactWrapper, shallow } from 'enzyme';
import React from 'react';
import ConnectedMessageForm, { MessageForm } from '../components/MessageForm';
import RootProvider from '../components/RootProvider';

let wrapper: ReactWrapper;

describe('MessageForm', () => {
    beforeEach(() => {
        wrapper = mount(
            <RootProvider>
                <ConnectedMessageForm />
            </RootProvider>
        );
    });
    afterEach(() => {
        wrapper.unmount();
    });
    it('should have <form />', () => {
        expect(wrapper.find('form')).toHaveLength(1);
    });
    it('should have 4 <Field /> components', () => {
        // <Field /> is wrapped in hoc and that's why if LoginForm has 4 <Field />,
        // we should expect 8 in this test;
        expect(wrapper.find('Field')).toHaveLength(8);
    });
    it('should have submit <button />', () => {
        expect(
            wrapper.contains(
                <button className="ui primary button" type="submit">
                    Zatwierdź
                </button>
            )
        ).toBe(true);
    });
    it('should have 1 <Dropzone />', () => {
        expect(wrapper.find('Dropzone')).toHaveLength(1);
        expect(
            wrapper
                .find('Dropzone')
                .first()
                .find('input')
        ).toHaveLength(1);
        expect(
            wrapper
                .find('Dropzone')
                .first()
                .contains(<p>lub przeciągnij je tutaj</p>)
        ).toBe(true);
    });
    it('should have 1 <ReceiversInput />', () => {
        expect(wrapper.find('ReceiversInput')).toHaveLength(1);
    });
    it('should display <div /> with error messages after leaving blank inputs', () => {
        const onSubmit = jest.spyOn(
            wrapper
                .find('MessageForm')
                .first()
                .instance() as MessageForm,
            'onSubmit'
        );
        shallow(wrapper.find('renderField').get(0))
            .find('input')
            .first()
            .simulate('change', '');
        shallow(wrapper.find('renderField').get(0))
            .find('input')
            .first()
            .simulate('blur');
        wrapper
            .find('ReceiversInput')
            .first()
            .find('input')
            .last()
            .simulate('change', {
                preventDefault: jest.fn(),
                target: { value: '' }
            });
        wrapper
            .find('ReceiversInput')
            .first()
            .find('input')
            .last()
            .simulate('blur');
        wrapper
            .find('form')
            .first()
            .simulate('submit');
        wrapper.update();
        expect(onSubmit).toBeCalledTimes(0);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać email</div>
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać przynajmniej jednego odbiorcę
                    </div>
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać przynajmniej jeden plik
                    </div>
                </div>
            )
        ).toBe(true);
    });
    it('should display <div /> with error message after typing invalid email', () => {
        shallow(wrapper.find('renderField').get(0))
            .find('input')
            .first()
            .simulate('change', 'someInvalidEmail');
        shallow(wrapper.find('renderField').get(0))
            .find('input')
            .first()
            .simulate('blur');
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać email w prawidłowym formacie
                    </div>
                </div>
            )
        ).toBe(true);
    });
    it('should call onFileDeleted when file is deleted from input', () => {
        const onFileDeleted = jest.spyOn(
            wrapper
                .find('MessageForm')
                .first()
                .instance() as MessageForm,
            'onFileDeleted'
        );
        (wrapper
            .find('renderDropzone')
            .first()
            .props() as any).input.onChange([
            new File(['0o0111001110001'], 'plik1.txt')
        ]);
        (wrapper
            .find('renderDropzone')
            .first()
            .props() as any).onFilesChange([
            new File(['0o0111001110001'], 'plik1.txt')
        ]);
        shallow(wrapper.find('renderDropzone').get(0))
            .find('Dropzone')
            .first()
            .simulate('blur');
        wrapper.update();
        wrapper
            .find('FileElement')
            .first()
            .find('div.negative')
            .first()
            .simulate('click');
        expect(onFileDeleted).toBeCalledTimes(1);
    });
    it('should call onSubmit method after submitting form', () => {
        const onSubmit = jest.spyOn(
            wrapper
                .find('MessageForm')
                .first()
                .instance() as MessageForm,
            'onSubmit'
        );
        shallow(wrapper.find('renderField').get(0))
            .find('input')
            .first()
            .simulate('change', 'grazyna@gmail.com');
        shallow(wrapper.find('renderField').get(0))
            .find('input')
            .first()
            .simulate('blur');
        wrapper
            .find('ReceiversInput')
            .first()
            .find('input')
            .first()
            .simulate('change', {
                preventDefault: jest.fn(),
                target: { value: 'janusz@gmail.com' }
            });
        wrapper
            .find('ReceiversInput')
            .first()
            .find('input')
            .first()
            .simulate('keyDown', {
                preventDefault: jest.fn(),
                keyCode: 13
            });
        (wrapper
            .find('renderDropzone')
            .first()
            .props() as any).input.onChange([
            new File(['0o0111001110001'], 'plik1.txt')
        ]);
        (wrapper
            .find('renderDropzone')
            .first()
            .props() as any).onFilesChange([
            new File(['0o0111001110001'], 'plik1.txt')
        ]);
        shallow(wrapper.find('renderDropzone').get(0))
            .find('Dropzone')
            .first()
            .simulate('blur');
        wrapper
            .find('form')
            .first()
            .simulate('submit');
        expect(onSubmit).toBeCalledTimes(1);
    });
});
