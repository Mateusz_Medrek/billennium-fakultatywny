import { mount, shallow } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import reduxThunk from 'redux-thunk';
import wait from 'waait';
import { AppState, Message, User } from '../actions/types';
import ConnectedApp, { App } from '../components/App';
import RootProvider from '../components/RootProvider';

const mockStore = configureMockStore<AppState>([reduxThunk]);

describe('App', () => {
    it('should render div.container', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedApp />
            </RootProvider>
        );
        expect(wrapper.find('div.container')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <Navbar />', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedApp />
            </RootProvider>
        );
        expect(wrapper.find('Navbar')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render guest navbar', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedApp />
            </RootProvider>
        );
        expect(
            wrapper
                .find('Navbar')
                .first()
                .find('div.secondary')
        ).toHaveLength(1);
        expect(
            wrapper
                .find('Navbar')
                .first()
                .find('div.green')
        ).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <MessageForm /> and <LoginForm /> as guest content', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedApp />
            </RootProvider>
        );
        expect(wrapper.find('MessageForm')).toHaveLength(1);
        expect(wrapper.find('LoginForm')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <RegistrationForm /> as guest content', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedApp />
            </RootProvider>
        );
        wrapper.find('div.secondary').simulate('click');
        wrapper.update();
        expect(wrapper.find('RegistrationForm')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render three tabs in loggedIn navbar when user role is User', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        expect(wrapper.find('a')).toHaveLength(3);
        wrapper.unmount();
    });
    it('should render four tabs in loggedIn navbar when user role is Admin', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'Admin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        expect(wrapper.find('a')).toHaveLength(4);
        wrapper.unmount();
    });
    it('should render five tabs in loggedIn navbar when user role is SuperAdmin', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'SuperAdmin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        expect(wrapper.find('a')).toHaveLength(5);
        wrapper.unmount();
    });
    it('should render <MessageInbox /> as first tab in loggedIn navbar', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'Admin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        await wait(0);
        wrapper.update();
        expect(wrapper.find('MessageInbox')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <MessageForm /> as second tab in loggedIn navbar', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'Admin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        shallow(wrapper.find('a').get(1)).simulate('click');
        wrapper.update();
        expect(wrapper.find('MessageForm')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <AccountManagement /> as third tab in loggedIn navbar', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'Admin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        shallow(wrapper.find('a').get(2)).simulate('click');
        wrapper.update();
        expect(wrapper.find('AccountManagement')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <AcceptMessagesPanel /> as fourth tab in loggedIn navbar', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'Admin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        shallow(wrapper.find('a').get(3)).simulate('click');
        wrapper.update();
        expect(wrapper.find('AcceptMessagesPanel')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render <UserManagementPanel /> as fifth tab in loggedIn navbar', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'SuperAdmin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        shallow(wrapper.find('a').get(4)).simulate('click');
        wrapper.update();
        expect(wrapper.find('UserManagementPanel')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should call signOut, after clicking sign out button', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'Admin',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedApp />
            </Provider>
        );
        const onSignOut = jest.spyOn(
            wrapper
                .find('App')
                .first()
                .instance() as App,
            'onSignOut'
        );
        shallow(wrapper.find('div.button').get(0)).simulate('click');
        expect(onSignOut).toBeCalledTimes(1);
        wrapper.unmount();
    });
});
