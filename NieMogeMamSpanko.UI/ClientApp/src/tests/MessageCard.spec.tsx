import { mount } from 'enzyme';
import React from 'react';
import MessageCard from '../components/MessageCard';

const props = {
    message: {
        sender: 'janusz@gmail.com',
        messageReceivers: [
            'halyna@gmail.com',
            'pjoter@gmail.com',
            'grazyna@gmail.com'
        ],
        files: [
            {
                ...new File(['0o0101010101', '0o101010101101010'], 'file1.txt'),
                id: 'jakiesId',
                messageId: '3',
                title: 'file2.txt'
            },
            {
                ...new File(['0o0001001111', '0o11101100011100'], 'file2.txt'),
                id: 'inneId',
                messageId: '3',
                title: 'file2.txt'
            }
        ],
        description: 'To jest wiadomość do mojej rodzinki, załonczam 2 pliki',
        id: '3',
        date: new Date('2019-05-12T13:16:46.581Z')
    }
};

describe('MessageCard', () => {
    it('should render <MessageCard />', () => {
        const styles: { [key: string]: React.CSSProperties } = {
            content: {
                marginLeft: '40px'
            },
            notFullViewHeaders: {
                margin: '0px 5px'
            }
        };
        let wrapper = mount(<MessageCard {...props} />);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Nadawca:
                </div>
            )
        ).toBe(true);
        expect(wrapper.contains(props.message.sender)).toBe(true);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Liczba odbiorców:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>
                    {props.message.messageReceivers.length}
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Liczba plików:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>{props.message.files.length}</div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="header" style={styles.notFullViewHeaders}>
                    Opis:
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div style={styles.content}>
                    {props.message.description.slice(0, 50) + '...'}
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
        props.message.description = 'ABC';
        wrapper = mount(<MessageCard {...props} />);
        expect(wrapper.contains(props.message.description)).toBe(true);
        wrapper.unmount();
        props.message.description = '';
        wrapper = mount(<MessageCard {...props} />);
        expect(wrapper.contains('Brak')).toBe(true);
        wrapper.unmount();
    });
    it('should call onMessageClick after clicking on MessageCard', () => {
        const onMessageClick = jest.fn();
        const wrapper = mount(
            <MessageCard {...props} onMessageClick={onMessageClick} />
        );
        wrapper
            .find('div.item')
            .first()
            .simulate('click');
        expect(onMessageClick).toBeCalledWith(props.message);
        wrapper.unmount();
    });
});
