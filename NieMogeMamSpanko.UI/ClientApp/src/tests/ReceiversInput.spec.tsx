import { mount, shallow } from 'enzyme';
import React from 'react';
import { WrappedFieldInputProps } from 'redux-form';
import ReceiversInput from '../components/ReceiversInput';

describe('ReceiversInput', () => {
    it('should have 2 <input />', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount(<ReceiversInput {...props} />);
        expect(wrapper.find('input')).toHaveLength(2);
        wrapper.unmount();
    });
    it('should call onInputChange after typing some value', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com' }
        });
        expect(onInputChange).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onAddingReceiver after typing some value and ; at the end', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        const onAddingReceiver = jest.spyOn(
            wrapper.instance(),
            'onAddingReceiver'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com;' }
        });
        shallow(wrapper.find('input').get(0)).simulate('blur');
        expect(onInputChange).toBeCalledTimes(1);
        expect(onAddingReceiver).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onAddingReceiver after typing some value and pressing Enter key', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        const onAddingReceiver = jest.spyOn(
            wrapper.instance(),
            'onAddingReceiver'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com' }
        });
        shallow(wrapper.find('input').get(0)).simulate('keyDown', {
            preventDefault: jest.fn(),
            keyCode: 13
        });
        expect(onInputChange).toBeCalledTimes(1);
        expect(onAddingReceiver).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call display error after typing some invalid value', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        const onAddingReceiver = jest.spyOn(
            wrapper.instance(),
            'onAddingReceiver'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'someInvalidValue' }
        });
        shallow(wrapper.find('input').get(0)).simulate('keyDown', {
            preventDefault: jest.fn(),
            keyCode: 13
        });
        expect(onInputChange).toBeCalledTimes(1);
        expect(onAddingReceiver).toBeCalledTimes(1);
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">
                        Musisz podać email w prawidłowym formacie
                    </div>
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should call display error after typing some value which was already typed', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        const onAddingReceiver = jest.spyOn(
            wrapper.instance(),
            'onAddingReceiver'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com' }
        });
        shallow(wrapper.find('input').get(0)).simulate('keyDown', {
            preventDefault: jest.fn(),
            keyCode: 13
        });
        wrapper.update();
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com' }
        });
        shallow(wrapper.find('input').get(0)).simulate('keyDown', {
            preventDefault: jest.fn(),
            keyCode: 13
        });
        expect(onInputChange).toBeCalledTimes(2);
        expect(onAddingReceiver).toBeCalledTimes(2);
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Podałeś już tego odbiorcę</div>
                </div>
            )
        ).toBe(true);
        wrapper.unmount();
    });
    it('should have 1 <Receiver /> after typing 1 receiver', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        const onAddingReceiver = jest.spyOn(
            wrapper.instance(),
            'onAddingReceiver'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com' }
        });
        shallow(wrapper.find('input').get(0)).simulate('keyDown', {
            preventDefault: jest.fn(),
            keyCode: 13
        });
        expect(onInputChange).toBeCalledTimes(1);
        expect(onAddingReceiver).toBeCalledTimes(1);
        wrapper.update();
        expect(wrapper.find('Receiver')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should call onReceiverDelete after deleting Receiver', () => {
        const props: WrappedFieldInputProps = {
            name: 'receivers',
            onBlur: jest.fn(),
            onChange: jest.fn(),
            onDragStart: jest.fn(),
            onDrop: jest.fn(),
            onFocus: jest.fn(),
            value: ''
        };
        const wrapper = mount<ReceiversInput>(<ReceiversInput {...props} />);
        const onInputChange = jest.spyOn(wrapper.instance(), 'onInputChange');
        const onAddingReceiver = jest.spyOn(
            wrapper.instance(),
            'onAddingReceiver'
        );
        const onReceiverDelete = jest.spyOn(
            wrapper.instance(),
            'onReceiverDelete'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', {
            preventDefault: jest.fn(),
            target: { value: 'janusz@gmail.com' }
        });
        shallow(wrapper.find('input').get(0)).simulate('keyDown', {
            preventDefault: jest.fn(),
            keyCode: 13
        });
        expect(onInputChange).toBeCalledTimes(1);
        expect(onAddingReceiver).toBeCalledTimes(1);
        wrapper.update();
        expect(wrapper.find('Receiver')).toHaveLength(1);
        shallow(wrapper.find('Receiver').get(0))
            .find('div.negative')
            .first()
            .simulate('click');
        expect(onReceiverDelete).toBeCalledTimes(1);
        wrapper.unmount();
    });
});
