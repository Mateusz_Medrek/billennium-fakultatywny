import { mount } from 'enzyme';
import React from 'react';
import FileElement from '../components/FileElement';

describe('FileElement', () => {
    it('should render FileElement with X button', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            onDelete: jest.fn(),
            value: new File(['0o0101010101', '0o101010101101010'], 'file1.txt')
        };
        const wrapper = mount(<FileElement {...props} />);
        expect(wrapper.find('div.negative')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should render FileElement with download icon', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            onDownload: jest.fn(),
            value: new File(['0o0101010101', '0o101010101101010'], 'file1.txt')
        };
        const wrapper = mount(<FileElement {...props} />);
        expect(wrapper.contains(<i className="download icon" />)).toBe(true);
        wrapper.unmount();
    });
    it('should render FileElement with download icon', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            value: new File(['0o0101010101', '0o101010101101010'], 'file1.txt')
        };
        const wrapper = mount(<FileElement {...props} />);
        expect(wrapper.contains(<div className="withoutButton" />)).toBe(true);
        wrapper.unmount();
    });
    it('should render name of file', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            onDownload: jest.fn(),
            value: new File(['0o0101010101', '0o101010101101010'], 'file1.txt')
        };
        const styles: { [key: string]: React.CSSProperties } = {
            value: {
                display: 'inline',
                marginLeft: 10,
                marginRight: 10
            }
        };
        const wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<div style={styles.value}>{props.value.name}</div>)
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render different types of icons', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            onDownload: jest.fn(),
            value: new File(
                ['0o0101010101', '0o101010101101010'],
                'file1.zip',
                { type: 'application/zip' }
            )
        };
        let wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<i className="file archive outline icon" />)
        ).toBe(true);
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.wav',
            { type: 'audio/wav' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<i className="file audio outline icon" />)
        ).toBe(true);
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.xls',
            { type: 'application/vnd.ms-excel' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<i className="file excel outline icon" />)
        ).toBe(true);
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.png',
            { type: 'image/png' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<i className="file image outline icon" />)
        ).toBe(true);
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.pdf',
            { type: 'application/pdf' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(wrapper.contains(<i className="file pdf outline icon" />)).toBe(
            true
        );
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.ppt',
            { type: 'application/vnd.ms-powerpoint' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<i className="file powerpoint outline icon" />)
        ).toBe(true);
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.mpeg',
            { type: 'video/mpeg' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(
            wrapper.contains(<i className="file video outline icon" />)
        ).toBe(true);
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.doc',
            { type: 'application/msword' }
        );
        wrapper = mount(<FileElement {...props} />);
        expect(wrapper.contains(<i className="file word outline icon" />)).toBe(
            true
        );
        wrapper.unmount();
        props.value = new File(
            ['0o0101010101', '0o101010101101010'],
            'file1.txt'
        );
        wrapper = mount(<FileElement {...props} />);
        expect(wrapper.contains(<i className="file outline icon" />)).toBe(
            true
        );
        wrapper.unmount();
    });
    it('should call onDelete prop after simulating delete file', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            onDelete: jest.fn(),
            value: new File(['0o0101010101', '0o101010101101010'], 'file1.zip')
        };
        const wrapper = mount(<FileElement {...props} />);
        wrapper
            .find('div.negative')
            .first()
            .simulate('click');
        expect(props.onDelete).toBeCalledWith(props.fileKey);
        wrapper.unmount();
    });
    it('should call onDownload prop after simulating file download', () => {
        const props = {
            fileId: '34',
            fileKey: 4,
            onDownload: jest.fn(),
            value: new File(['0o0101010101', '0o101010101101010'], 'file1.zip')
        };
        const wrapper = mount(<FileElement {...props} />);
        wrapper
            .find('div.downloadButton')
            .first()
            .simulate('click');
        expect(props.onDownload).toBeCalledWith(props.fileId, props.value.name);
        wrapper.unmount();
    });
});
