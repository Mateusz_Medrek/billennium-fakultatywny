import { signIn, signOut } from '../actions';
import { SIGN_IN, SIGN_OUT, SignActions } from '../actions/types';

describe('SignActions', () => {
    let action: SignActions;
    describe('SignIn action creator', () => {
        beforeEach(() => {
            action = signIn();
        });
        it('should return action with SIGN_IN type', () => {
            expect(action.type).toEqual(SIGN_IN);
        });
    });
    describe('SignOut action creator', () => {
        beforeEach(() => {
            action = signOut();
        });
        it('should return action with SIGN_OUT type', () => {
            expect(action.type).toEqual(SIGN_OUT);
        });
    });
});
