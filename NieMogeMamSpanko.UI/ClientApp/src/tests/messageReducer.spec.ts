import {
    ACCEPT_MESSAGE,
    ACCEPT_MESSAGE_ERROR,
    AppState,
    CREATE_MESSAGE,
    CREATE_MESSAGE_ERROR,
    DOWNLOAD_FILE,
    DOWNLOAD_FILE_ERROR,
    GET_MESSAGES_FOR_USER,
    GET_MESSAGES_FOR_USER_ERROR,
    GET_NOT_ACCEPTED_MESSAGES,
    GET_NOT_ACCEPTED_MESSAGES_ERROR,
    Message,
    MessageActions,
    ReceivedMessage,
    REJECT_MESSAGE,
    REJECT_MESSAGE_ERROR
} from '../actions/types';
import messageReducer from '../reducers/messageReducer';

const messages: Message[] = [
    {
        id: '123',
        sender: 'janusz@gmail.com',
        messageReceivers: [
            'grazyna@gmail.com',
            'pjoter@gmail.com',
            'ksbdvkjsbvkjsbvkjsbdvksbvksjbdv',
            'abksjbvksbvksjbvksjbvksj',
            'ajbksjbvdkjsbdvkjsbdvksdvkjbskvskdbvkjsbdvkjsbvkjsdbv'
        ],
        files: [
            {
                ...new File(
                    ['0o0101110010001', '0o111000011010100'],
                    'plik1.txt'
                ),
                id: 'jakiesId',
                messageId: '123',
                title: 'plik1.txt'
            }
        ],
        description: 'Wiadomosc do mojej rodzinki',
        date: new Date('2019-05-12T13:16:46.581Z')
    },
    {
        id: '456',
        sender: 'somsiad@gmail.com',
        messageReceivers: ['janusz@gmail.com'],
        files: [
            {
                ...new File(['0o111110010001', '0o1111010100'], 'plik2.txt'),
                id: 'dziwneId',
                messageId: '456',
                title: 'plik2.txt'
            },
            {
                ...new File(
                    ['0o111110010001', '0o1111010100'],
                    'plik12345.txt'
                ),
                id: 'normalneId',
                messageId: '456',
                title: 'plik12345.txt'
            }
        ],
        description: 'Dla mojego najlepszego somsiada, Janusza',
        date: new Date('2019-05-09T13:16:46.581Z')
    }
];

describe('messageReducer', () => {
    it('should return new message piece of state after dispatching ACCEPT_MESSAGE action', () => {
        const messageId = 'someMessageId';
        const action: MessageActions = {
            type: ACCEPT_MESSAGE,
            payload: messageId
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(null);
    });
    it('should return new message piece of state after dispatching ACCEPT_MESSAGE_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: MessageActions = {
            type: ACCEPT_MESSAGE_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(errorMessage);
    });
    it('should return new message piece of state after dispatching CREATE_MESSAGE action', () => {
        const action: MessageActions = {
            type: CREATE_MESSAGE,
            payload: undefined
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(null);
    });
    it('should return new message piece of state after dispatching CREATE_MESSAGE_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: MessageActions = {
            type: CREATE_MESSAGE_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(errorMessage);
    });
    it('should return new message piece of state after dispatching DOWNLOAD_FILE action', () => {
        const file = new File(['0o00001010101'], 'plik1.txt');
        const action: MessageActions = {
            type: DOWNLOAD_FILE,
            payload: undefined
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(null);
    });
    it('should return new message piece of state after dispatching DOWNLOAD_FILE_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: MessageActions = {
            type: DOWNLOAD_FILE_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(errorMessage);
    });
    it('should return new message piece of state after dispatching GET_MESSAGES_FOR_USER action', () => {
        const action: MessageActions = {
            type: GET_MESSAGES_FOR_USER,
            payload: messages
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.messagesForUser).toEqual(messages);
        expect(newState.error).toEqual(null);
    });
    it('should return new message piece of state after dispatching GET_MESSAGES_FOR_USER_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: MessageActions = {
            type: GET_MESSAGES_FOR_USER_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(errorMessage);
        expect(newState.messagesForUser).toEqual([]);
    });
    it('should return new message piece of state after dispatching GET_NOT_ACCEPTED_MESSAGES action', () => {
        const action: MessageActions = {
            type: GET_NOT_ACCEPTED_MESSAGES,
            payload: messages
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.notAcceptedMessages).toEqual(messages);
        expect(newState.error).toEqual(null);
    });
    it('should return new message piece of state after dispatching GET_NOT_ACCEPTED_MESSAGES_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: MessageActions = {
            type: GET_NOT_ACCEPTED_MESSAGES_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(errorMessage);
        expect(newState.notAcceptedMessages).toEqual([]);
    });
    it('should return new message piece of state after dispatching REJECT_MESSAGE action', () => {
        const messageId = 'someMessageId';
        const action: MessageActions = {
            type: REJECT_MESSAGE,
            payload: messageId
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(null);
    });
    it('should return new message piece of state after dispatching REJECT_MESSAGE_ERROR action', () => {
        const statusError = 415;
        const errorMessage = new Error(
            `Request failed with status code ${statusError}`
        ).message;
        const action: MessageActions = {
            type: REJECT_MESSAGE_ERROR,
            payload: errorMessage
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(errorMessage);
    });
    it('should return the same piece of state after dispatching some other action', () => {
        const action: any = {
            type: 'SOME_ACTION'
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(
            initialState,
            action
        );
        expect(newState.error).toEqual(initialState.error);
    });
    it('should return initial piece of state after dispatching some other action', () => {
        const action: any = {
            type: 'SOME_ACTION'
        };
        const initialState: AppState['message'] = {
            error: null,
            messagesForUser: [] as ReceivedMessage[],
            notAcceptedMessages: [] as Message[]
        };
        const newState: AppState['message'] = messageReducer(undefined, action);
        expect(newState.error).toEqual(initialState.error);
    });
});
