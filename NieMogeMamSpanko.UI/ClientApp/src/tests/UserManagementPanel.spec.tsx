import { mount, shallow } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import reduxThunk from 'redux-thunk';
import wait from 'waait';
import { AppState, Message, User } from '../actions/types';
import RootProvider from '../components/RootProvider';
import ConnectedUserManagementPanel, {
    UserManagementPanel
} from '../components/UserManagementPanel';

const mockStore = configureMockStore<AppState>([reduxThunk]);

const users: User[] = [
    {
        email: 'janusz@gmail.com',
        userName: 'AdminJanusz69',
        role: 'Admin'
    },
    {
        email: 'grazyna@gmail.com',
        userName: 'Grazyna84',
        role: 'User'
    },
    {
        email: 'pjoter@gmail.com',
        userName: 'Pjoter',
        role: 'User'
    }
];

describe('UserManagementPanel', () => {
    it('should render sort label, sort <Dropdown /> and filter input with icon', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedUserManagementPanel />
            </RootProvider>
        );
        const styles: { [key: string]: React.CSSProperties } = {
            filterSorterInput: {
                margin: '5px',
                padding: '5px'
            }
        };
        expect(
            wrapper.contains(
                <div className="header" style={styles.filterSorterInput}>
                    Sortuj
                </div>
            )
        ).toBe(true);
        expect(wrapper.find('Dropdown')).toHaveLength(1);
        expect(
            wrapper
                .find('div.input')
                .first()
                .find('input')
        ).toHaveLength(1);
        expect(
            wrapper.contains(<i className="circular search link icon" />)
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render <UserCard /> for each user', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        expect(wrapper.find('UserCard')).toHaveLength(
            (wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel).props.users.length
        );
        wrapper.unmount();
    });
    it('should render emptyUserWindow when none of users were clicked', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedUserManagementPanel />
            </RootProvider>
        );
        expect(
            wrapper
                .find('div.emptyUserWindow')
                .first()
                .contains('Wybierz jednego z użytkowników po lewej')
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render div.noUsers when there are no users', () => {
        const wrapper = mount(
            <RootProvider>
                <ConnectedUserManagementPanel />
            </RootProvider>
        );
        expect(wrapper.find('div.noUsers')).toHaveLength(1);
        wrapper.unmount();
    });
    it('should call doesContainPhrase for every user after typing some value in filter input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const doesContainPhrase = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'doesContainPhrase'
        );
        wrapper
            .find('div.input')
            .first()
            .find('input')
            .first()
            .simulate('change', { target: { value: 'someValue' } });
        wrapper.update();
        expect(doesContainPhrase).toBeCalledTimes(
            (wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel).props.users.length
        );
        wrapper.unmount();
    });
    it('should render userWindow after clicking on one of users', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const styles: { [key: string]: React.CSSProperties } = {
            headers: {
                margin: '5px'
            }
        };
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains(
                    <div className="header" style={styles.headers}>
                        Login:
                    </div>
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains(
                    (wrapper
                        .find('UserManagementPanel')
                        .first()
                        .instance() as UserManagementPanel).state.selectedUser!
                        .userName
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains(
                    <div className="header" style={styles.headers}>
                        Email:
                    </div>
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains(
                    (wrapper
                        .find('UserManagementPanel')
                        .first()
                        .instance() as UserManagementPanel).state.selectedUser!
                        .email
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains(
                    <div className="header" style={styles.headers}>
                        Rola:
                    </div>
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains(
                    (wrapper
                        .find('UserManagementPanel')
                        .first()
                        .instance() as UserManagementPanel).state.selectedUser!
                        .role
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .find('div.primary')
        ).toHaveLength(1);
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .find('div.negative')
        ).toHaveLength(1);
        wrapper.unmount();
    });
    it('should make userWindow empty after closing current selected message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.negative')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.emptyUserWindow')
                .first()
                .contains('Wybierz jednego z użytkowników po lewej')
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render different role on button depending on current user role', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains('Zmień rolę na: User')
        ).toBe(true);
        wrapper
            .find('UserCard')
            .last()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.userWindow')
                .first()
                .contains('Zmień rolę na: Admin')
        ).toBe(true);
        wrapper.unmount();
    });
    it('should call onEmailValueChange after typing value in email input', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onEmailValueChange = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onEmailValueChange'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        shallow(
            wrapper
                .find('div.userWindow')
                .first()
                .find('div.input')
                .get(0)
        )
            .find('input')
            .first()
            .simulate('change', {
                preventDefault: jest.fn(),
                target: { value: 'someValue' }
            });
        expect(onEmailValueChange).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onLoginValueChange after typing value in login input', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onLoginValueChange = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onLoginValueChange'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        shallow(
            wrapper
                .find('div.userWindow')
                .first()
                .find('div.input')
                .get(1)
        )
            .find('input')
            .first()
            .simulate('change', {
                preventDefault: jest.fn(),
                target: { value: 'someValue' }
            });
        expect(onLoginValueChange).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onPasswordValueChange after typing value in password input', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onPasswordValueChange = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onPasswordValueChange'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        shallow(
            wrapper
                .find('div.userWindow')
                .first()
                .find('div.input')
                .get(2)
        )
            .find('input')
            .first()
            .simulate('change', {
                preventDefault: jest.fn(),
                target: { value: 'someValue' }
            });
        expect(onPasswordValueChange).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onChangeUserEmail after simulating changing user email', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onChangeUserEmail = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onChangeUserEmail'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.email')
            .first()
            .find('button')
            .first()
            .simulate('click');
        expect(onChangeUserEmail).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onChangeUserLogin after simulating changing user login', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onChangeUserLogin = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onChangeUserLogin'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.login')
            .first()
            .find('button')
            .first()
            .simulate('click');
        expect(onChangeUserLogin).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onChangeUserRole after simulating changing user role', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onChangeUserRole = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onChangeUserRole'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.primary')
            .first()
            .simulate('click');
        expect(onChangeUserRole).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should call onResetUserPassword after simulating changing user password', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        const onResetUserPassword = jest.spyOn(
            wrapper
                .find('UserManagementPanel')
                .first()
                .instance() as UserManagementPanel,
            'onResetUserPassword'
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.password')
            .first()
            .find('button')
            .first()
            .simulate('click');
        expect(onResetUserPassword).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should render error after typing invalid value in email input', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: [] as Message[],
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users
            }
        });
        const wrapper = mount<UserManagementPanel>(
            <Provider store={store}>
                <ConnectedUserManagementPanel />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('UserCard')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.email')
            .first()
            .find('input')
            .first()
            .simulate('change', {
                preventDefault: jest.fn(),
                target: { value: 'someInvalidEmail' }
            });
        wrapper
            .find('div.userWindow')
            .first()
            .find('div.email')
            .first()
            .find('button')
            .first()
            .simulate('click');
        expect(wrapper.contains('Podany email ma nieprawidłowy format')).toBe(
            true
        );
        wrapper.unmount();
    });
});
