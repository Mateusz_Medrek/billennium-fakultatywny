import { mount } from 'enzyme';
import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import reduxThunk from 'redux-thunk';
import wait from 'waait';
import { AppState, Message, ReceivedMessage, User } from '../actions/types';
import ConnectedMessageInbox, {
    MessageInbox
} from '../components/MessageInbox';

const mockStore = configureMockStore<AppState>([reduxThunk]);

const file = new File(['0o0101110010001', '0o111000011010100'], 'plik1.txt', {
    type: 'text/plain'
});
const messages: ReceivedMessage[] = [
    {
        id: '123',
        sender: 'pjoter@gmail.com',
        files: [
            {
                id: 'jakieśId',
                lastModified: file.lastModified,
                messageId: '123',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            }
        ],
        description: 'Wiadomosc do mojej rodzinki',
        date: new Date('2019-05-12T13:16:46.581Z')
    },
    {
        id: '456',
        sender: 'somsiad@gmail.com',
        files: [
            {
                id: 'inneId',
                lastModified: file.lastModified,
                messageId: '456',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            },
            {
                id: 'jeszczeInneId',
                lastModified: file.lastModified,
                messageId: '456',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            }
        ],
        description: 'Dla mojego najlepszego somsiada, Janusza',
        date: new Date('2019-05-09T13:16:46.581Z')
    },
    {
        id: '789',
        sender: 'grazyna@gmail.com',
        files: [
            {
                id: 'dziwneId',
                lastModified: file.lastModified,
                messageId: '789',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            },
            {
                id: 'normalneId',
                lastModified: file.lastModified,
                messageId: '789',
                name: file.name,
                size: file.size,
                slice: file.slice,
                title: file.name,
                type: file.type
            }
        ],
        description: 'Nie pij tyle',
        date: new Date('2019-05-06T13:16:46.581Z')
    }
];

describe('MessageInbox', () => {
    it('should render sort label, sort <Dropdown /> and filter input with icon', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        const styles: { [key: string]: React.CSSProperties } = {
            filterSorterInput: {
                margin: '5px',
                padding: '5px'
            }
        };
        expect(
            wrapper.contains(
                <div className="header" style={styles.filterSorterInput}>
                    Sortuj
                </div>
            )
        ).toBe(true);
        expect(wrapper.find('Dropdown')).toHaveLength(1);
        expect(
            wrapper
                .find('div.input')
                .first()
                .find('input')
        ).toHaveLength(1);
        expect(
            wrapper.contains(<i className="circular search link icon" />)
        ).toBe(true);
        wrapper.unmount();
    });
    it('should render emptyMessageWindow when none of messages are clicked', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<MessageInbox>(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        expect(
            wrapper
                .find('div.emptyMessageWindow')
                .first()
                .contains('Wybierz jedną z wiadomości po lewej')
        ).toBe(true);
        wrapper.unmount();
    });
    it('should have <MessageElement /> components rendered for every message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<MessageInbox>(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        await wait(0);
        expect(wrapper.find('MessageElement')).toHaveLength(
            (wrapper
                .find('MessageInbox')
                .first()
                .instance() as MessageInbox).props.messages.length
        );
        wrapper.unmount();
    });
    it('should call doesContainPhrase for every message after typing some value in filter input', () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<MessageInbox>(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        const doesContainPhrase = jest.spyOn(
            wrapper
                .find('MessageInbox')
                .first()
                .instance() as MessageInbox,
            'doesContainPhrase'
        );
        wrapper
            .find('div.input')
            .first()
            .find('input')
            .first()
            .simulate('change', { target: { value: 'someValue' } });
        wrapper.update();
        expect(doesContainPhrase).toBeCalledTimes(
            (wrapper
                .find('MessageInbox')
                .first()
                .instance() as MessageInbox).props.messages.length
        );
        wrapper.unmount();
    });
    it('should render messageWindow after clicking on one of messages', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<MessageInbox>(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('MessageElement')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Nadawca:</div>)
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(
                    (wrapper
                        .find('MessageInbox')
                        .first()
                        .instance() as MessageInbox).state.selectedMessage!
                        .sender
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Pliki:</div>)
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .find('FileElement')
        ).toHaveLength(
            (wrapper
                .find('MessageInbox')
                .first()
                .instance() as MessageInbox).state.selectedMessage!.files.length
        );
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(<div className="header">Opis:</div>)
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .contains(
                    (wrapper
                        .find('MessageInbox')
                        .first()
                        .instance() as MessageInbox).state.selectedMessage!
                        .description
                )
        ).toBe(true);
        expect(
            wrapper
                .find('div.messageWindow')
                .first()
                .find('div.negative')
        ).toHaveLength(1);
        wrapper.unmount();
    });
    it('should call downloadFile prop after clicking on one of files icon', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<MessageInbox>(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        const downloadFile = jest.spyOn(
            wrapper
                .find('MessageInbox')
                .first()
                .instance() as MessageInbox,
            'downloadFile'
        );
        await wait(0);
        wrapper
            .find('MessageElement')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.messageWindow')
            .first()
            .find('FileElement')
            .find('div.downloadButton')
            .first()
            .simulate('click');
        expect(downloadFile).toBeCalledTimes(1);
        wrapper.unmount();
    });
    it('should make messageWindow empty after closing current selected message', async () => {
        const store = mockStore({
            auth: {
                changeEmailError: null,
                changeLoginError: null,
                changePasswordError: null,
                email: 'janusz@gmail.com',
                loginError: null,
                isSignedIn: true,
                registerError: null,
                role: 'User',
                userId: 'someUserId'
            },
            message: {
                error: null,
                messagesForUser: messages,
                notAcceptedMessages: [] as Message[]
            },
            user: {
                changeUserEmailError: '',
                changeUserLoginError: '',
                changeUserRoleError: '',
                getAllUsersError: '',
                resetUserPasswordError: '',
                users: [] as User[]
            }
        });
        const wrapper = mount<MessageInbox>(
            <Provider store={store}>
                <ConnectedMessageInbox />
            </Provider>
        );
        await wait(0);
        wrapper
            .find('MessageElement')
            .first()
            .find('div.item')
            .first()
            .simulate('click');
        wrapper.update();
        wrapper
            .find('div.messageWindow')
            .first()
            .find('div.negative')
            .first()
            .simulate('click');
        wrapper.update();
        expect(
            wrapper
                .find('div.emptyMessageWindow')
                .first()
                .contains('Wybierz jedną z wiadomości po lewej')
        ).toBe(true);
        wrapper.unmount();
    });
});
