import { shallow } from 'enzyme';
import React from 'react';
import Navbar from '../components/Navbar';

describe('Navbar', () => {
    it('should render children', () => {
        const children = (
            <div>
                <div>Children</div>
            </div>
        );
        const wrapper = shallow(<Navbar>{children}</Navbar>);
        expect(
            wrapper.contains(
                <div className="ui blue three inverted menu">{children}</div>
            )
        ).toBe(true);
    });
});
