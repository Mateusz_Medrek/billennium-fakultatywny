import { mount, ReactWrapper, shallow } from 'enzyme';
import React from 'react';
import ConnectedLoginForm, { LoginForm } from '../components/LoginForm';
import RootProvider from '../components/RootProvider';

describe('LoginForm', () => {
    let wrapper: ReactWrapper;
    beforeEach(() => {
        wrapper = mount(
            <RootProvider>
                <ConnectedLoginForm />
            </RootProvider>
        );
    });
    afterEach(() => {
        wrapper.unmount();
    });
    it('should have <form />', () => {
        expect(wrapper.find('form')).toHaveLength(1);
    });
    it('should have 2 <Field /> components', () => {
        // <Field /> is wrapped in hoc and that's why if LoginForm has 2 <Field />,
        // we should expect 4 in this test;
        expect(wrapper.find('Field')).toHaveLength(4);
    });
    it('should have submit <button />', () => {
        expect(
            wrapper.contains(
                <button className="ui primary button" type="submit">
                    Zaloguj
                </button>
            )
        ).toBe(true);
    });
    it('should display <div /> with error messages after leaving blank inputs', () => {
        shallow(wrapper.find('input').get(0)).simulate('change', '');
        shallow(wrapper.find('input').get(0)).simulate('blur');
        shallow(wrapper.find('input').get(1)).simulate('change', '');
        shallow(wrapper.find('input').get(1)).simulate('blur');
        wrapper.update();
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać email</div>
                </div>
            )
        ).toBe(true);
        expect(
            wrapper.contains(
                <div className="ui error message">
                    <div className="header">Musisz podać hasło</div>
                </div>
            )
        ).toBe(true);
    });
    it('should call authenticateUser prop after submitting form', () => {
        const onSubmit = jest.spyOn(
            wrapper
                .find('LoginForm')
                .first()
                .instance() as LoginForm,
            'onSubmit'
        );
        shallow(wrapper.find('input').get(0)).simulate('change', 'Janusz69');
        shallow(wrapper.find('input').get(0)).simulate('blur');
        shallow(wrapper.find('input').get(1)).simulate('change', 'janusz');
        shallow(wrapper.find('input').get(1)).simulate('blur');
        wrapper.update();
        wrapper
            .find('form')
            .first()
            .simulate('submit');
        expect(onSubmit).toBeCalledTimes(1);
    });
});
